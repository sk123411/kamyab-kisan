package com.ruralculture

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.widget.Toast
import com.ruralculture.kayabkishan.model.SignUpResponse
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import io.paperdb.Paper
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class Constant {

    companion object {

        val SELECTED_PRODUCTS: String?="selected_products"
        val SAVED_CATEGORY: String?="saved_category"
        val FILTER_CATEGORY_SELECTED: String?="filter_category_selected"
        val FILTER_CATEGORY: String?="filter_cateogry"
        val API_GET_NOTIFICATION: String?="show_notification"
        val API_GET_WALLET_BALANCE: String?="show_wallet"
        val FILTER: String?="filter_products"
        val FRESHCHAT_RESTORE_ID: String?="freshchat_restore_id"
        val API_ALL_MESSAGES: String?="show_all_userchat"
        val API_ALL_MESSAGES_USER: String?="show_message"
        val API_SEND_MESSAGE: String?="message_insert"
        val ORDER_ID: String?="order_id"
        val API_USER_STATS: String?="view_user_stats"
        val SAVED_ORDER_ID: String?="saved_order_id"
        val API_CHANGE_ORDER_PAYMENT_TYPE: String?="update_checkout"
        val PENDING_ORDER: String?="pending_order"
        val API_CHANGE_ORDER_STATUS: String?="make_payment"
        val API_GET_ORDERS: String?="my_investment"
        val API_PLACE_ORDER: String?="checkout"
        val PRODUCT: String?="product"
        val ISVERIFIED: String?="isVerified"
        val API_VERIFICATION_STATUS: String?="show_user_byuserid"
        val API_UPLOAD_DOCUMENT: String?="uplode_document"
        val API_WISHLIST_PRODUCTS: String?="show_favrouti_product"
        val API_GET_PROFILE: String?="profile_update"
        val COMPAIGNS: String?="Compaigns"
        val API_UPDATE_COMMENT_ARTICLE: String?="edit_comment_blog"

        val API_DELETE_COMMENT_ARTICLE: String?="delete_comment_blog"
        val API_ADD_LIKE_PRODUCT: String?="add_product_tofavrouti"
        val API_SHOW_ALL_PRODUCTS: String?="show_all_product"
        val API_ADD_LIKE: String?="like_blog"
        val API_ADD_COMMENT: String?="comment_blog"

        val API_ARTICLES_DETAILS: String?="show_blogby_id"
        val API_ALL_ARTICLES: String?="show_blog"
        val BASE_IMAGE_PATH: String?="https://ruparnatechnology.com/Kamyabkisan/Admin/images/"
        val API_PRODUCT_DETAIL: String?="product_details_byid"
        val API_PRODUCTS: String?="show_product"
        val API_CATEGORY: String?="show_catetory"
        val USER_LOGGED: String?="user_logged"
        val PASSWORD: String?="password"
        val NUMBER: String?="number"
        val REMEMBERED_DATA: String?="loginSavedEmail"
        val REMEMBER_CHECKED: String?="rememberChecked"
        val API_LOGIN: String?="login"
        var clicked:String? = "1"

        const val API_SIGNUP="sign_up"





        fun getUserID(context: Context):SignUpResponse{

            Paper.init(context)
            return Paper.book().read<SignUpResponse>(Constant.USER_LOGGED,null)
        }


        fun shareImageFromURI(url: String?, title: String?, context: Context) {
            Picasso.get().load(url).into(object : Target {
                override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {

                    val intent = Intent(Intent.ACTION_SEND)
                    intent.type = "*/*"
                    intent.putExtra(Intent.EXTRA_STREAM, getBitmapFromView(bitmap, context))
                    intent.putExtra(
                        Intent.EXTRA_TEXT,
                        title + "\n" + "Kamyab kisan download app from playstore"
                    )
                    context.startActivity(Intent.createChooser(intent, "Share Image"))
                }

                override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
                override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {}
            })
        }


        fun getBitmapFromView(bitmap: Bitmap?, context: Context?): Uri? {
            var bmpUri: Uri? = null
            try {
                val file =
                    File(
                        context!!.externalCacheDir.toString(),
                        System.currentTimeMillis().toString() + ".jpg"
                    )

                val out = FileOutputStream(file)
                bitmap?.compress(Bitmap.CompressFormat.JPEG, 90, out)
                out.close()
                bmpUri = Uri.fromFile(file)

            } catch (e: IOException) {
                e.printStackTrace()
            }

            return bmpUri!!

        }


        fun showToast(context: Context,message:String){
            Toast.makeText(context, message,Toast.LENGTH_SHORT).show()
        }


    }
}