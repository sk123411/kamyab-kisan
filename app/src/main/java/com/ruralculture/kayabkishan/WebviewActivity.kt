package com.ruralculture.kayabkishan

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import com.ruralculture.kayabkishan.databinding.ActivityWebviewBinding

class WebviewActivity : AppCompatActivity() {
    lateinit var binding: ActivityWebviewBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityWebviewBinding.inflate(layoutInflater)
        setContentView(binding.root)




        val url = intent.getStringExtra("url")


        if (url!=null){


            binding.webView.settings.loadsImagesAutomatically = true;
            binding.webView.settings.javaScriptEnabled = true
            binding.webView.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY;
            binding.webView.loadUrl(url)
            binding.progressBar.visibility = View.VISIBLE

            binding.webView.setWebViewClient(object : WebViewClient() {
                override fun onPageFinished(view: WebView, url: String) {

                    binding.progressBar.visibility = View.GONE

                }


            })




        }





    }
}