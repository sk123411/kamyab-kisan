package com.ruralculture.kayabkishan

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.fragments.login.LoginActivity
import com.ruralculture.kayabkishan.model.SignUpResponse
import com.ruralculture.kayabkishan.profile.repository.ProfileViewModel
import io.paperdb.Paper


class SplashActivity : AppCompatActivity() {


    private val profileViewModel: ProfileViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)


//        if(Constant.getUserID(this).userID!=null) {
//            profileViewModel.getVerificationStatus(Constant.getUserID(this).userID.toString())
//        }
        Paper.init(this)








        lifecycleScope.launchWhenStarted {


            profileViewModel.profileEvent.observe(this@SplashActivity, Observer {

                when(it){


                    is ProfileViewModel.ProfileEvent.Success -> {


                        if (it.profleResponse.activeStatus!=null){

                            if (it.profleResponse.activeStatus.equals("1")){


                                Paper.book().write(Constant.ISVERIFIED,true)

                            }else {

                                Paper.book().write(Constant.ISVERIFIED,false)

                            }
                        }


                    }

                    is ProfileViewModel.ProfileEvent.Loading -> {



                    }
                }




            })



        }




        Paper.init(this)
        val userLogged = Paper.book().read<SignUpResponse>(Constant.USER_LOGGED)





        Handler().postDelayed({

            if (userLogged!=null) {
                startActivity(Intent(this, MainActivity::class.java))
            }else {
                startActivity(Intent(this, LoginActivity::class.java))

            }


        },2000)
    }
}