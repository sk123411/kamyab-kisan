package com.ruralculture.kayabkishan

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.databinding.FragmentCompareBinding
import com.ruralculture.kayabkishan.model.invest.InvestProduct
import com.squareup.picasso.Picasso
import io.paperdb.Paper

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [CompareFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CompareFragment : Fragment() {

    lateinit var binding:FragmentCompareBinding



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCompareBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment

        Paper.init(requireContext())

        val selectedProductList = Paper.book().read<List<InvestProduct>>(Constant.SELECTED_PRODUCTS)


        setFirstProductData(selectedProductList.get(0))
        setSecondProductData(selectedProductList.get(1))



        return binding.root
    }

    private fun setSecondProductData(investProduct: InvestProduct) {
        Picasso.get().load(Constant.BASE_IMAGE_PATH+investProduct.productImage).into(binding.productOneImage)


        binding.productOneUIText.text = investProduct.productName
        binding.investmentPriceFirst.text = "PKR "+ investProduct.productPrice
        binding.roiFirstText.text = investProduct.roi
        binding.harvestOnePrice.text = investProduct.contract

    }

    private fun setFirstProductData(investProduct: InvestProduct) {
        Picasso.get().load(Constant.BASE_IMAGE_PATH+investProduct.productImage).into(binding.productTwoImage)
        binding.productTwoUIText.text = investProduct.productName

        binding.investmentSecondFirst.text = "PKR "+ investProduct.productPrice
        binding.roiSecondText.text = investProduct.roi
        binding.harvestTwoPrice.text = investProduct.contract



    }

}