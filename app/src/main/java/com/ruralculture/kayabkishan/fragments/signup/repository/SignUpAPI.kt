package com.ruralculture.kayabkishan.fragments.signup.repository

import com.ruralculture.Constant
import com.ruralculture.kayabkishan.model.SignUpResponse
import retrofit2.Response
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Query

interface SignUpAPI {


    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun register(@Query("action") action:String?, @FieldMap data:HashMap<String,String>):Response<SignUpResponse>



}