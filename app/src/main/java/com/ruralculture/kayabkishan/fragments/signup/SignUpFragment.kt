package com.ruralculture.kayabkishan.fragments.signup

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.databinding.FragmentSignUpBinding
import com.ruralculture.kayabkishan.fragments.signup.repository.SignUpViewModel
import io.paperdb.Paper


class SignUpFragment : Fragment() {

    lateinit var binding: FragmentSignUpBinding

    private val signUpViewModel: SignUpViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSignUpBinding.inflate(layoutInflater)
        binding.userNameEdit.requestFocus()
        binding.usernameLayout.setBackgroundResource(R.drawable.selected_edittext)


        binding.ccp.registerPhoneNumberTextView(binding.mobileEdit)



        binding.signupButton.setOnClickListener {

            //    Navigation.findNavController(it).navigate(R.id.OTPFragment)


            if (binding.userNameEdit.text.toString().isEmpty() ||
                binding.mobileEdit.text.toString().isEmpty() ||
                binding.passwordEdit.text.toString().isEmpty()
            ) {



            } else {


                signUpViewModel.register(binding.userNameEdit.text.toString(), binding.mobileEdit.text.toString(),binding.emailEdit.text.toString(),
                    binding.passwordEdit.text.toString())
            }

        }





        lifecycleScope.launchWhenStarted {



            signUpViewModel.signUpEvent.observe(viewLifecycleOwner, Observer {res ->


                when(res){


                   is  SignUpViewModel.SignUpEvent.Success -> {

                       activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                       if (res.signUpResponse.status!!) {

                           Paper.init(activity)
                           Paper.book().write(Constant.USER_LOGGED,res.signUpResponse)


                           Toast.makeText(
                               context,
                               "Sign up successfull ",
                               Toast.LENGTH_SHORT
                           ).show()

                           Navigation.findNavController(requireView()).navigate(R.id.otpFragment)


                       }else {

                           Toast.makeText(
                               context,
                               " Result ${res.signUpResponse.result}",
                               Toast.LENGTH_SHORT
                           ).show()
                       }
                    }

                    is SignUpViewModel.SignUpEvent.Failure -> {


                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false
                        Toast.makeText(context, "Sign up failed ${res.message}", Toast.LENGTH_SHORT).show()
                    }


                    is SignUpViewModel.SignUpEvent.Loading -> {


                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true
                    }
                }




            })


        }

        binding.emailEdit.setOnFocusChangeListener(object : View.OnFocusChangeListener {
            override fun onFocusChange(v: View?, hasFocus: Boolean) {

                if (hasFocus) {
                    binding.emaailLayout.setBackgroundResource(R.drawable.selected_edittext)
                } else {
                    binding.emaailLayout.setBackgroundResource(R.drawable.white_background_radius)
                }

            }

        })



        binding.userNameEdit.setOnFocusChangeListener(object : View.OnFocusChangeListener {
            override fun onFocusChange(v: View?, hasFocus: Boolean) {

                if (hasFocus) {
                    binding.usernameLayout.setBackgroundResource(R.drawable.selected_edittext)
                } else {
                    binding.usernameLayout.setBackgroundResource(R.drawable.white_background_radius)
                }

            }

        })


        binding.passwordEdit.setOnFocusChangeListener(object : View.OnFocusChangeListener {
            override fun onFocusChange(v: View?, hasFocus: Boolean) {
                if (hasFocus) {
                    binding.passwordLayout.setBackgroundResource(R.drawable.selected_edittext)
                } else {
                    binding.passwordLayout.setBackgroundResource(R.drawable.white_background_radius)
                }

            }

        })
        binding.mobileEdit.setOnFocusChangeListener(object : View.OnFocusChangeListener {
            override fun onFocusChange(v: View?, hasFocus: Boolean) {
                if (hasFocus) {
                    binding.mobileLayout.setBackgroundResource(R.drawable.selected_edittext)
                } else {
                    binding.mobileLayout.setBackgroundResource(R.drawable.white_background_radius)
                }

            }

        })


        binding.userNameEdit.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                binding.emailEdit.requestFocus()
                return@OnEditorActionListener true
            }
            false
        })



        binding.emailEdit.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                binding.mobileEdit.requestFocus()
                return@OnEditorActionListener true
            }
            false
        })



        binding.mobileEdit.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                binding.passwordEdit.requestFocus()
                return@OnEditorActionListener true
            }
            false
        })




        binding.signupButton.setOnClickListener {


            if (binding.emailEdit.text.toString().isNotEmpty()&&
                binding.userNameEdit.text.toString().isNotEmpty()&&
                binding.mobileEdit.text.toString().isNotEmpty()&&
                binding.passwordEdit.text.toString().isNotEmpty()
                    )
            {



                signUpViewModel.register(binding.userNameEdit.text.toString(), binding.mobileEdit.text.toString(),binding.emailEdit.text.toString(),
                    binding.passwordEdit.text.toString())
            }


        }

        return binding.root
    }


}