package com.ruralculture.kayabkishan.fragments.login

import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.CompoundButton
import android.widget.ProgressBar
import android.widget.TextView.OnEditorActionListener
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.MainActivity
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.WebviewActivity
import com.ruralculture.kayabkishan.databinding.FragmentLoginBinding
import com.ruralculture.kayabkishan.fragments.login.repository.LoginViewModel
import com.ruralculture.kayabkishan.profile.TERMS_AND_CONDITION_URL
import io.paperdb.Paper


class LoginFragment : Fragment() {


    lateinit var binding:FragmentLoginBinding
    private val loginViewModel: LoginViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLoginBinding.inflate(layoutInflater)
        binding.numberEdit.requestFocus()
        binding.mobileLayout.setBackgroundResource(R.drawable.selected_edittext)



        // Inflate the layout for this fragment

        binding.buttonSignUp.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.signUpFragment)


        }



        Paper.init(activity)
        val rememberChecked = Paper.book().read<Int>(Constant.REMEMBER_CHECKED)

        if (rememberChecked==1){

            binding.rememberRadio.isChecked = true

            val savedLoginData = Paper.book().read<HashMap<String,String>>(Constant.REMEMBERED_DATA)

            if (savedLoginData!=null) {
                binding.numberEdit.setText(savedLoginData.get(Constant.NUMBER))
                binding.passwordEdit.setText(savedLoginData.get(Constant.PASSWORD))
                binding.passwordEdit.inputType = InputType.TYPE_TEXT_VARIATION_PASSWORD

            }

        }else {

            binding.rememberRadio.isChecked = false
        }






        binding.rememberRadio.setOnCheckedChangeListener(object :CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {

                if (isChecked){

                    Paper.init(context)
                    Paper.book().write(Constant.REMEMBER_CHECKED,1)

                    if (binding.numberEdit.text.toString().isNotEmpty() && binding.passwordEdit.text.toString().isNotEmpty()){

                        val loginSavedData = HashMap<String,String>()
                        loginSavedData.put(Constant.NUMBER!!, binding.numberEdit.text.toString())
                        loginSavedData.put(Constant.PASSWORD!!, binding.passwordEdit.text.toString())

                        Paper.book().write(Constant.REMEMBERED_DATA,loginSavedData)


                    }
                }else {
                    Paper.init(context)
                    Paper.book().write(Constant.REMEMBER_CHECKED,0)


                }
            }

        })




        binding.termsAndConditionLayout.setOnClickListener {
            startActivity(Intent(context,WebviewActivity::class.java).putExtra("url",
                TERMS_AND_CONDITION_URL))

        }





        binding.numberEdit.setOnFocusChangeListener(object:View.OnFocusChangeListener{
            override fun onFocusChange(v: View?, hasFocus: Boolean) {

                if (hasFocus){

                    binding.mobileLayout.setBackgroundResource(R.drawable.selected_edittext)

                }else {
                    binding.mobileLayout.setBackgroundResource(R.drawable.white_background_radius)

                }
            }

        })

        binding.passwordEdit.setOnFocusChangeListener(object:View.OnFocusChangeListener{
            override fun onFocusChange(v: View?, hasFocus: Boolean) {

                if (hasFocus){

                    binding.passwordLayout.setBackgroundResource(R.drawable.selected_edittext)

                }else {
                    binding.passwordLayout.setBackgroundResource(R.drawable.white_background_radius)

                }
            }

        })







        binding.numberEdit.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                binding.passwordEdit.requestFocus()
                return@OnEditorActionListener true
            }
            false
        })



        lifecycleScope.launchWhenStarted {



            loginViewModel.loginEvent.observe(viewLifecycleOwner, Observer {res ->


                when(res){


                    is  LoginViewModel.LoginEvent.Success -> {



                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                        if (res.signUpResponse.status!!) {

                            Paper.init(activity)
                            Paper.book().write(Constant.USER_LOGGED,res.signUpResponse)


                            Toast.makeText(
                                context,
                                "Sign in successfull ${res.signUpResponse}",
                                Toast.LENGTH_SHORT
                            ).show()


                            val verifyStatus = Paper.book().read<Boolean>(Constant.ISVERIFIED,false)
                            if (verifyStatus){
                                startActivity(Intent(context,MainActivity::class.java))


                            }else {


                                Navigation.findNavController(requireView()).navigate(R.id.otpFragment)
                            }






                        }else {

                            Toast.makeText(
                                context,
                                " Result ${res.signUpResponse.result}",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }

                    is LoginViewModel.LoginEvent.Failure -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false


                        Toast.makeText(context, "Sign up failed ${res.message}", Toast.LENGTH_SHORT).show()
                    }


                    is LoginViewModel.LoginEvent.Loading -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true

                    }
                }




            })


        }




        binding.loginButton.setOnClickListener {







            if (binding.numberEdit.text.toString().isNullOrEmpty() ||
                binding.passwordEdit.text.toString().isNullOrEmpty()
            ) {


            } else {


                loginViewModel.login(binding.numberEdit.text.toString(),binding.passwordEdit.text.toString())
            }
        }
        return binding.root
    }


}