package com.ruralculture.kayabkishan.fragments.signup.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ruralculture.kayabkishan.helper.Resource
import com.ruralculture.kayabkishan.model.SignUpResponse
import com.ruralculture.kayabkishan.retofit.RetrofitInstance
import kotlinx.coroutines.launch

class SignUpViewModel :ViewModel(){



    private val signUpEvent_ = MutableLiveData<SignUpEvent>()
    var signUpAPI: SignUpAPI
    var signUp: Signup


    val signUpEvent:LiveData<SignUpEvent>
        get() = signUpEvent_


init {
    signUpAPI = RetrofitInstance.getRetrofitInstance().create(SignUpAPI::class.java)
    signUp =
        SignUpImplementation(
            signUpAPI
        )


}







    sealed class SignUpEvent{

         class Success(val signUpResponse: SignUpResponse):
             SignUpEvent()
         class Failure(val message:String?):
             SignUpEvent()
         object Loading:
             SignUpEvent()
         object Empty:
             SignUpEvent()

    }




    fun register(userName:String?,mobile:String?,email:String?,password:String?){

        val data = HashMap<String,String>()
        data.put("name", userName!!)
        data.put("email",email!!)
        data.put("mobile", mobile!!)
        data.put("password",password!!)


        viewModelScope.launch {
            signUpEvent_.value =
                SignUpEvent.Loading

            val response = signUp.register(data)

            when (response) {

                is Resource.Success -> {


                    signUpEvent_.value =
                        SignUpEvent.Success(
                            response.data!!
                        )



                }

                is Resource.Error -> {
                    signUpEvent_.value =
                        SignUpEvent.Failure(
                            "Sign up successfull"!!
                        )


                }


            }



        }



    }





}