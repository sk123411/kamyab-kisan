package com.ruralculture.kayabkishan.fragments.signup.repository

import android.util.Log
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.helper.Resource
import com.ruralculture.kayabkishan.model.SignUpResponse

class SignUpImplementation constructor(var signUpAPI: SignUpAPI) :
    Signup {



    override suspend fun register(data: HashMap<String, String>): Resource<SignUpResponse> {

        return try {

            val response = signUpAPI.register(Constant.API_SIGNUP,data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }

    }





}