package com.ruralculture.kayabkishan.fragments.login.repository

import com.ruralculture.Constant
import com.ruralculture.kayabkishan.model.SignUpResponse
import retrofit2.Response
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Query

interface LoginAPI {


    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun login(@Query("action") action:String?, @FieldMap data:HashMap<String,String>): Response<SignUpResponse>



}