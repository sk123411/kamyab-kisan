package com.ruralculture.kayabkishan.fragments.login

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.github.dhaval2404.imagepicker.ImagePicker
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.MainActivity
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.databinding.FragmentVerificationBinding
import com.ruralculture.kayabkishan.profile.model.ProfileResponse
import com.ruralculture.kayabkishan.profile.repository.ProfileViewModel
import com.squareup.picasso.Picasso
import java.io.File

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [OTPFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class VerificationFragment : Fragment() {

    private val profileViewModel: ProfileViewModel by viewModels()
    lateinit var binding: FragmentVerificationBinding

    var photo:PHOTO?=null
    var imageFiles:MutableList<File>?=null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentVerificationBinding.inflate(layoutInflater)
        imageFiles = ArrayList()




        binding.frontPhotoButton.setOnClickListener {

            photo = PHOTO.FRONT
            ImagePicker.with(this)
                .galleryOnly()
                .compress(1024)
                .start()
        }



        binding.backButtonPhoto.setOnClickListener {

            photo = PHOTO.BACK

            ImagePicker.with(this)
                .galleryOnly()
                .compress(1024)
                .start()

        }




        binding.takeSelfieButton.setOnClickListener {

            photo = PHOTO.SELFIE

            ImagePicker.with(this)
                .cameraOnly()
                .compress(1024)
                .start()
        }


            lifecycleScope.launchWhenStarted {

                profileViewModel.imageFiles.observe(viewLifecycleOwner, Observer {

                    binding.uploadPhotoButton.setOnClickListener {v->
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true

//                        profileViewModel.uploadDocument(
//                            Constant.getUserID(requireContext()).userID.toString(),
//                            it.get(0),it.get(1),it.get(2)
//                        )

                        AndroidNetworking.upload("https://ruparnatechnology.com/Kamyabkisan/api/process.php?action=uplode_document")
                            .addMultipartFile("front_image",it.get(0)
                            ).addMultipartFile("back_image",it.get(1))
                            .addMultipartFile("image",it.get(2))
                            .build().getAsObject(ProfileResponse::class.java,object:ParsedRequestListener<ProfileResponse>{
                                override fun onResponse(response: ProfileResponse?) {

                                    if (response!=null){
                                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false
                                        Constant.showToast(requireContext(),"Verification images! uploaded successfully")




                                    }

                                }

                                override fun onError(anError: ANError?) {

                                    Log.d("TESTTTTTTTTT", ":::"+anError.toString())

                                }


                            })


                    }

                })

                profileViewModel.profileEvent.observe(viewLifecycleOwner, Observer {

                    when(it){

                        is ProfileViewModel.ProfileEvent.SuccessVerification -> {
                            activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                        }
                        is ProfileViewModel.ProfileEvent.Loading -> {
                            activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true

                        }

                    }

                })

            }


        binding.skipButton.setOnClickListener {

            startActivity(Intent(requireActivity(), MainActivity::class.java))


        }



        return binding.root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val fileUri = data?.data

            val file: File = ImagePicker.getFile(data)!!

            if (photo==PHOTO.FRONT) {
                binding.frontIDImage.setImageURI(fileUri)
                binding.checkFrontID.setImageResource(R.drawable.check)
                binding.frontPhotoButton.setBackgroundTintList(resources.getColorStateList(R.color.orange));
                imageFiles!!.add(0,file)

            }else if (photo==PHOTO.BACK){
                binding.backIDImage.setImageURI(fileUri)
                binding.checkBackID.setImageResource(R.drawable.check)
                imageFiles!!.add(1,file)
                binding.backButtonPhoto.setBackgroundTintList(resources.getColorStateList(R.color.orange));


            }else {
                binding.selfieIDImage.setImageURI(fileUri)
                binding.checkSelfieID.setImageResource(R.drawable.check)
                imageFiles!!.add(2,file)
                binding.takeSelfieButton.setBackgroundTintList(resources.getColorStateList(R.color.orange));


            }



            //You can get File object from intent

            //You can also get File Path from intent
            profileViewModel.sendImageFiles(imageFiles!!)


            val filePath: String = ImagePicker.getFilePath(data)!!
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(context, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(context, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }


    enum class PHOTO{
        FRONT,
        BACK,
        SELFIE

    }
}