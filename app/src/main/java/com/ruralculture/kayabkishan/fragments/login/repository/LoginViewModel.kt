package com.ruralculture.kayabkishan.fragments.login.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ruralculture.kayabkishan.helper.Resource
import com.ruralculture.kayabkishan.model.SignUpResponse
import com.ruralculture.kayabkishan.retofit.RetrofitInstance
import kotlinx.coroutines.launch

class LoginViewModel  : ViewModel(){



    private val loginEvent_ = MutableLiveData<LoginEvent>()
    lateinit var loginAPI: LoginAPI
    lateinit var login: Login


    val loginEvent: LiveData<LoginEvent>
        get() = loginEvent_


    init {
        loginAPI = RetrofitInstance.getRetrofitInstance().create(LoginAPI::class.java)
        login =
            LoginImplemenation(
                loginAPI
            )


    }







    sealed class LoginEvent{

        class Success(val signUpResponse: SignUpResponse):
            LoginEvent()
        class Failure(val message:String?):
            LoginEvent()
        object Loading:
            LoginEvent()
    }




    fun login(mobile:String?,password:String?){

        val data = HashMap<String,String>()
        data.put("mobile", mobile!!)
        data.put("password",password!!)


        viewModelScope.launch {
            loginEvent_.value =
                LoginEvent.Loading

            val response = login.login(data)

            when (response) {

                is Resource.Success -> {


                    loginEvent_.value =
                        LoginEvent.Success(
                            response.data!!
                        )



                }

                is Resource.Error -> {
                    loginEvent_.value =
                        LoginEvent.Failure(
                            "Sign in success"
                        )


                }


            }



        }



    }





}