package com.ruralculture.kayabkishan.fragments.signup.repository

import com.ruralculture.Constant
import com.ruralculture.kayabkishan.helper.Resource
import com.ruralculture.kayabkishan.model.SignUpResponse
import retrofit2.http.FieldMap
import retrofit2.http.Query

interface Signup {

    suspend fun register(data:HashMap<String,String>):Resource<SignUpResponse>

}