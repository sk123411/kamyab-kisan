package com.ruralculture.kayabkishan.fragments.login.repository

import android.util.Log
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.helper.Resource
import com.ruralculture.kayabkishan.model.SignUpResponse

class LoginImplemenation  constructor(var loginAPI: LoginAPI) :
    Login {



    override suspend fun login(data: HashMap<String, String>): Resource<SignUpResponse> {

        return try {

            val response = loginAPI.login(Constant.API_LOGIN,data)
            Log.d("XXXXXXXXX", "" + data.toString())


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result!!)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }

    }





}