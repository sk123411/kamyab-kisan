package com.ruralculture.kayabkishan.fragments.login.repository

import com.ruralculture.kayabkishan.helper.Resource
import com.ruralculture.kayabkishan.model.SignUpResponse

interface Login {

    suspend fun login(data:HashMap<String,String>): Resource<SignUpResponse>

}