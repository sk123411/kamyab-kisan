package com.ruralculture.kayabkishan.model.blog


import androidx.annotation.Nullable
import com.google.gson.annotations.SerializedName

data class Blog(
    @SerializedName("blog_id")
    val blogId: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("user_id")
    val userId: String,
    @SerializedName("comment")
    @Nullable
    val comment: String?)