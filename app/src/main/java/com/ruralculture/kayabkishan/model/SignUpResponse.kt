package com.ruralculture.kayabkishan.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SignUpResponse(@SerializedName("userID") @Expose val userID : Int,
                          @SerializedName("userName") @Expose val userName : String,
                          @SerializedName("userMobile") @Expose val userMobile : Int,
                          @SerializedName("user_password")@Expose val user_password : String,
                          @SerializedName("address") @Expose val address : String,
                          @SerializedName("userImage") @Expose val userImage : String,
                          @SerializedName("role") @Expose val  role : Int,
                          @SerializedName("strtotime") @Expose val strtotime : String,
                          @SerializedName("path") @Expose val path : String,
                          @SerializedName("result") @Expose val result : String,
                          @SerializedName("status") @Expose val  status : Boolean)