package com.ruralculture.kayabkishan.model.blog


import androidx.annotation.Nullable
import com.google.gson.annotations.SerializedName
import com.ruralculture.kayabkishan.model.blog.Blog

data class BlogLikeResponse(
    @SerializedName("data")
    @Nullable
    val blog: Blog?,
    @SerializedName("result")
    val result: String
)