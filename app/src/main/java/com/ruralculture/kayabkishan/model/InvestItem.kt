package com.ruralculture.kayabkishan.model

data class InvestItem(
    val year:Int?,
    val returnPercent:String?,
    val returnPrice:Int?

    )