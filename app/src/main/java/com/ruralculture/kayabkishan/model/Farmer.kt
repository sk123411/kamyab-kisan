package com.ruralculture.kayabkishan.model


import com.google.gson.annotations.SerializedName

data class Farmer(
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("image")
    val image: String,
    @SerializedName("mobile")
    val mobile: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("crop_name")
    val crop_name: String

)