package com.ruralculture.kayabkishan.model

data class DashboardItem(
    val title:String?,
    val unitsSold:Int?,
    val unitsLeft:Int?,
    val investmentPrice:String?,
    val returnPercent:String?

)