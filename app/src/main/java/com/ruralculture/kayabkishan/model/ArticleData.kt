package com.ruralculture.kayabkishan.model


import androidx.annotation.Nullable
import com.google.gson.annotations.SerializedName

data class ArticleData(
    @SerializedName("description")
    val description: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("image")
    val image: String,
    @SerializedName("path")
    val path: String,
    @SerializedName("strtotime")
    val strtotime: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("date")
    @Nullable
    val date: String,
    @SerializedName("like_state")
    @Nullable
    val like_state: Int
)