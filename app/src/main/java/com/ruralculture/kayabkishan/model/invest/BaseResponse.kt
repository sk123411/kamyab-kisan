package com.ruralculture.kayabkishan.model.invest

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class BaseResponse<T>(
    @SerializedName("data")
    @Expose
    val `data`: List<T>,
    @SerializedName("status")
    @Expose
    val status: Boolean
)