package com.ruralculture.kayabkishan.model


import androidx.annotation.Nullable
import com.google.gson.annotations.SerializedName

data class Comment(
    @SerializedName("blog_id")
    val blogId: String,
    @SerializedName("comment")
    val comment: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("user_id")
    val userId: String,
    @SerializedName("userName")
    @Nullable
    val userName: String?,
    @SerializedName("time")
    val time: String?
)