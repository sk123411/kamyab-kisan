package com.ruralculture.kayabkishan.model


import androidx.annotation.Nullable
import com.google.gson.annotations.SerializedName
import com.ruralculture.kayabkishan.dashboard.slider.Slider

data class ProductDetailResponse(
    @SerializedName("available_Inventory")
    val availableInventory: String,
    @SerializedName("contract")
    val contract: String,
    @SerializedName("end_date")
    val endDate: String,
    @SerializedName("first_return")
    val firstReturn: String,
    @SerializedName("location")
    val location: String,
    @SerializedName("product_category")
    val productCategory: String,
    @SerializedName("product_detail")
    val productDetail: String,
    @SerializedName("product_id")
    val productId: String,
    @SerializedName("product_image")
    val productImage: String,
    @SerializedName("product_name")
    val productName: String,
    @SerializedName("product_price")
    val productPrice: String,
    @SerializedName("result")
    val result: Boolean,
    @SerializedName("ribbon")
    val ribbon: String,
    @SerializedName("roi")
    val roi: String,
    @SerializedName("start_date")
    val startDate: String,
    @SerializedName("starting_Inventory")
    val startingInventory: String,
    @SerializedName("strtotime")
    val strtotime: String,
    @SerializedName("sub_category_id")
    val subCategoryId: String,
    @SerializedName("category_name")
    val category_name: String,
    @SerializedName("like_state")
    val like_state: Int,
    @SerializedName("price_total")
    @Nullable
    var price_total: Double,
    @SerializedName("unit")
    @Nullable
    var unit: Int = 1,
    @SerializedName("multi_image")
    @Nullable
    var multi_image: List<Slider>
    ,
    @SerializedName("farmer")
    @Nullable
    val farmer: Farmer?

)