package com.ruralculture.kayabkishan.model.invest

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class InvestResponse(
    @SerializedName("categoryID")
    @Expose
    val categoryID: String,
    @SerializedName("categoryName")
    @Expose
    val categoryName: String,
    @SerializedName("image")
    @Expose
    val image: String,
    @SerializedName("path")
    @Expose
    val path: String
)