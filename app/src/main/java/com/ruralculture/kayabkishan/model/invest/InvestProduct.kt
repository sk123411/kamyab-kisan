package com.ruralculture.kayabkishan.model.invest


import com.google.gson.annotations.SerializedName

data class InvestProduct(
    @SerializedName("available_Inventory")
    val availableInventory: String,
    @SerializedName("contract")
    val contract: String,
    @SerializedName("end_date")
    val endDate: String,
    @SerializedName("first_return")
    val firstReturn: String,
    @SerializedName("location")
    val location: String,
    @SerializedName("path")
    val path: String,
    @SerializedName("product_category")
    val productCategory: String,
    @SerializedName("product_detail")
    val productDetail: String,
    @SerializedName("product_id")
    val productId: String,
    @SerializedName("product_image")
    val productImage: String,
    @SerializedName("product_name")
    val productName: String,
    @SerializedName("product_price")
    val productPrice: String,
    @SerializedName("ribbon")
    val ribbon: String,
    @SerializedName("roi")
    val roi: String,
    @SerializedName("start_date")
    val startDate: String,
    @SerializedName("starting_Inventory")
    val startingInventory: String,
    @SerializedName("strtotime")
    val strtotime: String,
    @SerializedName("sub_category_id")
    val subCategoryId: String,
    @SerializedName("like_state")
    val like_state: Int,
    @SerializedName("isSelected")
    var isSelected: String?

){


    companion object {


        fun ProductPriceCompare(): Comparator<InvestProduct> = object :Comparator<InvestProduct> {
            override fun compare(o1: InvestProduct?, o2: InvestProduct?): Int {
                return o1!!.productPrice.toInt() - o2!!.productPrice.toInt()
            }
        }
            fun ProductUnitSoldCompare(): Comparator<InvestProduct> = object :Comparator<InvestProduct> {
                override fun compare(o1: InvestProduct?, o2: InvestProduct?): Int {
                    return o2!!.startingInventory.toInt() - o1!!.startingInventory.toInt()
                }
            }
                fun ProductPriceHighToLowCompare(): Comparator<InvestProduct> = object :Comparator<InvestProduct> {
                    override fun compare(o1: InvestProduct?, o2: InvestProduct?): Int {
                        return o2!!.productPrice.toInt() - o1!!.productPrice.toInt()
                    }
                }

                    fun ProductLongTermCompare(): Comparator<InvestProduct> =
                        object : Comparator<InvestProduct> {
                            override fun compare(o1: InvestProduct?, o2: InvestProduct?): Int {
                                return o2!!.contract.toInt() - o1!!.contract.toInt()
                            }

                        }
//
//        fun ProductIdCompare(id:String): Comparator<InvestProduct> =
//            object : Comparator<InvestProduct> {
//                override fun compare(o1: InvestProduct?, o2: InvestProduct?): Int {
//                    return o2!!.productId.equals(id).compareTo()
//
//                }
//
//            }
                }

}