package com.ruralculture.kayabkishan.model


import androidx.annotation.Nullable
import com.google.gson.annotations.SerializedName

data class ArticleDetailResponse(
    @SerializedName("comment")
    val comment: List<Comment>,
    @SerializedName("comment_count")
    val commentCount: Int,
    @SerializedName("description")
    val description: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("image")
    val image: String,
    @SerializedName("image1")
    val image1: String,
    @SerializedName("image2")
    val image2: String,
    @SerializedName("like_count")
    val likeCount: Int,
    @SerializedName("path")
    val path: String,
    @SerializedName("strtotime")
    val strtotime: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("like_state")
    val like_state: Int,
    @SerializedName("date")
    @Nullable
    val date: String
)