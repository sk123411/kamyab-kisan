package com.ruralculture.kayabkishan.model


import com.google.gson.annotations.SerializedName

data class Article(
    @SerializedName("data")
    val `data`: List<ArticleData>,
    @SerializedName("status")
    val status: Boolean
)