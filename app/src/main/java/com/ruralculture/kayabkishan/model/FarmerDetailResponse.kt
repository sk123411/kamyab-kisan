package com.ruralculture.kayabkishan.model


import com.google.gson.annotations.SerializedName

data class FarmerDetailResponse(
    @SerializedName("farmer")
    val farmer: Farmer
)