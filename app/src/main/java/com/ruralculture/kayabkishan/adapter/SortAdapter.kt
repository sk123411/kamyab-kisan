package com.ruralculture.kayabkishan.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.dashboard.compaigns.SortFragment
import com.ruralculture.kayabkishan.dashboard.repository.DashboardViewModel
import com.ruralculture.kayabkishan.databinding.InvestItemBinding
import com.ruralculture.kayabkishan.databinding.SortCategoryItemBinding
import com.ruralculture.kayabkishan.model.invest.InvestResponse

class SortAdapter constructor(val list: List<InvestResponse>) :RecyclerView.Adapter<SortViewHolder> (){




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SortViewHolder {

        val view = SortCategoryItemBinding.inflate(LayoutInflater.from(parent.context))

        return SortViewHolder(view.root)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: SortViewHolder, position: Int) {



        holder.bindData(list.get(position))

    }


}

class SortViewHolder(view:View) :RecyclerView.ViewHolder(view) {

    val binding = SortCategoryItemBinding.bind(view)

    fun bindData(investResponse: InvestResponse) {
        binding.categoryCheckBox.setText(investResponse.categoryName)

        binding.categoryCheckBox.setOnClickListener {

            SortFragment.selectedIdList!!.add(investResponse.categoryID)

        }




    }






    }







