package com.ruralculture.kayabkishan.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ruralculture.kayabkishan.databinding.ReturnItemBinding
import com.ruralculture.kayabkishan.model.InvestItem

class ReturnAdapter constructor(val list: List<InvestItem>) :RecyclerView.Adapter<ReturnAdapter.ReturnViewHolder> () {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReturnViewHolder {

        val view = ReturnItemBinding.inflate(LayoutInflater.from(parent.context))

        return ReturnViewHolder(view.root)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ReturnViewHolder, position: Int) {

        holder.bindData(list.get(position))

    }



    class ReturnViewHolder(view:View) :RecyclerView.ViewHolder(view) {
        val binding = ReturnItemBinding.bind(view)

        fun bindData(investItem: InvestItem) {

            binding.yearText.setText( investItem.year.toString() + " year")
            binding.yearPercent.setText(investItem.returnPercent.toString() + "%")
            binding.yearPrice.setText("PKR "+ investItem.returnPrice.toString())


        }


    }

}