package com.ruralculture.kayabkishan.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.dashboard.repository.DashboardViewModel
import com.ruralculture.kayabkishan.databinding.InvestItemBinding
import com.ruralculture.kayabkishan.model.invest.InvestResponse

class InvestAdapter constructor(val list: List<InvestResponse>,val dashboardViewModel: DashboardViewModel) :RecyclerView.Adapter<InvestViewHolder> (){




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InvestViewHolder {

        val view = InvestItemBinding.inflate(LayoutInflater.from(parent.context))

        return InvestViewHolder(view.root)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: InvestViewHolder, position: Int) {

            val investItem = list.get(position)

            if (investItem.categoryID.equals(Constant.clicked!!)){
                Log.d("XXXXXXXX", "::"+Constant.clicked)
//                holdersetData(clicked = true, investItem = list.get(position),investAdapter = this)
                holder.binding.buttonInvest.setBackgroundColor(holder.binding.root.resources.getColor(R.color.light_grey))
                holder.binding.buttonInvest.setTextColor(holder.binding.root.resources.getColor(R.color.green))
                holder.binding.buttonInvest.setText(investItem.categoryName)



            }else {
                holder.binding.buttonInvest.setBackgroundColor(holder.binding.root.resources.getColor(R.color.white))
                holder.binding.buttonInvest.setTextColor(holder.binding.root.resources.getColor(R.color.grey))
                holder.binding.buttonInvest.setText(investItem.categoryName)

            }



        holder.binding.buttonInvest.setOnClickListener {


            Constant.clicked = investItem.categoryID
            notifyDataSetChanged()

            dashboardViewModel.getInvestProducts(investItem.categoryID)


        }

    }


}

class InvestViewHolder(view:View) :RecyclerView.ViewHolder(view) {

    val binding = InvestItemBinding.bind(view)


    }







