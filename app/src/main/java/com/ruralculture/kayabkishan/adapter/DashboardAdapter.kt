package com.ruralculture.kayabkishan.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.databinding.ItemListBinding
import com.ruralculture.kayabkishan.model.invest.InvestProduct
import com.squareup.picasso.Picasso

class DashboardAdapter constructor(val list: List<InvestProduct>, val size:Int?) :RecyclerView.Adapter<DashboardViewholder> (){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DashboardViewholder {

        val view = com.ruralculture.kayabkishan.databinding.ItemListBinding.inflate(LayoutInflater.from(parent.context))

        return DashboardViewholder(view.root)

    }

    override fun getItemCount(): Int {
        return size!!
    }

    override fun onBindViewHolder(holder: DashboardViewholder, position: Int) {


        holder.binding.productTitle.text = list.get(position).productName
        holder.binding.prPrice.text = "PKR " + list.get(position).productPrice
        Picasso.get().load(Constant.BASE_IMAGE_PATH+list.get(position).productImage).into(holder.binding.productImage)
        holder.binding.unitsSolds.text = list.get(position).startingInventory + " Units sold"
        holder.binding.unitsLeft.text = list.get(position).availableInventory + " Units left"


        if (list.get(position).firstReturn.isEmpty()){

            holder.binding.returnText.text = "18% " +"/year"
        }else {
            holder.binding.returnText.text = list.get(position).firstReturn +"%"+" /year"
        }



        holder.binding.productInventoryBar.max = list.get(position).availableInventory.toInt()+list.get(position).startingInventory.toInt()
        holder.binding.productInventoryBar.progress = list.get(position).startingInventory.toInt()



        holder.binding.root.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("id",list.get(position).productId)

            Navigation.findNavController(it!!).navigate(R.id.dashboardDetail,bundle)

        }
    }


}

class DashboardViewholder(view:View) :RecyclerView.ViewHolder(view) {

    val binding = ItemListBinding.bind(view)



}
