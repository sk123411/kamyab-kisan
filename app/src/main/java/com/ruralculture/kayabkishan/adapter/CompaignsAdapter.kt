package com.ruralculture.kayabkishan.adapter

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView.OnItemLongClickListener
import android.widget.Toast
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.dashboard.compaigns.AllCompaignsFragment
import com.ruralculture.kayabkishan.dashboard.repository.DashboardViewModel
import com.ruralculture.kayabkishan.databinding.CompaignItemBinding
import com.ruralculture.kayabkishan.model.invest.InvestProduct
import com.squareup.picasso.Picasso


class CompaignsAdapter constructor(val list: List<InvestProduct>,
                                   val dashboardViewModel: DashboardViewModel) :RecyclerView.Adapter<CompaignsViewHolder> (){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CompaignsViewHolder {

        val view = CompaignItemBinding.inflate(LayoutInflater.from(parent.context))
        return CompaignsViewHolder(view.root)


    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: CompaignsViewHolder, position: Int) {

        holder.setCompaignData(list.get(position),dashboardViewModel,this)

    }


}

class CompaignsViewHolder(view:View) :RecyclerView.ViewHolder(view) {

    val binding = CompaignItemBinding.bind(view)


    fun setCompaignData(investProduct: InvestProduct,dashboardViewModel: DashboardViewModel,compaignsAdapter: CompaignsAdapter){


        binding.categoryName.text = investProduct.productCategory
        binding.productTitle.text = investProduct.productName

        Picasso.get().load(Constant.BASE_IMAGE_PATH+investProduct.productImage)

        binding.investmentPrice.text = "PKR "+ investProduct.productPrice

        binding.unitsLeft.text = "/"+investProduct.availableInventory

        binding.unitsSold.text = investProduct.startingInventory

        if (!investProduct.firstReturn.equals("")) {
            binding.returnText.text = investProduct.firstReturn +" %"
        }


        if (!investProduct.contract.equals("")) {
            binding.contractText.text = investProduct.contract + " year"
        }



        binding.productInventoryBar.max = investProduct.startingInventory.toInt() + investProduct.availableInventory.toInt()
        binding.productInventoryBar.progress = investProduct.startingInventory.toInt()
        if (investProduct.like_state==0){


            binding.likeImageButton.setImageResource(R.drawable.heart)


            binding.likeImageButton.setOnClickListener {
                binding.likeImageButton.setImageResource(R.drawable.heart_full)

                dashboardViewModel.addLikeOnProduct(
                    investProduct.productId,
                    Constant.getUserID(it.context).userID.toString()
                )



            }


        }else {

            binding.likeImageButton.setImageResource(R.drawable.heart_full)

            binding.likeImageButton.setOnClickListener {
                binding.likeImageButton.setImageResource(R.drawable.heart)


                dashboardViewModel.addLikeOnProduct(
                    investProduct.productId,
                    Constant.getUserID(it.context).userID.toString()
                )



            }
        }

        Log.d("SELECTEDDDDDDDDDDDd", ":::"+ investProduct.isSelected)










        if (investProduct.isSelected!=null) {

            if (AllCompaignsFragment.selectedProductList!!.size <=2) {


                if (investProduct.isSelected.equals("0")) {

                    binding.checkImage.visibility = View.GONE
                    AllCompaignsFragment.selectedProductList!!.remove(investProduct)

                } else {
                    AllCompaignsFragment.selectedProductList!!.add(investProduct)
                    binding.checkImage.visibility = View.VISIBLE
                    binding.checkImage.setImageResource(R.drawable.check)

                }
            }else {


                Toast.makeText(itemView.context,"You can compare only two products at once", Toast.LENGTH_SHORT).show()

            }

        }

        binding.root.setOnLongClickListener {

            if (investProduct.isSelected.equals("0")) {
                investProduct.isSelected = "1"
                binding.checkImage.visibility = View.VISIBLE
                binding.checkImage.setImageResource(R.drawable.check)

                compaignsAdapter.notifyItemChanged(position)
            } else {
                binding.checkImage.visibility = View.GONE
                investProduct.isSelected = "0"
                compaignsAdapter.notifyItemChanged(position)

            }

            false
        }











        binding.seeMoreButton.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("id",investProduct.productId)

            Navigation.findNavController(it!!).navigate(R.id.dashboardDetail,bundle)

        }

    }




}
