package com.ruralculture.kayabkishan.wishlist

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.dashboard.repository.DashboardViewModel
import com.ruralculture.kayabkishan.databinding.FragmentWishlistBinding


class WishlistFragment : Fragment() {

    lateinit var binding:FragmentWishlistBinding

    private val dashboardViewModel: DashboardViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentWishlistBinding.inflate(layoutInflater)
        // Inflate the layout
        //
        // ut for this fragment

        dashboardViewModel.getFavrouteProducts(Constant.getUserID(requireContext()).userID.toString())


        lifecycleScope.launchWhenStarted {

            dashboardViewModel.dashboardEvent.observe(viewLifecycleOwner, Observer {

                when(it){

                    is DashboardViewModel.DashboardEvent.SuccessFavrouteProducts -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false


                        if (!it.wishlistResponse.status){
                        }else {

                            binding.wishlist.apply {

                                layoutManager = LinearLayoutManager(context)
                                adapter = WishlistAdapter(it.wishlistResponse.data!!.toMutableList(),dashboardViewModel,binding.noWishlistText,this@WishlistFragment)

                            }

                        }
                    }


                    is DashboardViewModel.DashboardEvent.Loading -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true



                    }
                    is DashboardViewModel.DashboardEvent.Empty -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                        binding.noWishlistText.visibility = View.VISIBLE




                    }
                    is DashboardViewModel.DashboardEvent.SuccessBlogResponse -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false



                    }


                }





            })


        }



        return binding.root
    }


}