package com.ruralculture.kayabkishan.wishlist.model


import com.google.gson.annotations.SerializedName
import com.ruralculture.kayabkishan.wishlist.model.Product

data class Data(
    @SerializedName("category_name")
    val categoryName: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("path")
    val path: String,
    @SerializedName("product")
    val product: Product,
    @SerializedName("product_id")
    val productId: String,
    @SerializedName("sub_category_name")
    val subCategoryName: String,
    @SerializedName("user_id")
    val userId: String,
    var isSelected: Boolean = false
)