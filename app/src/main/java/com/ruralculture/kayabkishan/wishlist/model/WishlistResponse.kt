package com.ruralculture.kayabkishan.wishlist.model


import androidx.annotation.Nullable
import com.google.gson.annotations.SerializedName
import com.ruralculture.kayabkishan.wishlist.model.Data

data class WishlistResponse(
    @SerializedName("data")
    @Nullable
    val `data`: List<Data>?,
    @SerializedName("status")
    val status: Boolean
)