package com.ruralculture.kayabkishan.wishlist.model


import com.google.gson.annotations.SerializedName

data class Product(
    @SerializedName("available_Inventory")
    val availableInventory: String,
    @SerializedName("contract")
    val contract: String,
    @SerializedName("end_date")
    val endDate: String,
    @SerializedName("farmer_id")
    val farmerId: String,
    @SerializedName("first_return")
    val firstReturn: String,
    @SerializedName("harvest")
    val harvest: String,
    @SerializedName("lat")
    val lat: String,
    @SerializedName("location")
    val location: String,
    @SerializedName("long")
    val long: String,
    @SerializedName("product_category")
    val productCategory: String,
    @SerializedName("product_detail")
    val productDetail: String,
    @SerializedName("product_id")
    val productId: String,
    @SerializedName("product_image")
    val productImage: String,
    @SerializedName("product_name")
    val productName: String,
    @SerializedName("product_price")
    val productPrice: String,
    @SerializedName("ribbon")
    val ribbon: String,
    @SerializedName("roi")
    val roi: String,
    @SerializedName("start_date")
    val startDate: String,
    @SerializedName("starting_Inventory")
    val startingInventory: String,
    @SerializedName("strtotime")
    val strtotime: String,
    @SerializedName("sub_category_id")
    val subCategoryId: String
)