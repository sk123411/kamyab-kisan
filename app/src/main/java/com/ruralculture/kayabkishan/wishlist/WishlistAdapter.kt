package com.ruralculture.kayabkishan.wishlist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.dashboard.repository.DashboardViewModel
import com.ruralculture.kayabkishan.databinding.CompaignItemBinding
import com.squareup.picasso.Picasso
import com.ruralculture.kayabkishan.wishlist.model.Data

class WishlistAdapter(
    val list: MutableList<Data>,
    val dashboardViewModel: DashboardViewModel,
    val noWishlistText: TextView,
    val wishlistFragment: WishlistFragment
) : RecyclerView.Adapter<WishlistViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WishlistViewHolder {

        val view = CompaignItemBinding.inflate(LayoutInflater.from(parent.context))

        return WishlistViewHolder(view.root)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: WishlistViewHolder, position: Int) {

        holder.setCompaignData(list.get(position), dashboardViewModel, this, list)


    }


}

class WishlistViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    val binding = CompaignItemBinding.bind(view)


    fun setCompaignData(
        investProduct: Data,
        dashboardViewModel: DashboardViewModel,
        wishlistAdapter: WishlistAdapter,
        list: MutableList<Data>
    ) {


        binding.categoryName.text = investProduct.categoryName
        binding.productTitle.text = investProduct.product.productName

        Picasso.get().load(Constant.BASE_IMAGE_PATH + investProduct.product.productImage).into(binding.productImage)



        binding.investmentPrice.text = investProduct.product.productPrice
        binding.unitsLeft.text = "/" + investProduct.product.availableInventory

        binding.unitsSold.text = investProduct.product.startingInventory
        binding.returnText.text = investProduct.product.firstReturn

        binding.contractText.text = investProduct.product.contract


        binding.productInventoryBar.max = investProduct.product.availableInventory.toInt()+investProduct.product.startingInventory.toInt()
        binding.productInventoryBar.progress = investProduct.product.startingInventory.toInt()


        binding.likeImageButton.setImageResource(R.drawable.heart_full)


        binding.likeImageButton.setOnClickListener {

            dashboardViewModel.addLikeOnProduct(
                investProduct.productId,
                Constant.getUserID(it.context).userID.toString()
            )




            list.remove(investProduct)
            wishlistAdapter.notifyItemRemoved(adapterPosition)
            wishlistAdapter.notifyItemChanged(adapterPosition, list.size)


            if (list.isEmpty()) {
                dashboardViewModel.getFavrouteProducts(Constant.getUserID(it.context).userID.toString())

            }


        }

        if (investProduct.isSelected){
            binding.seeMoreButton.text = "i Selected"
        }else {
            binding.seeMoreButton.text = "i Unselected"
        }

        binding.seeMoreButton.setOnClickListener {
          /*  val bundle = Bundle()
            bundle.putString("id", investProduct.productId)

            Navigation.findNavController(it!!).navigate(R.id.dashboardDetail, bundle)*/


            if(investProduct.isSelected){
                investProduct.isSelected = false
               wishlistAdapter.notifyItemChanged(position)
            }else {
                investProduct.isSelected = true
                wishlistAdapter.notifyItemChanged(position)

            }


        }


    }
}









