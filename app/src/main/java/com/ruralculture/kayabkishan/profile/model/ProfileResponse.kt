package com.ruralculture.kayabkishan.profile.model


import com.google.gson.annotations.SerializedName

data class ProfileResponse(
    @SerializedName("active_status")
    val activeStatus: String?,
    @SerializedName("address")
    val address: String?,
    @SerializedName("back_image")
    val backImage: String?,
    @SerializedName("bank_ac")
    val bankAc: String?,
    @SerializedName("birth_place")
    val birthPlace: String?,
    @SerializedName("citizenship")
    val citizenship: String?,
    @SerializedName("document")
    val document: String?,
    @SerializedName("email")
    val email: String?,
    @SerializedName("front_image")
    val frontImage: String?,
    @SerializedName("gender")
    val gender: String?,
    @SerializedName("job_inforamation")
    val jobInforamation: String?,
    @SerializedName("last_education")
    val lastEducation: String?,
    @SerializedName("married")
    val married: String?,
    @SerializedName("path")
    val path: String?,
    @SerializedName("result")
    val result: String?,
    @SerializedName("role")
    val role: String?,
    @SerializedName("strtotime")
    val strtotime: String?,
    @SerializedName("userID")
    val userID: String?,
    @SerializedName("userImage")
    val userImage: String?,
    @SerializedName("userMobile")
    val userMobile: String?,
    @SerializedName("userName")
    val userName: String?,
    @SerializedName("user_password")
    val userPassword: String?
)