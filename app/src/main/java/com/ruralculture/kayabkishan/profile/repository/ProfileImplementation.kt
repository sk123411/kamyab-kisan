package com.ruralculture.kayabkishan.profile.repository

import android.util.Log
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.helper.Resource
import com.ruralculture.kayabkishan.profile.model.ProfileResponse
import com.ruralculture.kayabkishan.wallet.model.WalletResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject

class ProfileImplementation constructor(var profileAPI: ProfileAPI) :
    Profile {
    override suspend fun getProfile(data: HashMap<String, String>): Resource<ProfileResponse> {


        return try {

            val response = profileAPI.getProfile(Constant.API_GET_PROFILE,data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }





    }

    override suspend fun getProfile(
        image: MultipartBody.Part?,
        userId: RequestBody?
    ): Resource<ProfileResponse> {
        return try {
            val response = profileAPI.getProfile(Constant.API_GET_PROFILE, image,userId)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Log.d("e", "" +response.message())

                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Log.d("XXXXXXXXX", "" +e.message)

            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun getWalletBalance(data: HashMap<String, String>): Resource<WalletResponse> {
        return try {
            val response = profileAPI.getWallettBalance(Constant.API_GET_WALLET_BALANCE, data)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Log.d("e", "" +response.message())

                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Log.d("XXXXXXXXX", "" +e.message)

            Resource.Error(e.message ?: "An error occured")
        }    }



    override suspend fun uploadDocument(
        image: MultipartBody.Part?,
        frontImage: MultipartBody.Part?,
        backImage: MultipartBody.Part?,
        userId: RequestBody?
    ): Resource<JSONObject> {
        return try {
            val response = profileAPI.uploadDocument(Constant.API_UPLOAD_DOCUMENT, image,
                frontImage,backImage,userId)
            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())
                Resource.Success(result)

            } else {
                Log.d("e", "" +response.message())

                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Log.d("XXXXXXXXX", "" +e.message)

            Resource.Error(e.message ?: "An error occured")
        }


    }




    override suspend fun getVerificationStatus(data: HashMap<String, String>): Resource<ProfileResponse> {

        return try {

            val response = profileAPI.getVerificationStatus(Constant.API_VERIFICATION_STATUS,data)

            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)

            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }

    }


}