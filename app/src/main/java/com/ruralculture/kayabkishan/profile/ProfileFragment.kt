package com.ruralculture.kayabkishan.profile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import com.github.dhaval2404.imagepicker.ImagePicker
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.MainActivity
import com.ruralculture.kayabkishan.fragments.login.LoginActivity
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.databinding.FragmentProfileBinding
import com.ruralculture.kayabkishan.profile.repository.ProfileViewModel
import com.squareup.picasso.Picasso
import io.paperdb.Paper
import java.io.File

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ProfileFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ProfileFragment : Fragment() {

    lateinit var binding: FragmentProfileBinding

    private val profileViewModel: ProfileViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        binding = FragmentProfileBinding.inflate(layoutInflater)
        profileViewModel.getProfiles(Constant.getUserID(requireContext()).userID.toString())







        lifecycleScope.launchWhenStarted {

            profileViewModel.profileEvent.observe(viewLifecycleOwner, Observer {

                when (it) {


                    is ProfileViewModel.ProfileEvent.Success -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                        Toast.makeText(requireContext(), "Profile updated", Toast.LENGTH_LONG)
                            .show()

                        Picasso.get().load(Constant.BASE_IMAGE_PATH + it.profleResponse.userImage)
                            .placeholder(R.drawable.kisan).into(binding.profileImage)


                        if (!it.profleResponse.userName.equals("")&&!it.profleResponse.userImage.equals("")){

                            binding.checkOne.setImageResource(R.drawable.check)
                            binding.checkOneTextView.setText("Personal info added")

                        }

                        if (it.profleResponse.activeStatus.equals("1")){
                            binding.checkTwo.setImageResource(R.drawable.check)
                            binding.checkTwoTextView.setText("Verified")


                        }else {

                            binding.checkTwoTextView.setText("Identity not verified")

                        }

                        if (!it.profleResponse.bankAc.equals("")){
                            binding.checkThree.setImageResource(R.drawable.check)
                            binding.checkThreeTextView.setText("Bank Added")

                        }


                    }

                    is ProfileViewModel.ProfileEvent.Loading -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true

                    }

                }

            })

            profileViewModel.imageFile.observe(viewLifecycleOwner, Observer { p ->


                profileViewModel.updateProfileImage(
                    Constant.getUserID(requireContext()).userID.toString(),
                    p
                )


            })
        }

        binding.profileImage.setOnClickListener {


            ImagePicker.with(this)
                .galleryOnly()
                .compress(1024)
                .start()

        }


        binding.wishlistButton.setOnClickListener {

            Navigation.findNavController(it).navigate(R.id.wishlistFragment)
        }


        binding.walletButton.setOnClickListener {

            Navigation.findNavController(it).navigate(R.id.walletFragment)
        }



        binding.logoutButton.setOnClickListener {

            Paper.init(activity)
            Paper.book().delete(Constant.USER_LOGGED)
            it.context.startActivity(
                Intent(
                    context,
                    LoginActivity::class.java
                ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            )

        }


        binding.settingsButton.setOnClickListener {

            Navigation.findNavController(it).navigate(R.id.settingsFragment)

        }
        return binding.root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val fileUri = data?.data
            binding.profileImage.setImageURI(fileUri)
            //You can get File object from intent
            val file: File = ImagePicker.getFile(data)!!

            //You can also get File Path from intent
            profileViewModel.sendImageFile(file)


            val filePath: String = ImagePicker.getFilePath(data)!!
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(context, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(context, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }


    override fun onResume() {
        super.onResume()

        val bottomSupport = MainActivity.bottomNavigation.menu.findItem(R.id.bottom_profile)
        bottomSupport.isChecked = true
    }
}