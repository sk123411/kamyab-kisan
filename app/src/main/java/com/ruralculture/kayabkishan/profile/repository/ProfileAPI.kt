package com.ruralculture.kayabkishan.profile.repository

import com.ruralculture.kayabkishan.profile.model.ProfileResponse
import com.ruralculture.kayabkishan.wallet.model.WalletResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Response
import retrofit2.http.*

interface ProfileAPI {


    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun getProfile(
        @Query("action") action: String?,
        @FieldMap data: HashMap<String, String>
    ): Response<ProfileResponse>

    @Multipart
    @POST("api/process.php")
    suspend fun getProfile(
        @Query("action") updateProfile: String?,
        @Part image: MultipartBody.Part?,
        @Part("user_id") userId: RequestBody?
    ): Response<ProfileResponse>

    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun getWallettBalance(
        @Query("action") action: String?,
        @FieldMap data: HashMap<String, String>
    ): Response<WalletResponse>


    @Multipart
    @POST("api/process.php")
    suspend fun uploadDocument(
        @Query("action") updateProfile: String?,
        @Part image: MultipartBody.Part?,
        @Part frontImage: MultipartBody.Part?,
        @Part backImage: MultipartBody.Part?,
        @Part("user_id") userId: RequestBody?
    ): Response<JSONObject>


    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun getVerificationStatus(
        @Query("action") action: String?,
        @FieldMap data: HashMap<String, String>
    ): Response<ProfileResponse>


}