package com.ruralculture.kayabkishan.profile

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.databinding.FragmentPersonalInfoBinding
import com.ruralculture.kayabkishan.profile.repository.ProfileViewModel


class PersonalInfoFragment : Fragment() {
    private val profileViewModel: ProfileViewModel by viewModels()

    lateinit var binding:FragmentPersonalInfoBinding



    var gender:String?=null
    var citizenShip:String?=null
    var married:String?=null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentPersonalInfoBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment

        profileViewModel.getProfiles(Constant.getUserID(requireContext()).userID.toString())





        lifecycleScope.launchWhenStarted {

            profileViewModel.profileEvent.observe(viewLifecycleOwner, Observer {



                when(it){

                    is ProfileViewModel.ProfileEvent.Success -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false



                        binding.nameEdit.setText(it.profleResponse.userName)

                        binding.birthPlaceEdit.setText(it.profleResponse.birthPlace)
                        binding.educationEdit.setText(it.profleResponse.lastEducation)
                        binding.addressPlaceEdit.setText(it.profleResponse.address)


                        if (it.profleResponse.gender.equals("male")){

                            binding.maleRadio.isChecked = true
                            binding.femaleRadio.isChecked = false
                            gender = "male"

                        }else {

                            binding.maleRadio.isChecked = false
                            binding.femaleRadio.isChecked = true
                            gender = "female"
                        }


                        if (it.profleResponse.married.equals("married")){

                            binding.marriedRadio.isChecked = true
                            binding.singleRadio.isChecked = false
                            married = "married"

                        }else {

                            binding.marriedRadio.isChecked = false
                            binding.singleRadio.isChecked = true
                            married = "single"
                        }

                        if (it.profleResponse.citizenship.equals("indian")){

                            binding.indiaRadio.isChecked = true
                            binding.nriRadio.isChecked = false

                            citizenShip = "indian"


                        }else {

                            binding.indiaRadio.isChecked = false
                            binding.nriRadio.isChecked = true
                            citizenShip = "nro"
                        }





                    }

                    is ProfileViewModel.ProfileEvent.Loading -> {


                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true
                    }


                }


            })




            binding.maleRadio.setOnClickListener {

                binding.maleRadio.isChecked = true
                binding.femaleRadio.isChecked = false
                gender = "male"


            }
            binding.femaleRadio.setOnClickListener {


                binding.maleRadio.isChecked = false
                binding.femaleRadio.isChecked = true
                gender = "female"

            }




            binding.indiaRadio.setOnClickListener {

                binding.indiaRadio.isChecked = true
                binding.nriRadio.isChecked = false
                citizenShip = "indian"


            }
            binding.nriRadio.setOnClickListener {
                binding.nriRadio.isChecked = true
                binding.indiaRadio.isChecked = false
                citizenShip = "nri"

            }




            binding.marriedRadio.setOnClickListener {
                binding.marriedRadio.isChecked = true
                binding.singleRadio.isChecked = false
                married = "married"

            }

            binding.singleRadio.setOnClickListener {
                binding.marriedRadio.isChecked = false
                binding.singleRadio.isChecked = true
                married = "single"


            }















            binding.saveButton.setOnClickListener {



                if (binding.addressPlaceEdit.text.toString().isNotEmpty()||binding.birthPlaceEdit.text.toString().isNotEmpty()||binding.educationEdit.text.toString().isNotEmpty()||binding.nameEdit.text.toString().isNotEmpty()) {


                    profileViewModel.updateProfile(
                        userID = Constant.getUserID(requireContext()).userID.toString(),
                        address = binding.addressPlaceEdit.text.toString(),
                        birthPlace = binding.birthPlaceEdit.text.toString(),
                        gender = gender,
                        citizenShip = citizenShip,
                        education = binding.educationEdit.text.toString(),
                        married = married,
                        name = binding.nameEdit.text.toString()
                    )


                }


            }






        }





        return binding.root
    }


    enum class PROFILERADIOS {
        MALE,FEMALE,MARRIED,SINGLE,INDIAN,NRI
    }



}