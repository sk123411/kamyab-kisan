package com.ruralculture.kayabkishan.profile

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.WebviewActivity
import com.ruralculture.kayabkishan.databinding.FragmentSettingsBinding
import com.ruralculture.kayabkishan.databinding.ProfileInfoDialogBinding
import com.ruralculture.kayabkishan.profile.repository.ProfileViewModel

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [SettingsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */

const val PRIVACY_POLICY_URL = "https://ruparnatechnology.com/Kamyabkisan/privacy.php"
const val CONTACT_URL = "https://ruparnatechnology.com/Kamyabkisan/contactus.php"
const val TERMS_AND_CONDITION_URL = "https://ruparnatechnology.com/Kamyabkisan/terms.php"
const val FAQ_URL = "https://ruparnatechnology.com/Kamyabkisan/faq.php"
const val ABOUT_US_URL = "https://ruparnatechnology.com/Kamyabkisan/about.php"

class SettingsFragment : Fragment() {
    private val profileViewModel: ProfileViewModel by viewModels()
    lateinit var binding: FragmentSettingsBinding

    var contact:String?=null
    var bankAC:String?=null
    var j_info:String?=null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSettingsBinding.inflate(layoutInflater)


        profileViewModel.getProfiles(Constant.getUserID(requireContext()).userID.toString())









        lifecycleScope.launchWhenStarted {


            profileViewModel.profileEvent.observe(viewLifecycleOwner, Observer {


                when(it){

                    is ProfileViewModel.ProfileEvent.Success -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                        contact = it.profleResponse.userMobile
                        bankAC = it.profleResponse.bankAc
                        j_info = it.profleResponse.jobInforamation




                    }

                    is ProfileViewModel.ProfileEvent.Loading -> {


                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true
                    }


                }



            })





        }






        // Inflate the layout for this fragment

        binding.personalInfoButton.setOnClickListener {

            Navigation.findNavController(it).navigate(R.id.personalInfoFragment)
        }


        binding.privacyPolicyRoot.setOnClickListener {

          startActivity(Intent(requireActivity(),WebviewActivity::class.java).putExtra("url",
              PRIVACY_POLICY_URL))
        }

        binding.contactRootLayout.setOnClickListener {
            startActivity(Intent(requireActivity(),WebviewActivity::class.java).putExtra("url",
                CONTACT_URL))
        }

        binding.termsAndConditionLayout.setOnClickListener {
            startActivity(Intent(requireActivity(),WebviewActivity::class.java).putExtra("url",
                TERMS_AND_CONDITION_URL))
        }

        binding.faqRootLayout.setOnClickListener {
            startActivity(Intent(requireActivity(),WebviewActivity::class.java).putExtra("url",
                FAQ_URL))
        }







        binding.personalInfoButton.setOnClickListener {


            Navigation.findNavController(it).navigate(R.id.personalInfoFragment)




        }

        binding.contactInfoButton.setOnClickListener {
            openProfileInfo(PROFILE.CONTACT)
        }


        binding.bankInfoButton.setOnClickListener {
            openProfileInfo(PROFILE.BANK_ACCOUNT)
        }


        binding.jobInfoButton.setOnClickListener {
            openProfileInfo(PROFILE.JOB_INFORMATION)
        }









        return binding.root
    }

    private fun openProfileInfo(profile: PROFILE) {


        val binding = ProfileInfoDialogBinding.inflate(layoutInflater)
        val bottomSheetDialog = BottomSheetDialog(requireContext())
        bottomSheetDialog.setContentView(binding.root)
        bottomSheetDialog.show()




        when (profile) {


            PROFILE.CONTACT -> {


                binding.labelText.setText("Change Mobile")
                binding.usernameEdit.setText(contact)

            }

            PROFILE.JOB_INFORMATION -> {

                binding.labelText.setText("Change job information")

                binding.usernameEdit.setText(j_info)

            }

            PROFILE.BANK_ACCOUNT -> {

                binding.labelText.setText("Change bank ac information")

                binding.usernameEdit.setText(bankAC)

            }


        }


        binding.saveButton.setOnClickListener {


            if (!binding.usernameEdit.text!!.isEmpty()) {



                when (profile) {


                    PROFILE.CONTACT -> {


                        contact = binding.usernameEdit.text.toString()
                        profileViewModel.updateContact(
                            Constant.getUserID(requireContext()).userID.toString(),
                            contact
                        )
                        bottomSheetDialog.dismiss()

                    }

                    PROFILE.JOB_INFORMATION -> {

                        j_info = binding.usernameEdit.text.toString()
                        profileViewModel.updateJobInfo(
                            Constant.getUserID(requireContext()).userID.toString(),
                            j_info
                        )


                        bottomSheetDialog.dismiss()

                    }

                    PROFILE.BANK_ACCOUNT -> {

                        bankAC = binding.usernameEdit.text.toString()
                        profileViewModel.updateBankAC(
                            Constant.getUserID(requireContext()).userID.toString(),
                            bankAC
                        )


                        bottomSheetDialog.dismiss()

                    }


                }
            }
        }
    }


    enum class PROFILE {
        USERNAME,
        CONTACT,
        JOB_INFORMATION,
        BANK_ACCOUNT

    }


}