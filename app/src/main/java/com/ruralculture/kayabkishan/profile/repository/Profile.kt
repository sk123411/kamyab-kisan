package com.ruralculture.kayabkishan.profile.repository

import com.ruralculture.kayabkishan.helper.Resource
import com.ruralculture.kayabkishan.profile.model.ProfileResponse
import com.ruralculture.kayabkishan.wallet.model.WalletResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject

interface Profile {


    suspend fun getProfile(data:HashMap<String,String>): Resource<ProfileResponse>
    suspend fun getProfile(image: MultipartBody.Part?,
                            userId: RequestBody?): Resource<ProfileResponse>
    suspend fun getWalletBalance(data:HashMap<String,String>): Resource<WalletResponse>

    suspend fun uploadDocument(image: MultipartBody.Part?,
                               frontImage: MultipartBody.Part?,
                               backImage: MultipartBody.Part?,
                               userId: RequestBody?): Resource<JSONObject>

    suspend fun getVerificationStatus(data:HashMap<String,String>): Resource<ProfileResponse>

}