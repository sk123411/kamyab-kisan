package com.ruralculture.kayabkishan.profile.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ruralculture.kayabkishan.helper.Resource
import com.ruralculture.kayabkishan.profile.model.ProfileResponse
import com.ruralculture.kayabkishan.retofit.RetrofitInstance
import com.ruralculture.kayabkishan.wallet.model.WalletResponse
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import java.io.File


class ProfileViewModel : ViewModel(){




    val profileEventMutableData  = MutableLiveData<ProfileEvent>()

    var profileAPI: ProfileAPI?=null
    var profile: Profile?=null


    val profileEvent:LiveData<ProfileEvent>
        get() = profileEventMutableData




    init {
        profileAPI = RetrofitInstance.getRetrofitInstance().create(ProfileAPI::class.java)
        profile =
            ProfileImplementation(
                profileAPI!!
            )


    }

    private val imageFiles_ = MutableLiveData<List<File>>()
    private val imageFile_ = MutableLiveData<File>()


    val imageFiles: LiveData<List<File>>
        get() = imageFiles_


    val imageFile: LiveData<File>
        get() = imageFile_







    sealed class ProfileEvent{


        class Success(val profleResponse: ProfileResponse):
            ProfileEvent()
        class SuccessVerification(val userVerificationResponse: JSONObject):
            ProfileEvent()
        class SuccessWalletBalance(val walletResponse: WalletResponse):
            ProfileEvent()

        class Failure(m:String):
            ProfileEvent()
        object Empty: ProfileEvent()
        object Loading:
            ProfileEvent()


    }




    fun getProfiles(userID:String?){


        val data = HashMap<String,String>()
        data.put("user_id",userID!!)

        profileEventMutableData.value =
            ProfileEvent.Loading


        viewModelScope.launch {


            val response = profile?.getProfile(data)


            when(response){


                is Resource.Success -> {

                    if (response.data?.result.equals("successfully")) {


                        profileEventMutableData.value =
                            ProfileEvent.Success(
                                response.data!!
                            )
                    }else {

                        profileEventMutableData.value =
                            ProfileEvent.Failure(
                                response.data!!.result!!
                            )

                    }
                }


                is Resource.Error -> {



                    profileEventMutableData.value =
                        ProfileEvent.Failure(
                            response.message!!
                        )

                }



            }


        }

    }




    fun getWalletBalance(userID:String?){


        val data = HashMap<String,String>()
        data.put("user_id","2")

        profileEventMutableData.value =
            ProfileEvent.Loading


        viewModelScope.launch {


            val response = profile?.getWalletBalance(data)


            when(response){


                is Resource.Success -> {



                        profileEventMutableData.value =
                            ProfileEvent.SuccessWalletBalance(
                                response.data!!
                            )

                    }



                is Resource.Error -> {



                    profileEventMutableData.value =
                        ProfileEvent.Failure(
                            response.message!!
                        )

                }



            }


        }

    }

    fun getVerificationStatus(userID:String?){


        val data = HashMap<String,String>()
        data.put("user_id",userID!!)

        profileEventMutableData.value =
            ProfileEvent.Loading


        viewModelScope.launch {


            val response = profile?.getVerificationStatus(data)


            when(response){


                is Resource.Success -> {


                        profileEventMutableData.value =
                            ProfileEvent.Success(
                                response.data!!
                            )

                    }



                is Resource.Error -> {



                    profileEventMutableData.value =
                        ProfileEvent.Failure(
                            response.message!!
                        )

                }



            }


        }

    }



    fun updateProfile(userID:String?, name:String?,birthPlace:String?,
                      address:String?,gender:String?,education:String?,
                      married:String?,citizenShip:String?){


        val data = HashMap<String,String>()
        data.put("user_id",userID!!)
        data.put("name",name!!)
        data.put("birth_place",birthPlace!!)
        data.put("gender",gender!!)
        data.put("last_education",education!!)
        data.put("married",married!!)
        data.put("citizenship",citizenShip!!)
        data.put("address",address!!)



        profileEventMutableData.value =
            ProfileEvent.Loading


        viewModelScope.launch {


            val response = profile?.getProfile(data)


            when(response){


                is Resource.Success -> {

                    if (response.data?.result.equals("successfully")) {


                        profileEventMutableData.value =
                            ProfileEvent.Success(
                                response.data!!
                            )
                    }else {

                        profileEventMutableData.value =
                            ProfileEvent.Failure(
                                response.data!!.result!!
                            )

                    }
                }


                is Resource.Error -> {



                    profileEventMutableData.value =
                        ProfileEvent.Failure(
                            response.message!!
                        )

                }



            }


        }

    }

    fun updateContact(userID:String?,contact:String?){


        val data = HashMap<String,String>()
        data.put("user_id",userID!!)
        data.put("mobile",contact!!)

        profileEventMutableData.value =
            ProfileEvent.Loading


        viewModelScope.launch {


            val response = profile?.getProfile(data)


            when(response){


                is Resource.Success -> {

                    if (response.data?.result.equals("successfully")) {


                        profileEventMutableData.value =
                            ProfileEvent.Success(
                                response.data!!
                            )
                    }else {

                        profileEventMutableData.value =
                            ProfileEvent.Failure(
                                response.data!!.result!!
                            )

                    }
                }


                is Resource.Error -> {



                    profileEventMutableData.value =
                        ProfileEvent.Failure(
                            response.message!!
                        )

                }



            }


        }

    }
    fun updateBankAC(userID:String?,bankAC:String?){


        val data = HashMap<String,String>()
        data.put("user_id",userID!!)
        data.put("bank_ac",bankAC!!)

        profileEventMutableData.value =
            ProfileEvent.Loading


        viewModelScope.launch {


            val response = profile?.getProfile(data)


            when(response){


                is Resource.Success -> {

                    if (response.data?.result.equals("successfully")) {


                        profileEventMutableData.value =
                            ProfileEvent.Success(
                                response.data!!
                            )
                    }else {

                        profileEventMutableData.value =
                            ProfileEvent.Failure(
                                response.data!!.result!!
                            )

                    }
                }


                is Resource.Error -> {



                    profileEventMutableData.value =
                        ProfileEvent.Failure(
                            response.message!!
                        )

                }



            }


        }

    }
    fun updateJobInfo(userID:String?,jobInfo:String?){


        val data = HashMap<String,String>()
        data.put("user_id",userID!!)
        data.put("job_inforamation",jobInfo!!)

        profileEventMutableData.value =
            ProfileEvent.Loading


        viewModelScope.launch {


            val response = profile?.getProfile(data)


            when(response){


                is Resource.Success -> {

                    if (response.equals("successfully")) {


                        profileEventMutableData.value =
                            ProfileEvent.Success(
                                response.data!!
                            )
                    }else {

                        profileEventMutableData.value =
                            ProfileEvent.Failure(
                                response.data!!.result!!
                            )

                    }
                }


                is Resource.Error -> {



                    profileEventMutableData.value =
                        ProfileEvent.Failure(
                            response.message!!
                        )

                }



            }


        }

    }

    fun updateProfileImage(userID: String?,file: File?){


        val storeImage: RequestBody =  RequestBody.create(
            "image/jpg".toMediaTypeOrNull(),
            file!!)


        val storeImageBody: MultipartBody.Part =
            MultipartBody.Part.createFormData(
               "image",
                file.getName(), storeImage)





        val userBody = getMultiPartFormRequestBody(userID)

        profileEventMutableData.value =
            ProfileEvent.Loading


        viewModelScope.launch {



            val response = profile?.getProfile(
                storeImageBody, userBody)


            when(response){


                is Resource.Success -> {

        //            if (response.equals("successfully")) {


                        profileEventMutableData.value =
                            ProfileEvent.Success(
                                response.data!!
                            )
//                    }else {
//
//                        profileEventMutableData.value = ProfileEvent.Failure(response.data!!.result)
//
//                    }
                }


                is Resource.Error -> {



                    profileEventMutableData.value =
                        ProfileEvent.Failure(
                            response.message!!
                        )

                }



            }


        }

    }







    fun uploadDocument(userID: String?,frontImageFile:File?,backImageFile:File?,image: File?){
        Log.d("PARAMSSS", "RUNNNNNNNNNNn")


        val storeImage: RequestBody =  RequestBody.create(
            "image/jpg".toMediaTypeOrNull(),
            image!!)

        val storeImageBody: MultipartBody.Part =
            MultipartBody.Part.createFormData(
                "image",
                image.getName(), storeImage)


        val frontImage: RequestBody =  RequestBody.create(
            "image/jpg".toMediaTypeOrNull(),
            frontImageFile!!)

        val frontImageBody: MultipartBody.Part =
            MultipartBody.Part.createFormData(
                "front_image",
                frontImageFile.getName(), frontImage)


        val backImage: RequestBody =  RequestBody.create(
            "image/jpg".toMediaTypeOrNull(),
            backImageFile!!)

        val backImageBody: MultipartBody.Part =
            MultipartBody.Part.createFormData(
                "back_image",
                backImageFile.getName(), backImage)

        Log.d("PARAMSSS", "FPI" + frontImageBody.body.toString())
        Log.d("PARAMSSS", "BPI" + backImageBody.body.toString())
        Log.d("PARAMSSS", "TPI" + storeImageBody.body.toString())
        Log.d("PARAMSSS", "UID" + userID)




        val userBody = getMultiPartFormRequestBody(userID)

        profileEventMutableData.value =
            ProfileEvent.Loading


        viewModelScope.launch {

            val response = profile?.uploadDocument(
                storeImageBody,frontImageBody,backImageBody, userBody)

            when(response){

                is Resource.Success -> {

                    profileEventMutableData.value = ProfileEvent.SuccessVerification(response.data!!)

//                    }else {
//
//                        profileEventMutableData.value = ProfileEvent.Failure(response.data!!.result)
//
//                    }
                }
                is Resource.Error -> {

                    profileEventMutableData.value =
                        ProfileEvent.Failure(
                            response.message!!
                        )

                }



            }


        }

    }

    fun getMultiPartFormRequestBody(tag:String?):RequestBody{
        return RequestBody.create(MultipartBody.FORM, tag!!)

    }

    fun sendImageFiles(file: List<File>) {

        imageFiles_.value = file
    }
    fun sendImageFile(file:File) {
        imageFile_.value = file
    }


}