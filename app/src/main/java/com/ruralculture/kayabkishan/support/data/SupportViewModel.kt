package com.ruralculture.kayabkishan.support.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ruralculture.kayabkishan.helper.Resource
import com.ruralculture.kayabkishan.order.model.OrderListResponse
import com.ruralculture.kayabkishan.order.model.OrderResponse
import com.ruralculture.kayabkishan.retofit.RetrofitInstance
import com.ruralculture.kayabkishan.support.model.MessageInsertResponse
import com.ruralculture.kayabkishan.support.model.ShowAllMessagesResponse
import com.ruralculture.kayabkishan.support.model.ShowAllMessagesUserResponse
import kotlinx.coroutines.launch

class SupportViewModel : ViewModel() {


    private val supportEvent_ = MutableLiveData<OrderEvent>()
    var supportAPI: SupportAPI? = null
    var support: Support? = null


    val supportEvent: LiveData<OrderEvent>
        get() = supportEvent_


    init {
        supportAPI = RetrofitInstance.getRetrofitInstance().create(SupportAPI::class.java)
        support = SupportImplemenation(supportAPI!!)


    }


    sealed class OrderEvent {
        class SuccessAllMessagesUser(val showAllMessagesUserResponse: ShowAllMessagesUserResponse) :
            OrderEvent()

        class SuccessMessageSent(val messageInsertResponse: MessageInsertResponse) : OrderEvent()
        class SuccessAllMessages(val showAllMessagesResponse: ShowAllMessagesResponse) :
            OrderEvent()

        class Failure(val message: String?) : OrderEvent()
        object Loading : OrderEvent()
        object Empty : OrderEvent()
    }


    fun sendMessage(userID: String?, senderID: String?, receiverID: String?, message: String?) {
        supportEvent_.value = OrderEvent.Loading

        val map = HashMap<String, String>()
        map.put("user_id", userID!!)
        map.put("sender_id", senderID!!)
        map.put("reciver_id", receiverID!!)
        map.put("messages", message!!)



        viewModelScope.launch {

            val response = support?.sendMessage(data = map)

            when (response) {

                is Resource.Success -> {


                    supportEvent_.value = OrderEvent.SuccessMessageSent(response.data!!)


                }

                is Resource.Error -> {
                    supportEvent_.value = OrderEvent.Failure("Sign in success")


                }


            }


        }


    }


    fun getAllMessagesUser(userID: String?, senderID: String?, receiverID: String?) {
        supportEvent_.value = OrderEvent.Loading

        val map = HashMap<String, String>()
        map.put("user_id", userID!!)
        map.put("sender_id", senderID!!)
        map.put("reciver_id", receiverID!!)

        viewModelScope.launch {

            val response = support?.getAllMessagesUser(data = map)

            when (response) {

                is Resource.Success -> {


                    supportEvent_.value = OrderEvent.SuccessAllMessagesUser(response.data!!)


                }

                is Resource.Error -> {
                    supportEvent_.value = OrderEvent.Failure("Sign in success")


                }


            }


        }


    }

    fun getAllMessages(userID: String?) {
        supportEvent_.value = OrderEvent.Loading

        val map = HashMap<String, String>()
        map.put("user_id", userID!!)


        viewModelScope.launch {

            val response = support?.getAllMessages(data = map)

            when (response) {

                is Resource.Success -> {


                    if (response.data!!.result) {

                        supportEvent_.value = OrderEvent.SuccessAllMessages(response.data!!)
                    }else {

                        supportEvent_.value = OrderEvent.Failure("Server errror occured, Please try again")

                    }

                }

                is Resource.Error -> {
                    supportEvent_.value = OrderEvent.Failure("Sign in success")


                }

            }

        }


    }

}

