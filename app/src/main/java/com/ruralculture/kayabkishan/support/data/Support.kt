package com.ruralculture.kayabkishan.support.data

import com.ruralculture.kayabkishan.helper.Resource
import com.ruralculture.kayabkishan.order.model.OrderListResponse
import com.ruralculture.kayabkishan.order.model.OrderResponse
import com.ruralculture.kayabkishan.support.model.MessageInsertResponse
import com.ruralculture.kayabkishan.support.model.ShowAllMessagesResponse
import com.ruralculture.kayabkishan.support.model.ShowAllMessagesUserResponse

interface Support {

    suspend fun sendMessage(data:HashMap<String,String>): Resource<MessageInsertResponse>
    suspend fun getAllMessagesUser(data:HashMap<String,String>): Resource<ShowAllMessagesUserResponse>
    suspend fun getAllMessages(data:HashMap<String,String>): Resource<ShowAllMessagesResponse>


}