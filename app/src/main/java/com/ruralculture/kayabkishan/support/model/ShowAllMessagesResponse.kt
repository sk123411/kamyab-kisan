package com.ruralculture.kayabkishan.support.model


import com.google.gson.annotations.SerializedName

data class ShowAllMessagesResponse(
    @SerializedName("data")
    val `data`: List<DataX>,
    @SerializedName("result")
    val result: Boolean
)