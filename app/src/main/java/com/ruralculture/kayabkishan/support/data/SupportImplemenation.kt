package com.ruralculture.kayabkishan.support.data

import android.util.Log
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.helper.Resource
import com.ruralculture.kayabkishan.order.model.OrderListResponse
import com.ruralculture.kayabkishan.order.model.OrderResponse
import com.ruralculture.kayabkishan.support.model.MessageInsertResponse
import com.ruralculture.kayabkishan.support.model.ShowAllMessagesResponse
import com.ruralculture.kayabkishan.support.model.ShowAllMessagesUserResponse

class SupportImplemenation  constructor(var supportAPI: SupportAPI) : Support {
    override suspend fun sendMessage(data: HashMap<String, String>): Resource<MessageInsertResponse> {
        return try {

            val response = supportAPI.sendMessage(Constant.API_SEND_MESSAGE,data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result!!)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }

    }

    override suspend fun getAllMessagesUser(data: HashMap<String, String>): Resource<ShowAllMessagesUserResponse> {
        return try {

            val response = supportAPI.getAllMessagesUser(Constant.API_ALL_MESSAGES_USER, data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result!!)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }

    }

    override suspend fun getAllMessages(data: HashMap<String, String>): Resource<ShowAllMessagesResponse> {
        return try {

            val response = supportAPI.getAllMessages(Constant.API_ALL_MESSAGES,data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result!!)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }      }


}