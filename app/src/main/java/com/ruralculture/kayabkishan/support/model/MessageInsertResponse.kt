package com.ruralculture.kayabkishan.support.model


import com.google.gson.annotations.SerializedName

data class MessageInsertResponse(
    @SerializedName("date_time")
    val dateTime: String,
    @SerializedName("dates")
    val dates: String,
    @SerializedName("file")
    val `file`: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("path")
    val path: String,
    @SerializedName("reciver_id")
    val reciverId: String,
    @SerializedName("result")
    val result: String,
    @SerializedName("sender_id")
    val senderId: String,
    @SerializedName("strtotime")
    val strtotime: String
)