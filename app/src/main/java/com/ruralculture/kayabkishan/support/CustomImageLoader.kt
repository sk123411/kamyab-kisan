package com.ruralculture.kayabkishan.support

import android.content.Context
import android.graphics.Bitmap
import android.widget.ImageView
import androidx.annotation.Nullable
import androidx.core.graphics.drawable.toBitmap
import com.freshchat.consumer.sdk.FreshchatImageLoader
import com.freshchat.consumer.sdk.FreshchatImageLoaderRequest
import com.ruralculture.kayabkishan.R


class CustomImageLoader(val context: Context) : FreshchatImageLoader {
    override fun load(request: FreshchatImageLoaderRequest, imageView: ImageView) {
        // your code to download image and set to imageView
    }

    @Nullable
    override fun get(request: FreshchatImageLoaderRequest): Bitmap? {


        return context.resources.getDrawable(R.drawable.id_card).toBitmap(100,100,null)


    }

    override fun fetch(request: FreshchatImageLoaderRequest) {
        // code to download image
    }
}