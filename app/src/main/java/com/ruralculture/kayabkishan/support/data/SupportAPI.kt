package com.ruralculture.kayabkishan.support.data

import com.ruralculture.kayabkishan.helper.Resource
import com.ruralculture.kayabkishan.order.model.OrderListResponse
import com.ruralculture.kayabkishan.order.model.OrderResponse
import com.ruralculture.kayabkishan.support.model.MessageInsertResponse
import com.ruralculture.kayabkishan.support.model.ShowAllMessagesResponse
import com.ruralculture.kayabkishan.support.model.ShowAllMessagesUserResponse
import retrofit2.Response
import retrofit2.http.*

interface SupportAPI {


    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun sendMessage(@Query("action") action:String?, @FieldMap data:HashMap<String,String>): Response<MessageInsertResponse>


    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun getAllMessagesUser(@Query("action") action:String?, @FieldMap data:HashMap<String,String>): Response<ShowAllMessagesUserResponse>

    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun getAllMessages(@Query("action") action:String?, @FieldMap data:HashMap<String,String>): Response<ShowAllMessagesResponse>




}