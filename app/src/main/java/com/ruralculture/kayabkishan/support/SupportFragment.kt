package com.ruralculture.kayabkishan.support

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.freshchat.consumer.sdk.*
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.databinding.FragmentSupportBinding
import com.ruralculture.kayabkishan.databinding.SendMessageBottomDialogBinding
import com.ruralculture.kayabkishan.support.adapter.ChatUsersListAdapter
import com.ruralculture.kayabkishan.support.data.SupportViewModel
import io.paperdb.Paper


class SupportFragment : Fragment() {


    lateinit var binding:FragmentSupportBinding
    private val supportViewModel:SupportViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentSupportBinding.inflate(layoutInflater)

        setupFreshchat()
        Freshchat.showConversations(requireContext());


      //  supportViewModel.getAllMessages(Constant.getUserID(requireContext()).userID.toString())


        // Inflate the layout for this fragment


        lifecycleScope.launchWhenStarted {


            supportViewModel.supportEvent.observe(viewLifecycleOwner, Observer {
                activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false



                when(it){

                    is SupportViewModel.OrderEvent.SuccessAllMessages -> {


                        binding.chatUsersList.apply {

                            layoutManager = LinearLayoutManager(context)
                            adapter = ChatUsersListAdapter(it.showAllMessagesResponse.data)

                        }
                    }

                    is SupportViewModel.OrderEvent.Loading -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true


                    }


                    is SupportViewModel.OrderEvent.Failure -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true
                        supportViewModel.getAllMessages(Constant.getUserID(requireContext()).userID.toString())

                    }


                }





            })


        }



        binding.newSupportButton.setOnClickListener {


       // openBottomSheetDialog()
            Freshchat.showConversations(requireContext());



        }

        return binding.root
    }

    private fun setupFreshchat() {
        Freshchat.setImageLoader(CustomImageLoader(requireContext()))

        val config = FreshchatConfig(requireActivity().resources.getString(R.string.freshchat_app_id), resources.getString(
            R.string.freshchat_app_key
        ))
        config.setDomain(requireActivity().resources.getString(R.string.freshchat_domain))
        Freshchat.getInstance(requireContext())
            .init(config)

        val freshchatUser = Freshchat.getInstance(requireContext()).user
        freshchatUser.firstName = "John"
        freshchatUser.lastName = "Doe"
        freshchatUser.email = "john.doe.1982@mail.com"
        freshchatUser.setPhone("+91", "9790987495")
        Freshchat.getInstance(requireContext()).user =
            freshchatUser


        //Set up external id
        Freshchat.getInstance(requireContext()).identifyUser("john1234", null);


        //Set up restore id and sace
        val restoreId =
            Freshchat.getInstance(requireContext())
                .user.restoreId
        Paper.init(requireContext())
        Paper.book().write(Constant.FRESHCHAT_RESTORE_ID,restoreId)





        //Register broadcast receiver for generating restore id
        val broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                val restoreId =
                    Freshchat.getInstance(requireContext())
                        .user.restoreId

                Paper.init(requireContext())
                Paper.book().write(Constant.FRESHCHAT_RESTORE_ID,restoreId)
            }
        }



        val intentFilter = IntentFilter(Freshchat.FRESHCHAT_USER_RESTORE_ID_GENERATED)
        LocalBroadcastManager.getInstance(requireContext())
            .registerReceiver(broadcastReceiver, intentFilter)







    }
    private fun openBottomSheetDialog() {

        val bottomBinding = SendMessageBottomDialogBinding.inflate(layoutInflater)
        val bottomSheetDialog = BottomSheetDialog(requireContext())
        bottomSheetDialog.setContentView(bottomBinding.root)
        bottomSheetDialog.show()

        bottomBinding.sendMessageButton.setOnClickListener {


            if (bottomBinding.messageEdit.text.isNotEmpty()){

                val FreshchatMessage =
                    FreshchatMessage().setTag("tag").setMessage(bottomBinding.messageEdit.text.toString())
                Freshchat.sendMessage(requireContext(), FreshchatMessage)



                supportViewModel.sendMessage(Constant.getUserID(it.context).userID.toString(),
                Constant.getUserID(requireContext()).userID.toString(),"0", bottomBinding.messageEdit.text.toString())


                bottomSheetDialog.dismiss()
                supportViewModel.getAllMessages(Constant.getUserID(requireContext()).userID.toString())


            }


        }






    }




}