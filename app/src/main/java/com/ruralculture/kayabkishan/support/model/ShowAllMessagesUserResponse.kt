package com.ruralculture.kayabkishan.support.model


import com.google.gson.annotations.SerializedName

data class ShowAllMessagesUserResponse(
    @SerializedName("data")
    val `data`: List<Data>,
    @SerializedName("result")
    val result: Boolean
)