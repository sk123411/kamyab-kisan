package com.ruralculture.kayabkishan.support.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.databinding.MessageItemBinding
import com.ruralculture.kayabkishan.databinding.SupportChatMessageItemBinding
import com.ruralculture.kayabkishan.model.InvestItem
import com.ruralculture.kayabkishan.support.model.Data

class ChatMessagesListAdapter constructor(val list: List<Data>) :RecyclerView.Adapter<ChatMessagesListAdapter.ChatMessagesListAdapterViewHolder> () {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatMessagesListAdapterViewHolder {

        val view = MessageItemBinding.inflate(LayoutInflater.from(parent.context))

        return ChatMessagesListAdapterViewHolder(view.root)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ChatMessagesListAdapterViewHolder, position: Int) {

            holder.setup(list.get(position))
    }



    class ChatMessagesListAdapterViewHolder(view:View) :RecyclerView.ViewHolder(view) {

        val binding = MessageItemBinding.bind(view)


        fun setup(data: Data){
            val params =
                (binding.messageRoot.layoutParams) as RelativeLayout.LayoutParams
            if (Constant.getUserID(binding.root.context).userID.toString().equals(data.senderId)){

                params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT)


                binding.messageRoot.layoutParams = params
                binding.messageRoot.backgroundTintList =
                    ContextCompat.getColorStateList(binding.root.context, R.color.white);
                binding.message.setTextColor(binding.root.resources.getColor(R.color.grey))
                binding.message.text = data.message


            }else {



                params.addRule(RelativeLayout.ALIGN_PARENT_START)


                binding.messageRoot.layoutParams = params
                binding.messageRoot.backgroundTintList =
                    ContextCompat.getColorStateList(binding.root.context, R.color.green);
                binding.message.setTextColor(binding.root.resources.getColor(R.color.white))

                binding.message.text = data.message

            }


        }


    }

}