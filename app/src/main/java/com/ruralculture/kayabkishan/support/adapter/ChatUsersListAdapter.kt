package com.ruralculture.kayabkishan.support.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.databinding.SupportChatItemBinding
import com.ruralculture.kayabkishan.model.InvestItem
import com.ruralculture.kayabkishan.support.model.DataX
import com.squareup.picasso.Picasso

class ChatUsersListAdapter constructor(val list: List<DataX>) :RecyclerView.Adapter<ChatUsersListAdapter.CharUsersListAdapterViewHolder> () {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharUsersListAdapterViewHolder {

        val view = SupportChatItemBinding.inflate(LayoutInflater.from(parent.context))

        return CharUsersListAdapterViewHolder(view.root)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: CharUsersListAdapterViewHolder, position: Int) {


        holder.setup(list.get(position))
    }



    class CharUsersListAdapterViewHolder(view:View) :RecyclerView.ViewHolder(view) {

        val binding = SupportChatItemBinding.bind(view)






        fun setup(dataX: DataX){

            if (!dataX.name.equals("")) {
                binding.userName.text = dataX.name
            }else {
                binding.userName.text= "User "+adapterPosition.toString()
            }
                binding.userMessage.text = dataX.message



            binding.root.setOnClickListener {

                val bundle = Bundle()
                bundle.putString("id", dataX.reciverId)

                Navigation.findNavController(it).navigate(R.id.sendMessageFragment,bundle)
            }

        }


    }

}