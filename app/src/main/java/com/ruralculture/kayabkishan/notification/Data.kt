package com.ruralculture.kayabkishan.notification


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("description")
    val description: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("image")
    val image: String,
    @SerializedName("path")
    val path: String,
    @SerializedName("title")
    val title: String
)