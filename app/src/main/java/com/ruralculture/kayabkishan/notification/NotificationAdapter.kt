package com.ruralculture.kayabkishan.notification

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.databinding.NotificationItemBinding
import com.squareup.picasso.Picasso

data class NotificationAdapter(val list: MutableList<Data>):RecyclerView.Adapter<NotificationAdapter.MyViewHolder>(){
    class MyViewHolder(view:View):RecyclerView.ViewHolder(view) {

        val binding = NotificationItemBinding.bind(view)


        fun bind(notification: Data){

            binding.notificationText.text = notification.title
            binding.notificationDescription.text = notification.description
            Picasso.get().load(Constant.BASE_IMAGE_PATH+notification.image)


        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): NotificationAdapter.MyViewHolder {

        val binding = NotificationItemBinding.inflate(LayoutInflater.from(parent.context))

        return MyViewHolder(binding.root)
    }

    override fun getItemCount(): Int {

        return list.size
    }

    override fun onBindViewHolder(holder: NotificationAdapter.MyViewHolder, position: Int) {

        holder.bind(list.get(position))
    }


}