package com.ruralculture.kayabkishan.dashboard.slider

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Slider(
    @SerializedName("id")
    val id: String,
    @SerializedName("image")
    val image: String)
