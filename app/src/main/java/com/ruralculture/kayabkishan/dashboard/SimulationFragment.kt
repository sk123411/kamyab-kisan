package com.ruralculture.kayabkishan.dashboard

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.adapter.ReturnAdapter
import com.ruralculture.kayabkishan.databinding.FragmentSimulationBinding
import com.ruralculture.kayabkishan.invest.InvestActivity
import com.ruralculture.kayabkishan.model.InvestItem
import com.ruralculture.kayabkishan.model.ProductDetailResponse
import io.paperdb.Paper
import java.lang.invoke.ConstantCallSite


class SimulationFragment : Fragment() {

    private lateinit var binding: FragmentSimulationBinding
    var count: Int? = 1
    var year: Int? = 1
    var yearAndReturnMap: HashMap<String, String>? = null

    var productDetailResponse: ProductDetailResponse? = null
    var mutableInvestList: MutableList<InvestItem>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentSimulationBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment


        Paper.init(requireContext())
        productDetailResponse = Paper.book().read(Constant.PRODUCT)

        binding.countText.setText(count.toString())
        binding.countTextYear.setText(year.toString())


        binding.totalCapitalText.setText("Capital " + productDetailResponse!!.productPrice)
        binding.productTitle.setText(productDetailResponse!!.productName)

        mutableInvestList = ArrayList()
        calucateReturnAndSetText(count, year)



        binding.plusButton.setOnClickListener {
            count = count?.inc()
            binding.countText.text = count.toString()
        }

        binding.minusButton.setOnClickListener {
            count = count?.dec()
            binding.countText.text = count.toString()


        }



        binding.plusButtonYear.setOnClickListener {
            year = year?.inc()
            binding.countTextYear.text = year.toString()
        }

        binding.minusButtonYear.setOnClickListener {
            year = year?.dec()
            binding.countTextYear.text = year.toString()

        }


        val entries = mutableListOf<BarEntry>()
        val price = productDetailResponse!!.productPrice.toInt() * count!!.toInt()
        entries.add(BarEntry(1f, price.toFloat()))
        val set = BarDataSet(entries, "Profit calculation")
        val data = BarData(set)
        data.barWidth = 0.9f // set custom bar width
        binding.chart.setData(data)
        binding.chart.setFitBars(true)


        // make the x-axis fit exactly all bars
        binding.chart.invalidate() // refresh
        binding.chart.setBorderColor(android.R.color.holo_orange_light)
        binding.chart.setBorderWidth(2f)
        binding.chart.setGridBackgroundColor(resources.getColor(R.color.green))





        binding.simulateNowButton.setOnClickListener {


            calucateReturnAndSetText(count, year)
            setBarData(yearAndReturnMap)

        }


        return binding.root
    }

    private fun setBarData(yearAndReturnMap: HashMap<String, String>?) {
        val entries = mutableListOf<BarEntry>()

        for (mapData in yearAndReturnMap!!.entries) {
            entries.add(BarEntry(mapData.key.toFloat(), mapData.value.toFloat()))


        }

        val set = BarDataSet(entries, "BarDataSet")


        val data = BarData(set)
        data.barWidth = 0.9f // set custom bar width

        binding.chart.setData(data)
        binding.chart.setFitBars(true)


        // make the x-axis fit exactly all bars
        binding.chart.invalidate() // refresh

        binding.chart.setBorderColor(android.R.color.holo_orange_light)
        binding.chart.setBorderWidth(2f)
        binding.chart.setGridBackgroundColor(resources.getColor(R.color.green))


    }

    private fun calucateReturnAndSetText(count: Int?, year: Int?) {

        var price = productDetailResponse!!.productPrice.toInt() * count!!
        var returnPrice = price * productDetailResponse!!.roi.toInt() / 100
        val totalPrice = price + returnPrice

        binding.totalReturnText.setText("Return PKR " + totalPrice)
        binding.totalCapitalText.setText("Capital PKR " + price)

        mutableInvestList!!.add(InvestItem(year, productDetailResponse!!.roi, totalPrice))

        setRecyclerView(mutableInvestList!!)



        yearAndReturnMap = HashMap()

        if (year!! > 1) {
            for (y in 1..year!!) {

                val returnPrice = price * productDetailResponse!!.roi.toInt() / 100
                val totalPrice = price + returnPrice
                yearAndReturnMap!!.put(y.toString(), totalPrice.toString())
                price = totalPrice


            }

            Log.d("TESTTTTTTTTTt", "::" + yearAndReturnMap.toString())


            var total: Int? = 0
            mutableInvestList!!.clear()

            var countYear:Int? = 0
            for (v in yearAndReturnMap!!.entries) {
                total = total!!.plus(v.value.toInt())
                countYear = countYear!!.plus(1)
                mutableInvestList!!.add(
                    InvestItem(
                        countYear,
                        productDetailResponse!!.roi,
                        v.value.toInt()
                    )
                )


            }

            binding.totalReturnText.setText("Return PKR " + total)
            binding.totalCapitalText.setText("Capital PKR " + productDetailResponse!!.productPrice.toInt() *count * year)

            setRecyclerView(mutableInvestList!!)

        }


    }

    private fun setRecyclerView(mutableInvestList: MutableList<InvestItem>) {

        binding.returnInvestList.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = ReturnAdapter(mutableInvestList)

        }


    }

}