
package com.ruralculture.kayabkishan.dashboard

import android.animation.ObjectAnimator
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.adapter.DashboardAdapter
import com.ruralculture.kayabkishan.dashboard.repository.DashboardViewModel
import com.ruralculture.kayabkishan.dashboard.slider.Slider
import com.ruralculture.kayabkishan.dashboard.slider.SliderAdapter
import com.ruralculture.kayabkishan.databinding.FragmentDashboardDetailBinding
import com.ruralculture.kayabkishan.invest.InvestActivity
import com.ruralculture.kayabkishan.model.ProductDetailResponse
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView
import com.squareup.picasso.Picasso
import io.paperdb.Paper


class DashboardDetail : Fragment() {

    lateinit var binding:FragmentDashboardDetailBinding
    private val dashboardViewModel: DashboardViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentDashboardDetailBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment
        Paper.init(requireContext())
        val id = arguments?.getString("id")

        if (!Constant.clicked.equals("")) {
            dashboardViewModel.getInvestProducts(Constant.clicked);
        }
//
//        val tempList = listOf<Slider>(Slider(
//            "1", "https://res.cloudinary.com/dlmt4hsgw/image/upload/v1622833714/w2o1ib4ni9q3zu66icje.jpg"),
//            Slider("2","https://res.cloudinary.com/dlmt4hsgw/image/upload/v1616944984/jkct7oruq8yrgbbgabik.png"),
//            Slider("3","https://res.cloudinary.com/dlmt4hsgw/image/upload/v1616427585/shxpifkrcgerzuhxokz5.png"),
//            Slider("3","https://res.cloudinary.com/dlmt4hsgw/image/upload/v1616427585/shxpifkrcgerzuhxokz5.png")
//
//        )
//        setupSliderImages(tempList)




        if (id!=null){

            dashboardViewModel.getProductDetail(id,Constant.getUserID(requireContext()).userID.toString())
        }


        binding.backButton.setOnClickListener {

            Navigation.findNavController(it).navigateUp()
        }



        binding.investNowButton.setOnClickListener {




            startActivity(Intent(context,InvestActivity::class.java))

        }


        lifecycleScope.launchWhenStarted {


            dashboardViewModel.dashboardEvent.observe(viewLifecycleOwner, Observer {

                when(it){

                    is DashboardViewModel.DashboardEvent.SuccessProductDetail -> {

                        binding.shareProductButton.setOnClickListener { v->

                            val intent2 = Intent(Intent.ACTION_SEND)
                            intent2.type = "text/plain"
                            intent2.putExtra(Intent.EXTRA_TEXT, it.productDetailResponse.productName
                                    + "\n"+"Rate: "+ it.productDetailResponse.firstReturn +"%"
                                    + "\n"+"Price: PKR"+ it.productDetailResponse.productPrice
                                    + "\n"+"Download app from playstore")

                            startActivity(Intent.createChooser(intent2, "Share Product"))

                        }





                        if (it.productDetailResponse.like_state==1){

                            binding.favrouteButton.setImageResource(R.drawable.heart_full)

                            binding.favrouteButton.setOnClickListener {v->

                                binding.favrouteButton.setImageResource(R.drawable.heart)

                                dashboardViewModel.addLikeOnProduct(it.productDetailResponse.productId,Constant.getUserID(requireContext()).userID.toString())


                            }
                        }else {

                            binding.favrouteButton.setImageResource(R.drawable.heart)

                            binding.favrouteButton.setOnClickListener {v->

                                binding.favrouteButton.setImageResource(R.drawable.heart_full)

                                dashboardViewModel.addLikeOnProduct(it.productDetailResponse.productId,Constant.getUserID(requireContext()).userID.toString())


                            }


                        }



                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false


                        binding.categoryOne.text = it.productDetailResponse.category_name
                        binding.productName.text = it.productDetailResponse.productName
                        binding.description.text = it.productDetailResponse.productDetail
                        binding.unitSold.text = it.productDetailResponse.startingInventory + " Units Sold"
                        binding.unitLeft.text = it.productDetailResponse.availableInventory + " Units Left"


                        if (it.productDetailResponse.roi.equals("")) {
                            binding.roiText.text = "2%/year"
                        }else {
                            binding.roiText.text = it.productDetailResponse.roi+"%/year"
                        }


                        if (it.productDetailResponse.unit.equals("")) {
                            binding.harvestText.text = "3 units"
                        }else {
                            binding.harvestText.text = it.productDetailResponse.unit.toString()+" units"
                        }

                        if (it.productDetailResponse.firstReturn.equals("")) {
                            binding.firstReturnText.text = "18%/year"
                        }else {
                            binding.firstReturnText.text = it.productDetailResponse.firstReturn+"%/years"
                        }

                        if (it.productDetailResponse.contract.equals("")) {
                            binding.contractTextView.text = "3 years"
                        }else {
                            binding.contractTextView.text = it.productDetailResponse.contract+" years"
                        }


                        binding.productInventoryBar.max = it.productDetailResponse.availableInventory.toInt()+it.productDetailResponse.startingInventory.toInt()
                        binding.productInventoryBar.progress = it.productDetailResponse.startingInventory.toInt()


                        Picasso.get().load(Constant.BASE_IMAGE_PATH+it.productDetailResponse.productImage).into(binding.productImage)
                        binding.unitsLeftText.text = it.productDetailResponse.availableInventory +" units left"



                        if (it.productDetailResponse.multi_image.size>=1){
                            binding.productImage.visibility = View.GONE
                            setupSliderImages(it.productDetailResponse.multi_image)


                        }else {
                            binding.slider.visibility= View.GONE
                            binding.productImage.visibility = View.VISIBLE

                        }

                        if (it.productDetailResponse.farmer!=null){

                            binding.farmerName.setText(it.productDetailResponse.farmer.name)
                            binding.farmerdescription.setText(it.productDetailResponse.farmer.description)
                            Picasso.get().load(Constant.BASE_IMAGE_PATH+it.productDetailResponse.farmer.image).placeholder(
                                R.drawable.orange_grass
                            ).into(binding.farmerImage)
                        }





                        Paper.book().write(Constant.PRODUCT,it.productDetailResponse)
                        binding.totalPrice.text = "PKR "+it.productDetailResponse.productPrice









                        binding.readMoreButton.setOnClickListener {
                            cycleTextViewExpansion(binding.farmerdescription)
                        }
                    }


                    is DashboardViewModel.DashboardEvent.SuccessProducts -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false


//                        binding.shimmerLayoutProducts.stopShimmerAnimation()
//                        binding.shimmerLayoutProducts.visibility = View.GONE



                        binding.otherCompaignsList.apply {

                            layoutManager = LinearLayoutManager(context)
                            adapter = DashboardAdapter(it.producs,  it.producs.size)
                        }


                    }



                    is DashboardViewModel.DashboardEvent.Empty -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false


                    }

                    is DashboardViewModel.DashboardEvent.SuccessBlogResponse -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false
                        if (it.articleDetails.blog==null){


                            Toast.makeText(context,"Product disliked",Toast.LENGTH_SHORT).show()

                        }else {

                            Toast.makeText(context,"Product liked",Toast.LENGTH_SHORT).show()

                        }

                    }

                    is DashboardViewModel.DashboardEvent.Loading -> {


                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true
                    }


                }


            })


        }














        binding.profileRoot.setOnClickListener {

            Navigation.findNavController(it).navigate(R.id.simulationFragment)

        }




        return binding.root
    }

    fun setupSliderImages(images:List<Slider>){

        val sliderAdapter =
            SliderAdapter(
                requireContext()
            )
        for (data in images) {
            sliderAdapter.addItem(
                Slider(
                    data.id,
                    "${Constant.BASE_IMAGE_PATH}${data.image}"

                )
            )

        }
        binding.slider.scrollTimeInMillis = 1000
        binding.slider.setSliderAdapter(sliderAdapter)
        binding.slider.setIndicatorAnimation(IndicatorAnimationType.WORM);
        binding.slider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        binding.slider.startAutoCycle();







        if (images.size>=1) {
            binding.slider.setCurrentPageListener(object : SliderView.OnSliderPageListener {
                override fun onSliderPageChanged(position: Int) {


                    if (position==0){
                        binding.viewOne.setBackgroundColor(resources.getColor(R.color.lema_green))
                        binding.viewTwo.setBackgroundColor(resources.getColor(R.color.white))
                        binding.viewThree.setBackgroundColor(resources.getColor(R.color.white))
                        binding.viewFour.setBackgroundColor(resources.getColor(R.color.white))


                    }else if (position==1){
                        binding.viewTwo.setBackgroundColor(resources.getColor(R.color.lema_green))
                        binding.viewFour.setBackgroundColor(resources.getColor(R.color.white))
                        binding.viewThree.setBackgroundColor(resources.getColor(R.color.white))
                        binding.viewOne.setBackgroundColor(resources.getColor(R.color.white))

                    } else if (position==2){
                        binding.viewThree.setBackgroundColor(resources.getColor(R.color.lema_green))
                        binding.viewFour.setBackgroundColor(resources.getColor(R.color.white))
                        binding.viewTwo.setBackgroundColor(resources.getColor(R.color.white))
                        binding.viewOne.setBackgroundColor(resources.getColor(R.color.white))

                    }else {
                        binding.viewFour.setBackgroundColor(resources.getColor(R.color.lema_green))
                        binding.viewThree.setBackgroundColor(resources.getColor(R.color.white))
                        binding.viewTwo.setBackgroundColor(resources.getColor(R.color.white))
                        binding.viewOne.setBackgroundColor(resources.getColor(R.color.white))


                    }

                }

            })
        }


    }


    private fun cycleTextViewExpansion(tv: TextView) {
        val collapsedMaxLines = 4
        val animation = ObjectAnimator.ofInt(
            tv, "maxLines",
            if (tv.maxLines == collapsedMaxLines) tv.lineCount else collapsedMaxLines
        )


        val duration: Int = (tv.getLineCount() - collapsedMaxLines) * 10
        animation.setDuration(duration.toLong()).start()
    }

}