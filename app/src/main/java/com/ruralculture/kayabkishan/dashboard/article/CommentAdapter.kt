package com.ruralculture.kayabkishan.dashboard.article

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.dashboard.repository.DashboardViewModel
import com.ruralculture.kayabkishan.databinding.CommentItemBinding
import com.ruralculture.kayabkishan.model.Comment

class CommentAdapter constructor(val list: List<Comment>,val dashboardViewModel: DashboardViewModel) :RecyclerView.Adapter<CommentViewHolder> (){




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {

        val view = CommentItemBinding.inflate(LayoutInflater.from(parent.context))

        return CommentViewHolder(
            view.root
        )

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {



        holder.setCommentData(list.get(position),dashboardViewModel)

    }


}

class CommentViewHolder(view:View) :RecyclerView.ViewHolder(view) {

    val binding = CommentItemBinding.bind(view)



    fun setCommentData(comment: Comment,dashboardViewModel: DashboardViewModel){

        binding.userName.text = comment.userId
        binding.userComment.text = comment.comment
        binding.userPostTime.text = comment.time

        if (comment.userName!=null) {
            binding.userImageText.text = comment.userName.substring(0)
        }


        binding.editImageButton.setOnClickListener {



            if (binding.commentRoot.isVisible){

                if (binding.commentText.text!!.isNotEmpty()){


                    dashboardViewModel.updateCommentOnArticle(comment = binding.commentText.text.toString(),
                    id = comment.id)


                }


            }else {
                binding.commentRoot.visibility = View.VISIBLE



            }


        }


        binding.deleteImageButton.setOnClickListener {

            dashboardViewModel.deleteCommentOnArticle(id = comment.id)
        }


        Log.d("DDDDDDDDDDD", "Comment id " + comment.userId +":::"+"user "+Constant.getUserID(binding.root.context).toString())

        if (comment.userId.equals(Constant.getUserID(binding.root.context).userID.toString())){

            binding.editCommentLayout.visibility = View.VISIBLE

        }else {

            binding.editCommentLayout.visibility = View.GONE

        }
    }

    }







