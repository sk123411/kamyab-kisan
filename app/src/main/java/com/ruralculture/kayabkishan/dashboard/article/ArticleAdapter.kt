package com.ruralculture.kayabkishan.dashboard.article

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.databinding.ArticlesItemBinding
import com.ruralculture.kayabkishan.model.ArticleData
import com.squareup.picasso.Picasso

class ArticleAdapter constructor(val list: List<ArticleData>,val size:Int?) :RecyclerView.Adapter<ArticleViewholder> (){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewholder {

        val view = ArticlesItemBinding.inflate(LayoutInflater.from(parent.context))

        return ArticleViewholder(
            view.root
        )

    }

    override fun getItemCount(): Int {
        return size!!
    }

    override fun onBindViewHolder(holder: ArticleViewholder, position: Int) {



        holder.setData(list.get(position))



    }


}

class ArticleViewholder(view:View) :RecyclerView.ViewHolder(view) {

    val binding = ArticlesItemBinding.bind(view)

    fun setData(articleData: ArticleData) {

        binding.articleTitle.text = articleData.title
        binding.articleTime.text = articleData.date

        Picasso.get().load(Constant.BASE_IMAGE_PATH + articleData.image).into(binding.articleImage)


        binding.root.setOnClickListener {

            val bundle = Bundle()
            bundle.putString("id", articleData.id)

            Navigation.findNavController(it).navigate(R.id.articleDetailFragment,bundle)


        }



        
    }

}
