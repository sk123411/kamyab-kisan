package com.ruralculture.kayabkishan.dashboard.model


import com.google.gson.annotations.SerializedName

data class Image(
    @SerializedName("id")
    val id: String,
    @SerializedName("image")
    val image: String,
    @SerializedName("path")
    val path: String,
    @SerializedName("product_id")
    val productId: String
)