package com.ruralculture.kayabkishan.dashboard.model


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("return_amount")
    val returnAmount: String,
    @SerializedName("total_investment")
    val totalInvestment: String,
    @SerializedName("total_roi")
    val totalRoi: String,
    @SerializedName("total_units")
    val totalUnits: String,
    @SerializedName("zzqq")
    val zzqq: String
)