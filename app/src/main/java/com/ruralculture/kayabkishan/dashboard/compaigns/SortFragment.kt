package com.ruralculture.kayabkishan.dashboard.compaigns

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.adapter.InvestAdapter
import com.ruralculture.kayabkishan.adapter.SortAdapter
import com.ruralculture.kayabkishan.dashboard.repository.DashboardViewModel
import com.ruralculture.kayabkishan.databinding.FragmentSortBinding
import com.ruralculture.kayabkishan.model.InvestItem
import com.ruralculture.kayabkishan.model.invest.InvestResponse
import io.paperdb.Paper


class SortFragment : Fragment() {

    lateinit var binding:FragmentSortBinding

    var initialList:List<FilterProducts>?=null
    private val dashboardViewModel: DashboardViewModel by viewModels()

    companion object {

        var selectedIdList:MutableList<String>? = null
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSortBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment
        Paper.init(requireContext())
        selectedIdList = ArrayList()
       val filterOptionList = mutableListOf<FilterProducts>()
        initList()


        val savedCategoryList = Paper.book().read<List<InvestResponse>>(Constant.SAVED_CATEGORY)
        
        val filterOptions:List<FilterProducts> = Paper.book().read(Constant.FILTER,initialList!!)

        dashboardViewModel.getInvestCategories()


        if (filterOptions.contains(FilterProducts.MOST_SOLD)){

            binding.ckUnitSoldRadio.isChecked = true


        }
        if (filterOptions.contains(FilterProducts.LOW_TO_HIGH)){
            binding.ckLowToHigh.isChecked = true

        }
        if (filterOptions.contains(FilterProducts.HIGH_TO_LOW)){
            binding.ckHighToLow.isChecked = true


        }
        if (filterOptions.contains(FilterProducts.LONG_TERM)){
            binding.ckLongRadio.isChecked = true

        }


        if (savedCategoryList!=null){
            if (savedCategoryList.size> 0){


                binding.categoriesList.apply {
                    layoutManager = LinearLayoutManager(context)
                    adapter = SortAdapter(savedCategoryList)
                }


            }

        }




        binding.applyButton.setOnClickListener {

            Paper.init(requireContext())

            if (binding.ckLowToHigh.isChecked){

                filterOptionList.add(FilterProducts.LOW_TO_HIGH)
            }

            if (binding.ckHighToLow.isChecked){

                filterOptionList.add(FilterProducts.HIGH_TO_LOW)
            }


            if (binding.ckLongRadio.isChecked){

                filterOptionList.add(FilterProducts.LONG_TERM)
            }



            if (binding.ckUnitSoldRadio.isChecked){

                filterOptionList.add(FilterProducts.MOST_SOLD)
            }



            Paper.book().write(Constant.FILTER, filterOptionList)
            Paper.book().write(Constant.FILTER_CATEGORY, selectedIdList)


            if (selectedIdList!!.size > 0) {

                Paper.book().write(Constant.FILTER_CATEGORY_SELECTED, true)
            }else {
                Paper.book().write(Constant.FILTER_CATEGORY_SELECTED, false)

            }


            Navigation.findNavController(it).navigateUp()


        }





        return binding.root

    }

    private fun initList() {
        initialList = listOf<FilterProducts>(
            FilterProducts.LOW_TO_HIGH
        )
        // Paper.book().write(Constant.FILTER,initialList)
    }


    enum class FilterProducts {
        LOW_TO_HIGH,
        MOST_SOLD,
        LONG_TERM,
        HIGH_TO_LOW


    }


}