package com.ruralculture.kayabkishan.dashboard.article

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.dashboard.repository.DashboardViewModel
import com.ruralculture.kayabkishan.databinding.FragmentArticleBinding


class ArticleFragment : Fragment() {

    lateinit var binding:FragmentArticleBinding
    private val dashboardViewModel: DashboardViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding  = FragmentArticleBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment
        dashboardViewModel.getAllArticles()
        lifecycleScope.launchWhenStarted {



            dashboardViewModel.dashboardEvent.observe(viewLifecycleOwner, Observer {


                when(it){


                    is DashboardViewModel.DashboardEvent.Loading -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true
                    }

                    is DashboardViewModel.DashboardEvent.Empty -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                        Toast.makeText(context,"No result found", Toast.LENGTH_SHORT).show()
                    }

                    is DashboardViewModel.DashboardEvent.SuccessArticles -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false


                        binding.allArticlesList.apply {

                            layoutManager = LinearLayoutManager(context)
                            adapter =
                                ArticleAdapter(
                                    it.articles,
                                    4
                                )


                        }


                    }

                }



            })











        }



        return binding.root
    }

}