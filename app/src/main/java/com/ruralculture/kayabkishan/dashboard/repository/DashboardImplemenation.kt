package com.ruralculture.kayabkishan.dashboard.repository

import android.util.Log
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.dashboard.model.UserStatsResponse
import com.ruralculture.kayabkishan.helper.Resource
import com.ruralculture.kayabkishan.model.Article
import com.ruralculture.kayabkishan.model.ArticleDetailResponse
import com.ruralculture.kayabkishan.model.ProductDetailResponse
import com.ruralculture.kayabkishan.model.blog.BlogLikeResponse
import com.ruralculture.kayabkishan.model.invest.BaseResponse
import com.ruralculture.kayabkishan.model.invest.InvestProduct
import com.ruralculture.kayabkishan.model.invest.InvestResponse
import com.ruralculture.kayabkishan.notification.NotificationResponse
import com.ruralculture.kayabkishan.wishlist.model.WishlistResponse

class DashboardImplemenation  constructor(var dashboardAPI: DashboardAPI) :
    Dashboard {



    override suspend fun getInvestCategories(): Resource<BaseResponse<InvestResponse>> {

        return try {

            val response = dashboardAPI.getInvestCategories(Constant.API_CATEGORY)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result!!)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }

    }

    override suspend fun getInvestProducts(data:HashMap<String,String>): Resource<BaseResponse<InvestProduct>> {
        return try {

            val response = dashboardAPI.getInvestProducts(Constant.API_PRODUCTS,data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result!!)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }    }

    override suspend fun getProductDetailById(data: HashMap<String, String>): Resource<ProductDetailResponse> {
        return try {

            val response = dashboardAPI.getProductDetailById(Constant.API_PRODUCT_DETAIL,data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result!!)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun getAllArticles(): Resource<Article> {
        return try {

            val response = dashboardAPI.getAllArticles(Constant.API_ALL_ARTICLES)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result!!)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun getArticleDetailsById(data: HashMap<String, String>): Resource<ArticleDetailResponse> {
        return try {

            val response = dashboardAPI.getArticleDetailsById(Constant.API_ARTICLES_DETAILS,data)

            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result!!)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun addLikeOnArticle(data: HashMap<String, String>): Resource<BlogLikeResponse> {
        return try {

            val response = dashboardAPI.addLikeOnArticle(Constant.API_ADD_LIKE,data)

            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result!!)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun addCommentOnArticle(data: HashMap<String, String>): Resource<BlogLikeResponse> {

        return try {

            val response = dashboardAPI.addCommentOnArticle(Constant.API_ADD_COMMENT,data)

            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result!!)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }

    }

    override suspend fun getAllProducts(data: HashMap<String, String>): Resource<BaseResponse<InvestProduct>> {
        return try {

            val response = dashboardAPI.getAllProducts(Constant.API_SHOW_ALL_PRODUCTS,data)

            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result!!)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }

    }

    override suspend fun addLikeOnProduct(data: HashMap<String, String>): Resource<BlogLikeResponse> {
        return try {

            val response = dashboardAPI.addLikeOnProduct(Constant.API_ADD_LIKE_PRODUCT,data)

            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result!!)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun updateCommentOnArticle(data: HashMap<String, String>): Resource<BlogLikeResponse> {
        return try {

            val response = dashboardAPI.updateCommentOnArticle(Constant.API_UPDATE_COMMENT_ARTICLE,data)

            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result!!)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun deleteCommentOnArticle(data: HashMap<String, String>): Resource<BlogLikeResponse> {
        return try {

            val response = dashboardAPI.deleteCommentOnArticle(Constant.API_DELETE_COMMENT_ARTICLE,data)

            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result!!)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun getFavrouteProducts(data: HashMap<String, String>): Resource<WishlistResponse> {
        return try {

            val response = dashboardAPI.getFavrouteProducts(Constant.API_WISHLIST_PRODUCTS,data)

            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result!!)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }

    }

    override suspend fun getUserStats(data: HashMap<String, String>): Resource<UserStatsResponse> {
        return try {

            val response = dashboardAPI.getUserStats(Constant.API_USER_STATS,data)

            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result!!)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }


    }

    override suspend fun getNotifications(): Resource<NotificationResponse> {
        return try {

            val response = dashboardAPI.getNotifications(Constant.API_GET_NOTIFICATION)

            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result!!)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }


}