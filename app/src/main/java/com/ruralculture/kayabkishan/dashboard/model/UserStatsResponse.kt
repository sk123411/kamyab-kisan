package com.ruralculture.kayabkishan.dashboard.model


import com.google.gson.annotations.SerializedName

data class UserStatsResponse(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("result")
    val result: String
)