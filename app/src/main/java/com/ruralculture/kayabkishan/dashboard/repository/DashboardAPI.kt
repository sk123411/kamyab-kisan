package com.ruralculture.kayabkishan.dashboard.repository

import com.ruralculture.kayabkishan.dashboard.model.UserStatsResponse
import com.ruralculture.kayabkishan.helper.Resource
import com.ruralculture.kayabkishan.model.Article
import com.ruralculture.kayabkishan.model.ArticleDetailResponse
import com.ruralculture.kayabkishan.model.ProductDetailResponse
import com.ruralculture.kayabkishan.model.blog.BlogLikeResponse
import com.ruralculture.kayabkishan.model.invest.BaseResponse
import com.ruralculture.kayabkishan.model.invest.InvestProduct
import com.ruralculture.kayabkishan.model.invest.InvestResponse
import com.ruralculture.kayabkishan.notification.NotificationResponse
import com.ruralculture.kayabkishan.wishlist.model.WishlistResponse
import retrofit2.Response
import retrofit2.http.*

interface DashboardAPI {


    @GET("api/process.php")
    suspend fun getInvestCategories(@Query("action") action:String?): Response<BaseResponse<InvestResponse>>


    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun getInvestProducts(@Query("action") action:String?, @FieldMap data:HashMap<String,String>): Response<BaseResponse<InvestProduct>>


    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun getProductDetailById(@Query("action") action:String?, @FieldMap data:HashMap<String,String>): Response<ProductDetailResponse>



    @GET("api/process.php")
    suspend fun getAllArticles(@Query("action") action:String?): Response<Article>



    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun getAllProducts(@Query("action") action:String?,@FieldMap data:HashMap<String,String>): Response<BaseResponse<InvestProduct>>



    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun getArticleDetailsById(@Query("action") action:String?, @FieldMap data:HashMap<String,String>): Response<ArticleDetailResponse>

    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun addLikeOnArticle(@Query("action") action:String?, @FieldMap data:HashMap<String,String>): Response<BlogLikeResponse>


    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun addCommentOnArticle(@Query("action") action:String?, @FieldMap data:HashMap<String,String>): Response<BlogLikeResponse>


    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun updateCommentOnArticle(@Query("action") action:String?, @FieldMap data:HashMap<String,String>): Response<BlogLikeResponse>



    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun deleteCommentOnArticle(@Query("action") action:String?, @FieldMap data:HashMap<String,String>): Response<BlogLikeResponse>



    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun addLikeOnProduct(@Query("action") action:String?,
                                 @FieldMap data:HashMap<String,String>): Response<BlogLikeResponse>

    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun getFavrouteProducts(@Query("action") action:String?,
                                 @FieldMap data:HashMap<String,String>): Response<WishlistResponse>



    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun getUserStats(@Query("action") action:String?,
                                    @FieldMap data:HashMap<String,String>): Response<UserStatsResponse>


    @GET("api/process.php")
    suspend fun getNotifications(@Query("action") action:String?): Response<NotificationResponse>

}