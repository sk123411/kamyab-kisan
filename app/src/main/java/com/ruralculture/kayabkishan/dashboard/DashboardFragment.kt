package com.ruralculture.kayabkishan.dashboard

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.MainActivity
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.WebviewActivity
import com.ruralculture.kayabkishan.adapter.DashboardAdapter
import com.ruralculture.kayabkishan.adapter.InvestAdapter
import com.ruralculture.kayabkishan.dashboard.article.ArticleAdapter
import com.ruralculture.kayabkishan.dashboard.repository.DashboardViewModel
import com.ruralculture.kayabkishan.databinding.FragmentDashboardBinding
import com.ruralculture.kayabkishan.profile.ABOUT_US_URL
import com.ruralculture.kayabkishan.profile.FAQ_URL
import io.paperdb.Paper


class DashboardFragment : Fragment() {


    lateinit var binding: FragmentDashboardBinding

    private val dashboardViewModel: DashboardViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentDashboardBinding.inflate(layoutInflater)

        MainActivity.bottomNavigation.isVisible = true



        dashboardViewModel.getInvestCategories()
        dashboardViewModel.getInvestProducts("1");
        dashboardViewModel.getAllArticles()
        dashboardViewModel.getUserStats(Constant.getUserID(requireContext()).userID.toString())

        Paper.init(requireContext())
        Paper.book().write(Constant.FILTER_CATEGORY_SELECTED, false)

        binding.knowMoreButton.setOnClickListener {
            startActivity(
                Intent(requireActivity(), WebviewActivity::class.java).putExtra("url",
                ABOUT_US_URL
                ))
        }




        binding.notiIconButton.setOnClickListener {

            Navigation.findNavController(it).navigate(R.id.notificationFragment)


        }




        lifecycleScope.launchWhenStarted {


            dashboardViewModel.dashboardEvent.observe(viewLifecycleOwner, Observer {


                when (it) {

                    is DashboardViewModel.DashboardEvent.Success -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

//                        binding.shimmerLayout.stopShimmerAnimation()
//                        binding.shimmerLayout.visibility = View.GONE

                        Paper.init(requireContext())
                        Paper.book().write(Constant.SAVED_CATEGORY,it.cateogories)

                        binding.investlist.apply {

                            layoutManager =
                                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                            adapter = InvestAdapter(it.cateogories, dashboardViewModel)
                        }


                    }

                    is DashboardViewModel.DashboardEvent.SuccessProducts -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false


//                        binding.shimmerLayoutProducts.stopShimmerAnimation()
//                        binding.shimmerLayoutProducts.visibility = View.GONE



                        binding.itemlist.apply {

                            layoutManager = LinearLayoutManager(context)

                            if (it.producs.size > 3) {
                                adapter =
                                    DashboardAdapter(list = it.producs, size = 3)
                            }else {
                                adapter =DashboardAdapter(list = it.producs, size = it.producs.size)


                            }
                        }


                    }

                    is DashboardViewModel.DashboardEvent.SuccessStats -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false


                        if (it.userStatsResponse.data!=null) {
                            binding.investmentText.text =
                                "PKR ${it.userStatsResponse.data.totalInvestment}"
                            binding.investmentUnits.text =
                                "${it.userStatsResponse.data.totalUnits} units"
                            binding.investmentReturnAmount.text =
                                "PKR ${it.userStatsResponse.data.returnAmount}"
                            binding.investmentReturnPercent.text =
                                "${it.userStatsResponse.data.totalRoi} %"
                        }


                        }

                    is DashboardViewModel.DashboardEvent.Loading -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true
                    }

                    is DashboardViewModel.DashboardEvent.Empty -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                        Toast.makeText(context, "No result found", Toast.LENGTH_SHORT).show()
                    }

                    is DashboardViewModel.DashboardEvent.SuccessArticles -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false


                        binding.articlelist.apply {

                            layoutManager = LinearLayoutManager(context)
                            adapter =
                                ArticleAdapter(
                                    it.articles,
                                    3
                                )


                        }


                    }

                }


            })


        }

        binding.viewAllBtn.setOnClickListener {

            Navigation.findNavController(it).navigate(R.id.articleFragment)

        }


        binding.seeAllItemsButton.setOnClickListener {

            Navigation.findNavController(it).navigate(R.id.allCompaignsFragment)

        }


        binding.seeAllArticlesButton.setOnClickListener {

            Navigation.findNavController(it).navigate(R.id.articleFragment)

        }

        return binding.root
    }





    override fun onResume() {
        super.onResume()



//        binding.shimmerLayout.visibility = View.VISIBLE
//        binding.shimmerLayout.startShimmerAnimation()
//
//        binding.shimmerLayoutProducts.visibility = View.VISIBLE
//        binding.shimmerLayoutProducts.startShimmerAnimation()

        val bottomSupport = MainActivity.bottomNavigation.menu.findItem(R.id.bottom_home)
        bottomSupport.isChecked = true
    }


}