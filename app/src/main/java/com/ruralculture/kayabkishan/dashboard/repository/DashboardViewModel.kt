package com.ruralculture.kayabkishan.dashboard.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ruralculture.kayabkishan.dashboard.model.UserStatsResponse
import com.ruralculture.kayabkishan.helper.Resource
import com.ruralculture.kayabkishan.model.ArticleData
import com.ruralculture.kayabkishan.model.ArticleDetailResponse
import com.ruralculture.kayabkishan.model.ProductDetailResponse
import com.ruralculture.kayabkishan.model.blog.BlogLikeResponse
import com.ruralculture.kayabkishan.model.invest.InvestProduct
import com.ruralculture.kayabkishan.model.invest.InvestResponse
import com.ruralculture.kayabkishan.notification.NotificationResponse
import com.ruralculture.kayabkishan.retofit.RetrofitInstance
import com.ruralculture.kayabkishan.wishlist.model.WishlistResponse
import kotlinx.coroutines.launch

class DashboardViewModel : ViewModel() {


    private val dashboardEvent_ = MutableLiveData<DashboardEvent>()
    lateinit var dashboardAPI: DashboardAPI
    lateinit var dashboard: Dashboard


    val dashboardEvent: LiveData<DashboardEvent>
        get() = dashboardEvent_


    init {
        dashboardAPI = RetrofitInstance.getRetrofitInstance().create(DashboardAPI::class.java)
        dashboard =
            DashboardImplemenation(
                dashboardAPI
            )


    }


    sealed class DashboardEvent {

        class Success(val cateogories: List<InvestResponse>) : DashboardEvent()
        class SuccessProducts(val producs: List<InvestProduct>) : DashboardEvent()
        class SuccessNotification(val notification: NotificationResponse) : DashboardEvent()

        class SuccessArticles(val articles: List<ArticleData>) : DashboardEvent()
        class SuccessArticleDetails(val articleDetails: ArticleDetailResponse) : DashboardEvent()
        class SuccessBlogResponse(val articleDetails: BlogLikeResponse) : DashboardEvent()
        class SuccessCommentEditDeleteResponse(val articleDetails: BlogLikeResponse) :
            DashboardEvent()

        class SuccessFavrouteProducts(val wishlistResponse: WishlistResponse) :
            DashboardEvent()

        class SuccessProductDetail(val productDetailResponse: ProductDetailResponse) :
            DashboardEvent()

        class SuccessStats(val userStatsResponse: UserStatsResponse) : DashboardEvent()


        class Failure(val message: String?) : DashboardEvent()
        object Loading : DashboardEvent()
        object Empty : DashboardEvent()
    }


    fun getInvestCategories() {
        dashboardEvent_.value =
            DashboardEvent.Loading



        viewModelScope.launch {

            val response = dashboard.getInvestCategories()

            when (response) {

                is Resource.Success -> {


                    dashboardEvent_.value =
                        DashboardEvent.Success(
                            response.data!!.data
                        )


                }

                is Resource.Error -> {
                    dashboardEvent_.value =
                        DashboardEvent.Failure(
                            "Sign in success"
                        )


                }


            }


        }


    }

    fun getNotification() {
        dashboardEvent_.value =
            DashboardEvent.Loading



        viewModelScope.launch {

            val response = dashboard.getNotifications()

            when (response) {

                is Resource.Success -> {


                    dashboardEvent_.value =
                        DashboardEvent.SuccessNotification(
                            response.data!!)


                }

                is Resource.Error -> {
                    dashboardEvent_.value =
                        DashboardEvent.Failure(
                            "Sign in success"
                        )


                }


            }


        }


    }

    fun getInvestProducts(id: String?) {
        dashboardEvent_.value =
            DashboardEvent.Loading

        val map = HashMap<String, String>()
        map.put("category_id", id!!)


        viewModelScope.launch {

            val response = dashboard.getInvestProducts(map)

            when (response) {

                is Resource.Success -> {

                    if (response.data!!.status) {
                        dashboardEvent_.value =
                            DashboardEvent.SuccessProducts(
                                response.data!!.data
                            )
                    } else {


                        dashboardEvent_.value =
                            DashboardEvent.Empty

                    }


                }

                is Resource.Error -> {
                    dashboardEvent_.value =
                        DashboardEvent.Failure(
                            "Sign in success"
                        )


                }


            }


        }


    }


    fun getUserStats(id: String?) {
        dashboardEvent_.value =
            DashboardEvent.Loading

        val map = HashMap<String, String>()
        map.put("user_id", id!!)


        viewModelScope.launch {

            val response = dashboard.getUserStats(map)

            when (response) {

                is Resource.Success -> {

                    dashboardEvent_.value =
                        DashboardEvent.SuccessStats(
                            response.data!!
                        )
                    
                }

                is Resource.Error -> {
                    dashboardEvent_.value =
                        DashboardEvent.Failure(
                            "Sign in success"
                        )


                }


            }


        }


    }


    fun getProductDetail(id: String?, userID: String?) {
        dashboardEvent_.value =
            DashboardEvent.Loading

        val map = HashMap<String, String>()
        map.put("product_id", id!!)
        map.put("user_id", userID!!)



        viewModelScope.launch {

            val response = dashboard.getProductDetailById(map)

            when (response) {

                is Resource.Success -> {

                    if (response.data!!.result) {
                        dashboardEvent_.value =
                            DashboardEvent.SuccessProductDetail(
                                response.data
                            )
                    } else {


                        dashboardEvent_.value =
                            DashboardEvent.Empty

                    }


                }

                is Resource.Error -> {
                    dashboardEvent_.value =
                        DashboardEvent.Failure(
                            "Sign in success"
                        )


                }


            }


        }


    }


    fun getAllArticles() {
        dashboardEvent_.value =
            DashboardEvent.Loading



        viewModelScope.launch {

            val response = dashboard.getAllArticles()

            when (response) {

                is Resource.Success -> {

                    if (response.data!!.status) {
                        dashboardEvent_.value =
                            DashboardEvent.SuccessArticles(
                                response.data.data
                            )
                    } else {


                        dashboardEvent_.value =
                            DashboardEvent.Empty

                    }


                }

                is Resource.Error -> {
                    dashboardEvent_.value =
                        DashboardEvent.Failure(
                            "Sign in success"
                        )


                }


            }


        }


    }


    fun getArticleDetailsById(id: String?, userID: String?) {
        dashboardEvent_.value =
            DashboardEvent.Loading

        val map = HashMap<String, String>()
        map.put("blog_id", id!!)
        map.put("user_id", userID!!)




        viewModelScope.launch {

            val response = dashboard.getArticleDetailsById(map)

            when (response) {

                is Resource.Success -> {

                    dashboardEvent_.value =
                        DashboardEvent.SuccessArticleDetails(
                            response.data!!
                        )


                }

                is Resource.Error -> {
                    dashboardEvent_.value =
                        DashboardEvent.Failure(
                            "Sign in success"
                        )


                }


            }


        }


    }


    fun addLikeOnArticle(id: String?, userID: String?) {
        dashboardEvent_.value =
            DashboardEvent.Loading

        val map = HashMap<String, String>()
        map.put("blog_id", id!!)
        map.put("user_id", userID!!)


        viewModelScope.launch {

            val response = dashboard.addLikeOnArticle(map)

            when (response) {

                is Resource.Success -> {

                    dashboardEvent_.value =
                        DashboardEvent.SuccessBlogResponse(
                            response.data!!
                        )


                }

                is Resource.Error -> {
                    dashboardEvent_.value =
                        DashboardEvent.Failure(
                            "Sign in success"
                        )


                }


            }


        }


    }


    fun addCommentOnArticle(id: String?, userID: String?, comment: String) {
        dashboardEvent_.value =
            DashboardEvent.Loading

        val map = HashMap<String, String>()
        map.put("blog_id", id!!)
        map.put("user_id", userID!!)
        map.put("comment", comment)




        viewModelScope.launch {

            val response = dashboard.addCommentOnArticle(map)

            when (response) {

                is Resource.Success -> {

                    dashboardEvent_.value =
                        DashboardEvent.SuccessBlogResponse(
                            response.data!!
                        )


                }

                is Resource.Error -> {
                    dashboardEvent_.value =
                        DashboardEvent.Failure(
                            "Sign in success"
                        )


                }


            }


        }


    }


    fun addLikeOnProduct(id: String?, userID: String?) {
        dashboardEvent_.value =
            DashboardEvent.Loading

        val map = HashMap<String, String>()
        map.put("product_id", id!!)
        map.put("user_id", userID!!)


        viewModelScope.launch {

            val response = dashboard.addLikeOnProduct(map)

            when (response) {

                is Resource.Success -> {

                    dashboardEvent_.value =
                        DashboardEvent.SuccessBlogResponse(
                            response.data!!
                        )


                }

                is Resource.Error -> {
                    dashboardEvent_.value =
                        DashboardEvent.Failure(
                            "Sign in success"
                        )


                }


            }


        }


    }


    fun getFavrouteProducts(userID: String?) {
        dashboardEvent_.value =
            DashboardEvent.Loading

        val map = HashMap<String, String>()
        map.put("user_id", userID!!)



        viewModelScope.launch {

            val response = dashboard.getFavrouteProducts(map)

            when (response) {

                is Resource.Success -> {

                    if (response.data!!.status) {
                        dashboardEvent_.value =
                            DashboardEvent.SuccessFavrouteProducts(
                                response.data
                            )
                    } else {


                        dashboardEvent_.value =
                            DashboardEvent.Empty

                    }


                }

                is Resource.Error -> {
                    dashboardEvent_.value =
                        DashboardEvent.Failure(
                            "Sign in success"
                        )


                }


            }


        }


    }


    fun updateCommentOnArticle(id: String?, comment: String?) {
        dashboardEvent_.value =
            DashboardEvent.Loading

        val map = HashMap<String, String>()
        map.put("comment_id", id!!)
        map.put("comment", comment!!)


        viewModelScope.launch {

            val response = dashboard.updateCommentOnArticle(map)

            when (response) {

                is Resource.Success -> {

                    dashboardEvent_.value =
                        DashboardEvent.SuccessCommentEditDeleteResponse(
                            response.data!!
                        )


                }

                is Resource.Error -> {
                    dashboardEvent_.value =
                        DashboardEvent.Failure(
                            "Sign in success"
                        )


                }


            }


        }


    }


    fun deleteCommentOnArticle(id: String?) {
        dashboardEvent_.value =
            DashboardEvent.Loading

        val map = HashMap<String, String>()
        map.put("comment_id", id!!)


        viewModelScope.launch {

            val response = dashboard.deleteCommentOnArticle(map)

            when (response) {

                is Resource.Success -> {

                    dashboardEvent_.value =
                        DashboardEvent.SuccessCommentEditDeleteResponse(
                            response.data!!
                        )


                }

                is Resource.Error -> {
                    dashboardEvent_.value =
                        DashboardEvent.Failure(
                            "Sign in success"
                        )


                }


            }


        }


    }


    fun getAllProducts(userID: Int?) {
        dashboardEvent_.value =
            DashboardEvent.Loading
        val map = HashMap<String, String>()
        map.put("user_id", userID!!.toString())

        viewModelScope.launch {

            val response = dashboard.getAllProducts(map)

            when (response) {

                is Resource.Success ->

                    if (response.data!!.status) {
                        dashboardEvent_.value =
                            DashboardEvent.SuccessProducts(
                                response.data!!.data
                            )
                    } else {


                        dashboardEvent_.value =
                            DashboardEvent.Empty

                    }

                is Resource.Error -> {
                    dashboardEvent_.value =
                        DashboardEvent.Failure(
                            "Sign in success"
                        )


                }

            }


        }


    }


}

