package com.ruralculture.kayabkishan.dashboard.compaigns

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.MainActivity
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.adapter.CompaignsAdapter
import com.ruralculture.kayabkishan.dashboard.repository.DashboardViewModel
import com.ruralculture.kayabkishan.databinding.FragmentAllCompaignsBinding
import com.ruralculture.kayabkishan.model.invest.InvestProduct
import io.paperdb.Paper
import java.util.*
import kotlin.collections.ArrayList

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [AllCompaignsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AllCompaignsFragment : Fragment() {

    lateinit var binding: FragmentAllCompaignsBinding
    private val dashboardViewModel: DashboardViewModel by viewModels()
    var initialList:List<SortFragment.FilterProducts>?=null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentAllCompaignsBinding.inflate(layoutInflater)

        initList()
        AllCompaignsFragment.selectedProductList = ArrayList()
        dashboardViewModel.getAllProducts(Constant.getUserID(requireContext()).userID)

        val filterOptions:List<SortFragment.FilterProducts> = Paper.book().read(Constant.FILTER, initialList!!)





        binding.searchEdit.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (s.toString().isNotEmpty() && s.toString().length > 1) {

                    val compaignsList = Paper.book().read<List<InvestProduct>>(Constant.COMPAIGNS)
                    val filterList = mutableListOf<InvestProduct>()


                    for (c in compaignsList) {

                        if (c.productName.toString().toLowerCase()
                                .contains(s.toString().toLowerCase())
                        ) {
                            filterList.add(c)

                        }


                    }




                    binding.compaignList.apply {
                        layoutManager = LinearLayoutManager(context)


                        adapter = CompaignsAdapter(filterList, dashboardViewModel)


                    }


                } else {

                    dashboardViewModel.getAllProducts(Constant.getUserID(requireContext()).userID)

                }


            }

        })






        binding.sortButton.setOnClickListener {

            Navigation.findNavController(it).navigate(R.id.sortFragment)


        }


        lifecycleScope.launchWhenStarted {


            dashboardViewModel.dashboardEvent.observe(viewLifecycleOwner, Observer {


                when (it) {

                    is DashboardViewModel.DashboardEvent.Loading -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true

                    }


                    is DashboardViewModel.DashboardEvent.SuccessProducts -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false


                        Paper.book().write(Constant.COMPAIGNS, it.producs)

                        val isCategorySelected = Paper.book().read<Boolean>(Constant.FILTER_CATEGORY_SELECTED,false)

                        if (isCategorySelected){


                            val categoryList = Paper.book().read<List<String>>(Constant.FILTER_CATEGORY)

                            val filterCategoryProductList = mutableListOf<InvestProduct>()

                            for (categoryId in categoryList){

                                for (p in it.producs){

                                    if (p.productCategory.equals(categoryId!!)){

                                        filterCategoryProductList.add(p)
                                    }

                                }


                            }

                            if (filterOptions.contains(SortFragment.FilterProducts.MOST_SOLD)){

                                Collections.sort(filterCategoryProductList,InvestProduct.ProductUnitSoldCompare())


                            }
                            if (filterOptions.contains(SortFragment.FilterProducts.LOW_TO_HIGH)){
                                Collections.sort(filterCategoryProductList,InvestProduct.ProductPriceCompare())


                            }
                            if (filterOptions.contains(SortFragment.FilterProducts.HIGH_TO_LOW)){
                                Collections.sort(filterCategoryProductList,InvestProduct.ProductPriceHighToLowCompare())


                            }
                            if (filterOptions.contains(SortFragment.FilterProducts.LONG_TERM)){
                                Collections.sort(filterCategoryProductList,InvestProduct.ProductLongTermCompare())

                            }





                            binding.compaignList.apply {
                                layoutManager = LinearLayoutManager(context)
                                adapter = CompaignsAdapter(filterCategoryProductList, dashboardViewModel)

                            }



                        }else {




                            if (filterOptions.contains(SortFragment.FilterProducts.MOST_SOLD)){

                                Collections.sort(it.producs,InvestProduct.ProductUnitSoldCompare())


                            }
                            if (filterOptions.contains(SortFragment.FilterProducts.LOW_TO_HIGH)){
                                Collections.sort(it.producs,InvestProduct.ProductPriceCompare())


                            }
                            if (filterOptions.contains(SortFragment.FilterProducts.HIGH_TO_LOW)){
                                Collections.sort(it.producs,InvestProduct.ProductPriceHighToLowCompare())


                            }
                            if (filterOptions.contains(SortFragment.FilterProducts.LONG_TERM)){
                                Collections.sort(it.producs,InvestProduct.ProductLongTermCompare())

                            }


                            binding.compaignList.apply {
                                layoutManager = LinearLayoutManager(context)
                                adapter = CompaignsAdapter(it.producs, dashboardViewModel)

                            }
                        }










                    }

                    is DashboardViewModel.DashboardEvent.SuccessBlogResponse -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                        dashboardViewModel.getAllProducts(Constant.getUserID(requireContext()).userID)


                        if (it.articleDetails.blog == null) {


                            Toast.makeText(context, "Product disliked", Toast.LENGTH_SHORT).show()

                        } else {

                            Toast.makeText(context, "Product liked", Toast.LENGTH_SHORT).show()

                        }


                    }

                }


            })
        }




        binding.compareButton.setOnClickListener {

            if (AllCompaignsFragment.selectedProductList!!.size==2){
                Paper.init(context)
                Paper.book().write(Constant.SELECTED_PRODUCTS,AllCompaignsFragment.selectedProductList)

                Navigation.findNavController(it).navigate(R.id.compareFragment)



            }else {

                Toast.makeText(requireContext(),"Please select atleast 2 products to compare",Toast.LENGTH_LONG).show()
            }


        }





        return binding.root


    }

    private fun initList() {
        initialList = listOf<SortFragment.FilterProducts>(
            SortFragment.FilterProducts.LOW_TO_HIGH
        )
//       Paper.book().write(Constant.FILTER,initialList)
    }


    override fun onResume() {
        super.onResume()

        val bottomCompaign = MainActivity.bottomNavigation.menu.findItem(R.id.bottom_compaigns)
        bottomCompaign.isChecked = true


    }

    companion object {

        var selectedProductList:MutableList<InvestProduct>?=null

    }


}