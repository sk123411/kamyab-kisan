package com.ruralculture.kayabkishan.dashboard.repository

import com.ruralculture.kayabkishan.dashboard.model.UserStatsResponse
import com.ruralculture.kayabkishan.helper.Resource
import com.ruralculture.kayabkishan.model.Article
import com.ruralculture.kayabkishan.model.ArticleDetailResponse
import com.ruralculture.kayabkishan.model.ProductDetailResponse
import com.ruralculture.kayabkishan.model.blog.BlogLikeResponse
import com.ruralculture.kayabkishan.model.invest.BaseResponse
import com.ruralculture.kayabkishan.model.invest.InvestProduct
import com.ruralculture.kayabkishan.model.invest.InvestResponse
import com.ruralculture.kayabkishan.notification.NotificationResponse
import com.ruralculture.kayabkishan.wishlist.model.WishlistResponse
import retrofit2.Response
import retrofit2.http.Query

interface Dashboard {

    suspend fun getInvestCategories(): Resource<BaseResponse<InvestResponse>>
    suspend fun getInvestProducts(data:HashMap<String,String>): Resource<BaseResponse<InvestProduct>>
    suspend fun getProductDetailById(data:HashMap<String,String>): Resource<ProductDetailResponse>

    suspend fun getAllArticles(): Resource<Article>

    suspend fun getArticleDetailsById(data:HashMap<String,String>): Resource<ArticleDetailResponse>
    suspend fun addLikeOnArticle(data:HashMap<String,String>): Resource<BlogLikeResponse>

    suspend fun addCommentOnArticle(data:HashMap<String,String>): Resource<BlogLikeResponse>

    suspend fun getAllProducts(data:HashMap<String,String>): Resource<BaseResponse<InvestProduct>>
    suspend fun addLikeOnProduct(data:HashMap<String,String>): Resource<BlogLikeResponse>

    suspend fun updateCommentOnArticle(data:HashMap<String,String>): Resource<BlogLikeResponse>

    suspend fun deleteCommentOnArticle(data:HashMap<String,String>): Resource<BlogLikeResponse>


    suspend fun getFavrouteProducts(data:HashMap<String,String>): Resource<WishlistResponse>

    suspend fun getUserStats(data:HashMap<String,String>): Resource<UserStatsResponse>
    suspend fun getNotifications(): Resource<NotificationResponse>

}