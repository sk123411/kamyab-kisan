package com.ruralculture.kayabkishan.dashboard.article

import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.text.HtmlCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.appbar.AppBarLayout.OnOffsetChangedListener
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.dashboard.repository.DashboardViewModel
import com.ruralculture.kayabkishan.databinding.FragmentArticleDetailBinding
import com.squareup.picasso.Picasso


class ArticleDetailFragment : Fragment() {

    lateinit var binding:FragmentArticleDetailBinding
    private val dashboardViewModel: DashboardViewModel by viewModels()
     var shareText:String?=""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {



        val htmlText = "<h2>What is Android?</h2>\n" + "<p>Android is an open source and Linux-based <b>Operating System</b> for mobile devices such as smartphones and tablet computers.Android was developed by the <i>Open Handset Alliance</i>, led by Google, and other companies.</p>\n" + "<p>Android offers a unified approach to application development for mobile devices which means developers need only develop for Android, and their applications should be able to run on different devices powered by Android.</p>\n" + "<p>The first beta version of the Android Software Development Kit (SDK) was released by Google in 2007 whereas the first commercial version, Android 1.0, was released in September 2008.</p>";
        binding = FragmentArticleDetailBinding.inflate(layoutInflater)




        binding.appBar.addOnOffsetChangedListener(OnOffsetChangedListener { appBarLayout, verticalOffset ->
            if (Math.abs(verticalOffset) - appBarLayout.totalScrollRange == 0) {
                //  Collapsed

                binding.rootToolbar.shareLayout.visibility = View.VISIBLE

                val sharePostButton = binding.rootToolbar.shareLayout.findViewById<ImageView>(R.id.shareButton)

                sharePostButton.setOnClickListener {


                    val intent2 = Intent(Intent.ACTION_SEND)
                    intent2.type = "text/plain"
                    intent2.putExtra(Intent.EXTRA_TEXT, shareText)
                    startActivity(Intent.createChooser(intent2, "Share via"))



                }



            } else {
                //Expanded
                binding.rootToolbar.shareLayout.visibility = View.GONE

            }
        })

//        // Inflate the layout for this fragment
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            binding.articleText.setText(HtmlCompat.fromHtml(htmlText, 0));
//        }else {
//            binding.articleText.setText(HtmlCompat.fromHtml(htmlText, 0));
//        }


        val id = arguments?.getString("id")

        if (id!=null){

            dashboardViewModel.getArticleDetailsById(id,Constant.getUserID(requireContext()).userID.toString())

            binding.userImageText.text = Constant.getUserID(requireContext()).userName.substring(0,2)




        }


        binding.sendCommentButton.setOnClickListener {


            if (binding.commentEditText.text.toString().isNotEmpty()){


                dashboardViewModel.addCommentOnArticle(id,Constant.getUserID(requireContext()).userID.toString(),binding.commentEditText.text.toString())



            }

        }




        lifecycleScope.launchWhenStarted {



            dashboardViewModel.dashboardEvent.observe(viewLifecycleOwner, Observer {

                when(it){


                    is DashboardViewModel.DashboardEvent.Loading -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true

                    }

                    is DashboardViewModel.DashboardEvent.SuccessArticleDetails -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false
                        binding.articleText.setText(HtmlCompat.fromHtml(it.articleDetails.description, 0));
                  //      binding.articleText.setText(HtmlCompat.fromHtml(htmlText, 0));
                        //binding.articleTitle.text = it.articleDetails.title
                        binding.toolbarLayout.title = it.articleDetails.title

                        binding.articleTIme.text = it.articleDetails.date
                        Picasso.get().load(Constant.BASE_IMAGE_PATH+it.articleDetails.image).into(binding.imageOne)
                        Picasso.get().load(Constant.BASE_IMAGE_PATH+it.articleDetails.image1).into(binding.imageTwo)
                        Picasso.get().load(Constant.BASE_IMAGE_PATH+it.articleDetails.image2).into(binding.imageThree)


                        binding.likeCountText.text = it.articleDetails.likeCount.toString()

                        shareText = it.articleDetails.title


                        if (it.articleDetails.like_state==0){

                            binding.likeImageButton.setImageResource(R.drawable.wishlist)


                            binding.likeImageButton.setOnClickListener {v->
                                binding.likeImageButton.setImageResource(R.drawable.heart)
                                binding.likeCountText.text = (it.articleDetails.likeCount + 1).toString()
                                dashboardViewModel.addLikeOnArticle(id,Constant.getUserID(requireContext()).userID.toString())
                            }


                        }else {
                            binding.likeImageButton.setImageResource(R.drawable.heart)


                            binding.likeImageButton.setOnClickListener {v->

                                binding.likeCountText.text = (it.articleDetails.likeCount - 1).toString()
                                binding.likeImageButton.setImageResource(R.drawable.wishlist)
                                dashboardViewModel.addLikeOnArticle(id,Constant.getUserID(requireContext()).userID.toString())

                            }

                        }



                        if (it.articleDetails.comment.size > 0) {
                            binding.commentList.apply {

                                layoutManager = LinearLayoutManager(context)
                                adapter =
                                    CommentAdapter(
                                        it.articleDetails.comment,
                                        dashboardViewModel
                                    )


                            }

                        }



                    }

                    is DashboardViewModel.DashboardEvent.SuccessBlogResponse -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false
                        dashboardViewModel.getArticleDetailsById(id,Constant.getUserID(requireContext()).userID.toString())



                        if (it.articleDetails.blog != null) {


                            if (it.articleDetails.blog.comment != null) {
                                binding.commentEditText.setText("")
                                Toast.makeText(context, "Comment posted", Toast.LENGTH_SHORT).show()


                            } else {

                                Toast.makeText(context, "Post liked", Toast.LENGTH_SHORT).show()
                            }


                        }else {



                            Toast.makeText(context, "Post disliked", Toast.LENGTH_SHORT).show()

                        }
                    }

                    is DashboardViewModel.DashboardEvent.SuccessCommentEditDeleteResponse -> {


                        dashboardViewModel.getArticleDetailsById(id,Constant.getUserID(requireContext()).userID.toString())
                        if (it.articleDetails.blog != null) {


                            if (it.articleDetails.blog.comment != null) {

                                Toast.makeText(context, "Comment updated", Toast.LENGTH_SHORT).show()


                            } else {

                                Toast.makeText(context, "Post liked", Toast.LENGTH_SHORT).show()
                            }


                        }else {



                            Toast.makeText(context, "Comment deleted", Toast.LENGTH_SHORT).show()

                        }


                    }
                }






            })


        }











        return binding.root
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)

        inflater.inflate(R.menu.dashboard_detail_menu,menu)


    }

}