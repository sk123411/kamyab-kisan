package com.ruralculture.kayabkishan.invest

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.navigation.Navigation
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.databinding.FragmentInvestBinding
import com.ruralculture.kayabkishan.model.ProductDetailResponse
import io.paperdb.Paper

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [InvestFragment.newInstance] factory method to
 * create an instance of this fragment.
 */


class InvestFragment : Fragment() {


    lateinit var binding:FragmentInvestBinding
    var count:Int? = 0

    companion object{

        lateinit var fmanager:FragmentManager
    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentInvestBinding.inflate(layoutInflater)
        Paper.init(requireContext())
        val product = Paper.book().read<ProductDetailResponse>(Constant.PRODUCT)

        binding.plusButton.setOnClickListener {
            count = count?.inc()
            binding.countText.text = count.toString()

            val totalPrice = product.productPrice.toDouble() * count!!
            binding.totalPrice.text = "PKR "+ totalPrice.toString()


        }

        binding.minusButton.setOnClickListener {
            count = count?.dec()
            binding.countText.text = count.toString()

            val totalPrice = product.productPrice.toDouble() * count!!
            binding.totalPrice.text = "PKR "+ totalPrice.toString()

        }






        binding.investNowButton.setOnClickListener {

            val total = product.productPrice.toDouble() * count!!
            product.price_total = total
            product.unit = count!!
            Paper.book().write(Constant.PRODUCT,product)
            Navigation.findNavController(it).navigate(R.id.paymentFragment2)
        }




        return binding.root



    }








}