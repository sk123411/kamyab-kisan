package com.ruralculture.kayabkishan.invest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.databinding.ActivityInvestBinding

class InvestActivity : AppCompatActivity() {

    lateinit var binding:ActivityInvestBinding
    lateinit var navController: NavController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityInvestBinding.inflate(layoutInflater)

        setContentView(binding.root)


         navController = findNavController(R.id.investHost)






        navController.addOnDestinationChangedListener(object:NavController.OnDestinationChangedListener{
            override fun onDestinationChanged(
                controller: NavController,
                destination: NavDestination,
                arguments: Bundle?
            ) {


                when(destination.id){

                    R.id.paymentFragment2 -> {

                        binding.unitUncheckImage.visibility = View.GONE
                        binding.unitCheckImage.visibility = View.VISIBLE
                        binding.countText.setTextColor(resources.getColor(R.color.light_orange))



//
                        binding.paymentUnCheckImage.visibility = View.VISIBLE
                        binding.paymentCheckImage.visibility = View.GONE
                        binding.paymentText.setTextColor(resources.getColor(R.color.black))



                        binding.statusUnCheckImage.visibility = View.VISIBLE
                        binding.statusCheckImage.visibility = View.GONE



                    }
                    R.id.statusFragment -> {

                        binding.unitUncheckImage.visibility = View.GONE
                        binding.unitCheckImage.visibility = View.VISIBLE
                        binding.countText.setTextColor(resources.getColor(R.color.light_orange))



                        binding.paymentUnCheckImage.visibility = View.GONE
                        binding.paymentCheckImage.visibility = View.VISIBLE
                        binding.paymentText.setTextColor(resources.getColor(R.color.light_orange))



                        binding.statusUnCheckImage.visibility = View.VISIBLE
                        binding.statusCheckImage.visibility = View.GONE


                    }



                    R.id.investFragmentOne -> {
                        binding.unitUncheckImage.visibility = View.VISIBLE
                        binding.unitCheckImage.visibility = View.GONE
                        binding.countText.setTextColor(resources.getColor(R.color.black))


                        binding.paymentUnCheckImage.visibility = View.VISIBLE
                        binding.paymentCheckImage.visibility = View.GONE
                        binding.paymentText.setTextColor(resources.getColor(R.color.black))



                        binding.statusUnCheckImage.visibility = View.VISIBLE
                        binding.statusCheckImage.visibility = View.GONE
                        binding.statusText.setTextColor(resources.getColor(R.color.black))




                    }





                }


            }

        })

    }


    override fun onBackPressed() {

        if (navController.currentDestination?.id== R.id.paymentFragment2){

            navController.navigate(R.id.investFragmentOne)
        }else  if (navController.currentDestination?.id== R.id.statusFragment){
            navController.navigate(R.id.paymentFragment2)

        }else  if (navController.currentDestination?.id== R.id.investFragmentOne){
            super.onBackPressed()

        }

    }
}