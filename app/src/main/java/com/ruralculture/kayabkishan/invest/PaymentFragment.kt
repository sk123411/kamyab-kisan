package com.ruralculture.kayabkishan.invest

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.databinding.FragmentPaymentBinding
import com.ruralculture.kayabkishan.databinding.PaymentMethodsLytBinding
import com.ruralculture.kayabkishan.model.ProductDetailResponse
import com.ruralculture.kayabkishan.order.myinvestment.MyInvestmentActivity
import com.ruralculture.kayabkishan.order.OrderViewModel
import com.squareup.picasso.Picasso
import io.paperdb.Paper


class PaymentFragment : Fragment() {

    private val orderViewModel: OrderViewModel by viewModels()

    lateinit var binding: FragmentPaymentBinding
    var paymentType: String? = ""
    lateinit var product: ProductDetailResponse
    private var orderID: String? = "0"
    private var isOrderAdd:Boolean = false
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentPaymentBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment
        Paper.init(requireContext())


        product = Paper.book().read<ProductDetailResponse>(Constant.PRODUCT)
        val vid = Paper.book().read<String>(Constant.ORDER_ID)
        val userID = Constant.getUserID(requireContext()).userID.toString()
        orderViewModel.getOrders(userID)

        binding.productTitle.text = product.productName
        binding.prPrice.text = product.price_total.toString()
        binding.productUnit.text = product.unit.toString()
        Picasso.get().load(Constant.BASE_IMAGE_PATH + product.productImage)
        binding.productUnitPrice.text = product.productPrice
        binding.totalPrice.text = product.price_total.toString()
        binding.finalPrice.text = product.price_total.toString()




        lifecycleScope.launchWhenStarted {


            orderViewModel.orderEvent.observe(viewLifecycleOwner, Observer {


                when (it) {

                    is OrderViewModel.OrderEvent.SuccessOrder -> {

                        if(!it.orderResponse.id.equals("")){
                            isOrderAdd = true
                            orderID=it.orderResponse.id

                            Paper.init(requireActivity())
                            Paper.book().write(Constant.ORDER_ID,it.orderResponse.id)

                        }

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false
                        Toast.makeText(
                            requireContext(),
                            "" + it.orderResponse.payment_type + " selected",
                            Toast.LENGTH_LONG
                        ).show()

                    }
                    is OrderViewModel.OrderEvent.SuccessOrderList -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false



                        if (it.orderResponse.data!=null&&it.orderResponse.data.size>0) {
                            for (id in it.orderResponse.data) {


                                if (product.productId.equals(id.productId)) {

                                    isOrderAdd = true
                                    orderID = id.id


                                }

                            }
                        }

                    }
                    is OrderViewModel.OrderEvent.Loading -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true

                    }


                }


            })


        }

        binding.investNowButton.setOnClickListener {

            startActivity(Intent(requireContext(), MyInvestmentActivity::class.java))

        }



        binding.selectPaymentLayout.setOnClickListener {


            openPaymentDialog()
        }



        return binding.root
    }

    private fun openPaymentDialog() {



        val vid = Paper.book().read<String>(Constant.ORDER_ID)
        Log.d("BBBBBBBBBBBBB", "::"+vid)

        val bottomSheetDialog = BottomSheetDialog(requireContext())
        val dialogBinding = PaymentMethodsLytBinding.inflate(layoutInflater)

        bottomSheetDialog.setContentView(dialogBinding.root)
        bottomSheetDialog.show()





        dialogBinding.paytmRadio.setOnClickListener {


            dialogBinding.paythSelectedLayout.visibility = View.VISIBLE
            paymentType = "paytm"

            dialogBinding.visaSelectedLayout.visibility = View.GONE
            dialogBinding.epaySelectedLayout.visibility = View.GONE



            Log.d("POOOOOOOOOO","::"+orderID)


            if (isOrderAdd) {

                Log.d("LLLLLLLLLLL","true")

                orderViewModel.changeOrderPaymentType(
                    Constant.getUserID(requireContext()).userID.toString(),
                    paymentType,
                    orderID)

            }
            else {


                orderViewModel.placeOrder(
                    Constant.getUserID(requireContext()).userID.toString(),
                    product.price_total.toString(),
                    paymentType,
                    product.unit.toString(),
                    product.productId,
                    "0",
                    product.productPrice
                )

            }

        }



        dialogBinding.epayRadio.setOnClickListener {



            paymentType = "epay"

            dialogBinding.paythSelectedLayout.visibility = View.GONE
            dialogBinding.visaSelectedLayout.visibility = View.GONE
            dialogBinding.epaySelectedLayout.visibility = View.VISIBLE

            if (isOrderAdd) {

                orderViewModel.changeOrderPaymentType(
                    Constant.getUserID(requireContext()).userID.toString(),
                    paymentType,
                    orderID)

            } else {


                orderViewModel.placeOrder(
                    Constant.getUserID(requireContext()).userID.toString(),
                    product.price_total.toString(),
                    paymentType,
                    product.unit.toString(),
                    product.productId,
                    "0",
                    product.productPrice
                )

            }


        }


        dialogBinding.visaRadio.setOnClickListener {

            paymentType = "visa"


            dialogBinding.paythSelectedLayout.visibility = View.GONE
            dialogBinding.visaSelectedLayout.visibility = View.VISIBLE
            dialogBinding.epaySelectedLayout.visibility = View.GONE

            if (isOrderAdd) {

                orderViewModel.changeOrderPaymentType(
                    Constant.getUserID(requireContext()).userID.toString(),
                    paymentType,
                    orderID)

            } else {


                orderViewModel.placeOrder(
                    Constant.getUserID(requireContext()).userID.toString(),
                    product.price_total.toString(),
                    paymentType,
                    product.unit.toString(),
                    product.productId,
                    "0",
                    product.productPrice
                )

            }
        }


        dialogBinding.epaySelectedLayout.setOnClickListener {

            bottomSheetDialog.dismiss()
        }

        dialogBinding.paythSelectedLayout.setOnClickListener {

            bottomSheetDialog.dismiss()
        }


        dialogBinding.visaSelectedLayout.setOnClickListener {

            bottomSheetDialog.dismiss()
        }


    }


}