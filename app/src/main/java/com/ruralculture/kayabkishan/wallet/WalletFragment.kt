package com.ruralculture.kayabkishan.wallet

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.databinding.FragmentWalletBinding
import com.ruralculture.kayabkishan.profile.repository.ProfileViewModel
import com.ruralculture.kayabkishan.wallet.adapter.WalletTransactionAdapter

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [WalletFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class WalletFragment : Fragment() {

  lateinit var binding:FragmentWalletBinding

  private val profileViewModel:ProfileViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentWalletBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment

        profileViewModel.getWalletBalance(Constant.getUserID(requireContext()).userID.toString())





        lifecycleScope.launchWhenStarted {

            profileViewModel.profileEvent.observe(viewLifecycleOwner, Observer {

                when(it){

                    is ProfileViewModel.ProfileEvent.SuccessWalletBalance -> {


                        binding.walletTransactionText.setText(it.walletResponse.amount)


                    }

                    is ProfileViewModel.ProfileEvent.Failure -> {

                    }

                    is ProfileViewModel.ProfileEvent.Loading -> {

                    }


                }






            })





        }

        binding.walletTransactionText.setText("PKR 800")
        binding.walletTransactionList.apply {

            layoutManager = LinearLayoutManager(context)
            adapter = WalletTransactionAdapter()

        }


        return binding.root
    }

}