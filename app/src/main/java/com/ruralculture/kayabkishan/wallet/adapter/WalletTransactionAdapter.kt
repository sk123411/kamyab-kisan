package com.ruralculture.kayabkishan.wallet.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ruralculture.kayabkishan.databinding.WalletTransactionItemBinding

class WalletTransactionAdapter :RecyclerView.Adapter<WalletTransactionAdapter.MyViewHolder>(){






    class MyViewHolder(v:View):RecyclerView.ViewHolder(v) {

        fun bind(){

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val binding = WalletTransactionItemBinding.inflate(LayoutInflater.from(parent.context))

        return  MyViewHolder(binding.root)

    }

    override fun getItemCount(): Int {
      return  10
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {


    }

}