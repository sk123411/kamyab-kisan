package com.ruralculture.kayabkishan.wallet.model


import com.google.gson.annotations.SerializedName

data class WalletResponse(
    @SerializedName("amount")
    val amount: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("user_id")
    val userId: String
)