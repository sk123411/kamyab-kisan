package com.ruralculture.kayabkishan.order.repository

import com.ruralculture.kayabkishan.helper.Resource
import com.ruralculture.kayabkishan.order.model.ChangeOrderStatusResponse
import com.ruralculture.kayabkishan.order.model.OrderListResponse
import com.ruralculture.kayabkishan.order.model.OrderResponse

interface Order {

    suspend fun placeOrder(data:HashMap<String,String>): Resource<OrderResponse>
    suspend fun getOrders(data:HashMap<String,String>): Resource<OrderListResponse>
    suspend fun changeOrderStatus(data:HashMap<String,String>): Resource<ChangeOrderStatusResponse>
    suspend fun changeOrderPaymentType(data:HashMap<String,String>): Resource<ChangeOrderStatusResponse>


}