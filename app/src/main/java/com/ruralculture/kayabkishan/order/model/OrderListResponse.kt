package com.ruralculture.kayabkishan.order.model


import com.google.gson.annotations.SerializedName
import com.ruralculture.kayabkishan.order.model.Data

data class OrderListResponse(
    @SerializedName("data")
    val `data`: List<Data>,
    @SerializedName("result")
    val result: String
)