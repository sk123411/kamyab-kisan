package com.ruralculture.kayabkishan.order.model


import com.google.gson.annotations.SerializedName

data class DataX(
    @SerializedName("amount")
    val amount: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("order_id")
    val orderId: String,
    @SerializedName("time")
    val time: String,
    @SerializedName("user_id")
    val userId: String
)