package com.ruralculture.kayabkishan.order.repository

import com.ruralculture.kayabkishan.order.model.ChangeOrderStatusResponse
import com.ruralculture.kayabkishan.order.model.OrderListResponse
import com.ruralculture.kayabkishan.order.model.OrderResponse
import retrofit2.Response
import retrofit2.http.*

interface OrdersAPI {


    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun placeOrder(@Query("action") action:String?, @FieldMap data:HashMap<String,String>): Response<OrderResponse>


    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun getOrders(@Query("action") action:String?, @FieldMap data:HashMap<String,String>): Response<OrderListResponse>

    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun changeOrderStatus(@Query("action") action:String?, @FieldMap data:HashMap<String,String>): Response<ChangeOrderStatusResponse>


    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun changeOrderPaymentType(@Query("action") action:String?, @FieldMap data:HashMap<String,String>): Response<ChangeOrderStatusResponse>




}