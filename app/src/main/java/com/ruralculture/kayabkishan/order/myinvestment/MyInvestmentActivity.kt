package com.ruralculture.kayabkishan.order.myinvestment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import androidx.navigation.ui.setupActionBarWithNavController
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.databinding.ActivityMyInvestmentBinding

class MyInvestmentActivity : AppCompatActivity() {
    lateinit var binding: ActivityMyInvestmentBinding
    lateinit var navController: NavController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMyInvestmentBinding.inflate(layoutInflater)

        setContentView(binding.root)


        navController = findNavController(R.id.myinvestHost)

        setSupportActionBar(binding.rootToolbar.toolbarMain)
        setupActionBarWithNavController(navController)
        binding.rootToolbar.toolbarMain.setTitle(resources.getString(R.string.my_investment_title))





        binding.confirmationButton.setOnClickListener {

            navController.navigate(R.id.pendingPaymentListFragment)
            
        }

        binding.progressButton.setOnClickListener {

            navController.navigate(R.id.progressInvementListFragment)
        }

        binding.completedButton.setOnClickListener {

            navController.navigate(R.id.completedInvestmentsFragment)
        }


        navController.addOnDestinationChangedListener(object :
            NavController.OnDestinationChangedListener {
            override fun onDestinationChanged(
                controller: NavController,
                destination: NavDestination,
                arguments: Bundle?
            ) {
                when (destination.id) {


                    R.id.pendingPaymentListFragment -> {
                        binding.rootToolbar.toolbarMain.setTitle(resources.getString(R.string.my_investment_title))


                        binding.comfirmationRoot.setBackgroundResource(R.drawable.circle_green)

                        binding.confirmationCard.setColorFilter(
                            ContextCompat.getColor(
                                applicationContext,
                                R.color.green
                            ), android.graphics.PorterDuff.Mode.MULTIPLY
                        );

                        binding.confirmationProgress.setBackgroundResource(R.drawable.circle_grey)

                        binding.progressChart.setColorFilter(
                            ContextCompat.getColor(
                                applicationContext,
                                android.R.color.black
                            ), android.graphics.PorterDuff.Mode.MULTIPLY
                        );


                        binding.investmentText.setTextColor(resources.getColor(R.color.black))
                        binding.waitingText.setTextColor(resources.getColor(R.color.green))
                        binding.completedText.setTextColor(resources.getColor(R.color.black))



                        binding.confirmationCompleted.setBackgroundResource(R.drawable.circle_grey)

                        binding.completedCheck.setImageResource(R.drawable.completed)
                    }

                    R.id.progressInvementListFragment -> {


                        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back_ios_24)
                        binding.rootToolbar.toolbarMain.setTitle(resources.getString(R.string.my_investment_title))


                        binding.investmentText.setTextColor(resources.getColor(R.color.green))
                        binding.waitingText.setTextColor(resources.getColor(R.color.black))
                        binding.completedText.setTextColor(resources.getColor(R.color.black))


                        binding.confirmationProgress.setBackgroundResource(R.drawable.circle_green)

                        binding.progressChart.setColorFilter(
                            ContextCompat.getColor(
                                applicationContext,
                                R.color.green
                            ), android.graphics.PorterDuff.Mode.MULTIPLY
                        );


                        binding.comfirmationRoot.setBackgroundResource(R.drawable.circle_grey)

                        binding.confirmationCard.setColorFilter(
                            ContextCompat.getColor(
                                applicationContext,
                                android.R.color.black
                            ), android.graphics.PorterDuff.Mode.MULTIPLY
                        );

                        binding.confirmationCompleted.setBackgroundResource(R.drawable.circle_grey)

                        binding.completedCheck.setImageResource(R.drawable.completed)


                    }


                    R.id.completedInvestmentsFragment -> {

                        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back_ios_24)
                        binding.rootToolbar.toolbarMain.setTitle(resources.getString(R.string.my_investment_title))


                        binding.confirmationCompleted.setBackgroundResource(R.drawable.circle_green)

                        binding.completedCheck.setImageResource(R.drawable.check_gr)

                        binding.confirmationProgress.setBackgroundResource(R.drawable.circle_grey)

                        binding.progressChart.setColorFilter(
                            ContextCompat.getColor(
                                applicationContext,
                                android.R.color.black
                            ), android.graphics.PorterDuff.Mode.MULTIPLY
                        );


                        binding.comfirmationRoot.setBackgroundResource(R.drawable.circle_grey)

                        binding.confirmationCard.setColorFilter(
                            ContextCompat.getColor(
                                applicationContext,
                                android.R.color.black
                            ), android.graphics.PorterDuff.Mode.MULTIPLY
                        );



                        binding.investmentText.setTextColor(resources.getColor(R.color.black))
                        binding.waitingText.setTextColor(resources.getColor(R.color.black))
                        binding.completedText.setTextColor(resources.getColor(R.color.green))


                    }


                    else -> {


                    }
                }


            }


        })


    }


    override fun onBackPressed() {

        if (navController.currentDestination?.id == R.id.progressInvementListFragment) {

            navController.navigate(R.id.pendingPaymentListFragment)
        } else if (navController.currentDestination?.id == R.id.pendingPaymentListFragment) {
            super.onBackPressed()

        } else if (navController.currentDestination?.id == R.id.completedInvestmentsFragment) {
            navController.navigate(R.id.progressInvementListFragment)

        } else {
            super.onBackPressed()


        }

    }


    override fun onSupportNavigateUp(): Boolean {

        if (navController.currentDestination?.id==R.id.completedInvestmentsFragment) {
            navController.navigate(R.id.progressInvementListFragment)

        }else if(navController.currentDestination?.id == R.id.progressInvementListFragment){
            navController.navigate(R.id.pendingPaymentListFragment)
        }else {
            onBackPressed()
        }
        return false
    }

}