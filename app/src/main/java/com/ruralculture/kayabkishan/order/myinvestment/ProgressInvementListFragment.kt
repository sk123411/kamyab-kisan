package com.ruralculture.kayabkishan.order.myinvestment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.databinding.FragmentProgressInvementListBinding
import com.ruralculture.kayabkishan.order.OrderViewModel
import com.ruralculture.kayabkishan.order.myinvestment.adapter.PendingPaymentAdapter
import com.ruralculture.kayabkishan.order.myinvestment.adapter.ProgressPaymentAdapter


class ProgressInvementListFragment : Fragment() {
    lateinit var binding: FragmentProgressInvementListBinding
    private val orderViewModel: OrderViewModel by viewModels()

    companion object{

        lateinit var fmanager: FragmentManager
    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentProgressInvementListBinding.inflate(layoutInflater)

        val userID = Constant.getUserID(requireContext()).userID.toString()
        orderViewModel.getOrders(userID)



        lifecycleScope.launchWhenStarted {


            orderViewModel.orderEvent.observe(viewLifecycleOwner, Observer {

                when(it){



                    is OrderViewModel.OrderEvent.SuccessOrderList -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false



                        binding.progressList.apply {

                            if (it.orderResponse.result.equals("successfully")) {


                                it.orderResponse


                                if (it.orderResponse.data.size > 0) {

                                    binding.noDataView.visibility = View.GONE
                                    layoutManager = LinearLayoutManager(context)
                                    adapter = PendingPaymentAdapter(it.orderResponse.data.filter { p -> p.status.equals("1") })



                                }else {

                                    binding.noDataView.visibility = View.VISIBLE
                                }




                            }else {
                                binding.noDataView.visibility = View.VISIBLE

                            }
                        }


                    }


                    is OrderViewModel.OrderEvent.Loading -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true



                    }

                    is OrderViewModel.OrderEvent.Failure -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false
                        binding.noDataView.visibility = View.VISIBLE



                    }
                }



            })


        }

        return binding.root



    }
}