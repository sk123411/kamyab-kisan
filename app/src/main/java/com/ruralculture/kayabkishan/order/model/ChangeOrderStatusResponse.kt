package com.ruralculture.kayabkishan.order.model


import com.google.gson.annotations.SerializedName
import com.ruralculture.kayabkishan.order.model.DataX

data class ChangeOrderStatusResponse(
    @SerializedName("data")
    val `data`: DataX,
    @SerializedName("result")
    val result: String
)