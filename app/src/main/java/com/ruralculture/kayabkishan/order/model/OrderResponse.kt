package com.ruralculture.kayabkishan.order.model

import com.google.gson.annotations.SerializedName

data class OrderResponse(
    @SerializedName("id")
    val id: String,
    @SerializedName("order_id")

    val order_id: String,
    @SerializedName("payment_type")

    val payment_type: String,
    @SerializedName("product_id")

    val product_id: String,
    @SerializedName("product_price")

    val product_price: String,
    @SerializedName("quantity")

    val quantity: String,
    @SerializedName("result")

    val result: String,
    @SerializedName("status")

    val status: String,
    @SerializedName("strtotime")

    val strtotime: String,
    @SerializedName("times")

    val times: String,
    @SerializedName("times_date")

    val times_date: String,
    @SerializedName("total_amount")

    val total_amount: String,
    @SerializedName("user_id")
    val user_id: String
)