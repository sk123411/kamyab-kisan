package com.ruralculture.kayabkishan.order.myinvestment.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.databinding.PendingPayItemBinding
import com.ruralculture.kayabkishan.invest.InvestActivity
import com.ruralculture.kayabkishan.order.model.Data
import io.paperdb.Paper

class PendingPaymentAdapter constructor(val list: List<Data>) :
    RecyclerView.Adapter<PendingPaymentViewholder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PendingPaymentViewholder {

        val view = PendingPayItemBinding.inflate(LayoutInflater.from(parent.context))

        return PendingPaymentViewholder(view.root)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: PendingPaymentViewholder, position: Int) {


        holder.binding.root.setOnClickListener {

            Navigation.findNavController(it).navigate(R.id.progressInvementListFragment)
        }




        holder.setData(list.get(position))
    }


}

class PendingPaymentViewholder(view: View) : RecyclerView.ViewHolder(view) {

    val binding = PendingPayItemBinding.bind(view)


    fun setData(investItem: Data) {

        if (investItem.status.equals("0")) {

            //Checking for pending payment products


            binding.prTitle.text = investItem.productData.productName
            binding.unitBought.text = investItem.quantity + " unit"
            binding.investPrice.text = investItem.totalAmount
            binding.investNowButton.setText("Pay")


//            binding.investNowButton.setOnClickListener {
//                Paper.init(it.context)
//                Paper.book().write(Constant.PRODUCT,investItem)
//                it.context.startActivity(Intent(it.context,InvestActivity::class.java))
//
//            }
        }else if (investItem.status.equals("1")){
            binding.investNowButton.setText("in Progress")
            binding.investNowButton.setBackgroundColor(itemView.resources.getColor(
                R.color.light_orange))



        }else if (investItem.status.equals("2")){

            binding.investNowButton.setText("Completed investment")
        
        }
    }

}
