package com.ruralculture.kayabkishan.order.repository

import android.util.Log
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.helper.Resource
import com.ruralculture.kayabkishan.order.model.ChangeOrderStatusResponse
import com.ruralculture.kayabkishan.order.model.OrderListResponse
import com.ruralculture.kayabkishan.order.model.OrderResponse

class OrderImplemenation  constructor(var ordersAPI: OrdersAPI) :
    Order {




    override suspend fun placeOrder(data: HashMap<String, String>): Resource<OrderResponse> {
        return try {

            val response = ordersAPI.placeOrder(Constant.API_PLACE_ORDER,data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result!!)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }

    }

    override suspend fun getOrders(data: HashMap<String, String>): Resource<OrderListResponse> {
        return try {

            val response = ordersAPI.getOrders(Constant.API_GET_ORDERS,data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result!!)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun changeOrderStatus(data: HashMap<String, String>): Resource<ChangeOrderStatusResponse> {
        return try {

            val response = ordersAPI.changeOrderStatus(Constant.API_CHANGE_ORDER_STATUS,data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result!!)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }

    }

    override suspend fun changeOrderPaymentType(data: HashMap<String, String>): Resource<ChangeOrderStatusResponse> {
        return try {

            val response = ordersAPI.changeOrderPaymentType(Constant.API_CHANGE_ORDER_PAYMENT_TYPE,data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result!!)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }     }

}