package com.ruralculture.kayabkishan.order.myinvestment.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.databinding.ProgressItemBinding
import com.ruralculture.kayabkishan.model.DashboardItem

class ProgressPaymentAdapter(val b: Boolean) :RecyclerView.Adapter<ProgressPaymentViewholder> (){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProgressPaymentViewholder {

        val view = ProgressItemBinding.inflate(LayoutInflater.from(parent.context))

        return ProgressPaymentViewholder(view.root)

    }

    override fun getItemCount(): Int {
        return 45
    }

    override fun onBindViewHolder(holder: ProgressPaymentViewholder, position: Int) {


        holder.binding.root.setOnClickListener {




            Navigation.findNavController(it).navigate(R.id.completedInvestmentsFragment)



        }

        if (b){
            holder.binding.investNowButton.setText("Completed")

        }
    }


}

class ProgressPaymentViewholder(view:View) :RecyclerView.ViewHolder(view) {

    val binding = ProgressItemBinding.bind(view)

    fun setData(investItem: DashboardItem) {


        
    }

}
