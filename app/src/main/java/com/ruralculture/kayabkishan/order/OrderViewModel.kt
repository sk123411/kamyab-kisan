package com.ruralculture.kayabkishan.order

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ruralculture.kayabkishan.helper.Resource
import com.ruralculture.kayabkishan.order.model.ChangeOrderStatusResponse
import com.ruralculture.kayabkishan.order.model.OrderListResponse
import com.ruralculture.kayabkishan.order.model.OrderResponse
import com.ruralculture.kayabkishan.order.repository.Order
import com.ruralculture.kayabkishan.order.repository.OrderImplemenation
import com.ruralculture.kayabkishan.order.repository.OrdersAPI
import com.ruralculture.kayabkishan.retofit.RetrofitInstance
import kotlinx.coroutines.launch

class OrderViewModel : ViewModel() {


    private val orderEvent_ = MutableLiveData<OrderEvent>()
    lateinit var ordersAPI: OrdersAPI
    lateinit var order: Order


    val orderEvent: LiveData<OrderEvent>
        get() = orderEvent_


    init {
        ordersAPI = RetrofitInstance.getRetrofitInstance().create(OrdersAPI::class.java)
        order = OrderImplemenation(
            ordersAPI
        )


    }


    sealed class OrderEvent {
        class SuccessOrderList(val orderResponse: OrderListResponse) : OrderEvent()
        class SuccessOrderPaymentType(val orderResponse: ChangeOrderStatusResponse) : OrderEvent()

        class SuccessOrder(val orderResponse: OrderResponse) : OrderEvent()
        class Failure(val message: String?) : OrderEvent()
        object Loading : OrderEvent()
        object Empty : OrderEvent()
    }


    fun placeOrder(userID:String?, totalAmount:String?, paymentType:String?,quantity:String?,productID:String?,status:String? ,singleUnitPrice:String?) {
        orderEvent_.value = OrderEvent.Loading

        val map = HashMap<String,String>()
        map.put("user_id",userID!!)
        map.put("total_amount",totalAmount!!)
        map.put("payment_type",paymentType!!)
        map.put("quantity",quantity!!)
        map.put("product_id",productID!!)
        map.put("status",status!!)
        map.put("single_unit_price",singleUnitPrice!!)

        android.util.Log.d("CCCCCCCCCCCCcc", map.toString())



        viewModelScope.launch {

            val response = order.placeOrder(data = map)

            when (response) {

                is Resource.Success -> {


                    orderEvent_.value = OrderEvent.SuccessOrder(response.data!!)


                }

                is Resource.Error -> {
                    orderEvent_.value = OrderEvent.Failure("Sign in success")


                }


            }


        }


    }











    fun changeOrderPaymentType(userID:String?, paymentType:String?, orderID:String?) {
        orderEvent_.value = OrderEvent.Loading

        val map = HashMap<String,String>()
        map.put("user_id",userID!!)
        map.put("payment_type",paymentType!!)
        map.put("order_id",orderID!!)

        viewModelScope.launch {

            val response = order.changeOrderPaymentType(data = map)

            when (response) {

                is Resource.Success -> {


                    orderEvent_.value = OrderEvent.SuccessOrderPaymentType(response.data!!)


                }

                is Resource.Error -> {
                    orderEvent_.value = OrderEvent.Failure("Sign in success")


                }


            }


        }


    }

    fun getOrders(userID:String?) {
        orderEvent_.value = OrderEvent.Loading

        val map = HashMap<String,String>()
        map.put("user_id",userID!!)




        viewModelScope.launch {

            val response = order.getOrders(data = map)

            when (response) {

                is Resource.Success -> {


                    orderEvent_.value = OrderEvent.SuccessOrderList(response.data!!)


                }

                is Resource.Error -> {
                    orderEvent_.value = OrderEvent.Failure("Sign in success")


                }


            }


        }


    }

}

