package com.ruralculture.kayabkishan.order.model


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("id")
    val id: String,
    @SerializedName("order_id")
    val orderId: String,
    @SerializedName("payment_type")
    val paymentType: String,
    @SerializedName("product_data")
    val productData: ProductData,
    @SerializedName("product_id")
    val productId: String,
    @SerializedName("product_price")
    val productPrice: String,
    @SerializedName("quantity")
    val quantity: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("strtotime")
    val strtotime: String,
    @SerializedName("times")
    val times: String,
    @SerializedName("times_date")
    val timesDate: String,
    @SerializedName("total_amount")
    val totalAmount: String,
    @SerializedName("user_id")
    val userId: String



){


    companion object {


//
//            fun isPaymentStatusOne():Comparator<Data> = object :Comparator<Data>{
//                override fun compare(o1: Data?, o2: Data?): Int {
//
//                    return o2!!.name!!.compareTo(o1!!.name!!)
//                }
//
//            }




    }
}