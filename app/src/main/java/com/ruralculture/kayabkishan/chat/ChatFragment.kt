package com.ruralculture.kayabkishan.chat

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.ruralculture.kayabkishan.databinding.FragmentChatBinding
import com.ruralculture.kayabkishan.support.adapter.ChatMessagesListAdapter

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ChatFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ChatFragment : Fragment() {
   lateinit var binding:FragmentChatBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentChatBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment

        binding.chatList.apply {

            layoutManager = LinearLayoutManager(context)
            adapter = ChatMessagesListAdapter(listOf())

        }


        return binding.root
    }


}