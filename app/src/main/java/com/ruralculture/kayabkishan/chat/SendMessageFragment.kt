package com.ruralculture.kayabkishan.chat

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.R
import com.ruralculture.kayabkishan.databinding.FragmentSendMessageBinding
import com.ruralculture.kayabkishan.support.adapter.ChatMessagesListAdapter
import com.ruralculture.kayabkishan.support.data.SupportViewModel

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [SendMessageFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SendMessageFragment : Fragment() {

    lateinit var binding: FragmentSendMessageBinding
    private val supportViewModel: SupportViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSendMessageBinding.inflate(layoutInflater)

        val receiverId = arguments?.getString("id")
        supportViewModel.getAllMessagesUser(
            Constant.getUserID(requireContext()).userID.toString(),
            Constant.getUserID(requireContext()).userID.toString(), "0"
        )

        // Inflate the layout for this fragment

        lifecycleScope.launchWhenStarted {


            supportViewModel.supportEvent.observe(viewLifecycleOwner, Observer {


                when (it) {

                    is SupportViewModel.OrderEvent.SuccessAllMessagesUser -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false


                        binding.messagesList.apply {

                            layoutManager = LinearLayoutManager(context)
                            adapter = ChatMessagesListAdapter(it.showAllMessagesUserResponse.data)

                        }
                    }

                    is SupportViewModel.OrderEvent.Loading -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true


                    }
                    is SupportViewModel.OrderEvent.SuccessMessageSent -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                        binding.messageEdit.setText("")
                        Toast.makeText(requireContext(), "Message sent", Toast.LENGTH_LONG).show()
                        supportViewModel.getAllMessagesUser(
                            Constant.getUserID(requireContext()).userID.toString(),
                            Constant.getUserID(requireContext()).userID.toString(), "0"
                        )
                    }

                }


            })


        }


        binding.sendMessageButton.setOnClickListener {


            if (binding.messageEdit.text.isNotEmpty()) {

                supportViewModel.sendMessage(

                    Constant.getUserID(requireContext()).userID.toString(),
                    Constant.getUserID(requireContext()).userID.toString(),
                    "0",
                    binding.messageEdit.text.toString()

                )


            }


        }



        return binding.root
    }


}