package com.ruralculture.kayabkishan.retofit

import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitInstance {


    companion object {


        var instance: Retrofit? = null
        var BASE_URL: String? = "https://ruparnatechnology.com/Kamyabkisan/"

        fun getRetrofitInstance(): Retrofit {

            if (instance == null) {
                instance = Retrofit.Builder().baseUrl(BASE_URL!!)
                    .addConverterFactory(GsonConverterFactory.create(
                        GsonBuilder()
                        .setLenient()
                        .create())).build()

            }

            return instance!!

        }
    }


}