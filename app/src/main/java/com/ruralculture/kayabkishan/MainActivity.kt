package com.ruralculture.kayabkishan

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.*
import androidx.navigation.ui.setupActionBarWithNavController
import com.freshchat.consumer.sdk.Freshchat
import com.freshchat.consumer.sdk.FreshchatConfig
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.ruralculture.Constant
import com.ruralculture.kayabkishan.databinding.ActivityMainBinding
import com.ruralculture.kayabkishan.order.myinvestment.MyInvestmentActivity
import com.ruralculture.kayabkishan.support.CustomImageLoader
import io.paperdb.Paper

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    private var isFromPendingFragment: Boolean = false

    companion object {

        lateinit var bottomNavigation: BottomNavigationView
        lateinit var binding: ActivityMainBinding

    }




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val navController = findNavController(R.id.nav_host_fragment)




        bottomNavigation = binding.bottomNavigation
        binding.bottomNavigation.visibility = View.VISIBLE

        setSupportActionBar(binding.rootToolbar.toolbarMain)
        setupActionBarWithNavController(navController)


        isFromPendingFragment = intent.getBooleanExtra(Constant.PENDING_ORDER, false)

        if (isFromPendingFragment) {

            val id = intent.getStringExtra("id")
            val bundle = Bundle()
            bundle.putString("id", id)
            navController.navigate(R.id.dashboardDetail, bundle)

        }



        binding.bottomNavigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.bottom_home -> {
                    // Respond to navigation item 1 click

                    navController.navigate(R.id.dashboardFragment)

                    true
                }
                R.id.bottom_compaigns -> {
                    // Respond to navigation item 2 click
                    navController.navigate(R.id.allCompaignsFragment)
                    true
                }


                R.id.bottom_invest -> {
                    // Respond to navigation item 2 click
                    startActivity(Intent(this, MyInvestmentActivity::class.java))

                    true
                }


                R.id.bottom_profile -> {
                    // Respond to navigation item 2 click
                    navController.navigate(R.id.profileFragment)

                    true
                }

                R.id.bottom_support -> {
                    // Respond to navigation item 2 click
                    //  navController.navigate(R.id.supportFragment)
                    setupFreshchat()
                    Freshchat.showConversations(this);

                    true
                }

                else -> false
            }
        }




        navController.addOnDestinationChangedListener(object :
            NavController.OnDestinationChangedListener {
            override fun onDestinationChanged(
                controller: NavController,
                destination: NavDestination,
                arguments: Bundle?
            ) {

                when (destination.id) {


                    R.id.dashboardFragment -> {
                        binding.rootToolbar.toolbarMain.visibility = View.GONE


                    }

                    R.id.allCompaignsFragment -> {


                        Log.d("CHECKEDDDD", "compaigns")

                        binding.rootToolbar.toolbarMain.visibility = View.VISIBLE
                        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back_ios_24)
                        binding.rootToolbar.toolbarMain.visibility = View.GONE
//                        val bottomCompaign = bottomNavigation.menu.findItem(R.id.bottom_compaigns)
//                        bottomCompaign.isChecked = true
                    }

                    R.id.articleFragment -> {

                        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back_ios_24)

                        binding.rootToolbar.toolbarMain.visibility = View.VISIBLE

                    }

                    R.id.compareFragment -> {

                        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back_ios_24)

                        binding.rootToolbar.toolbarMain.visibility = View.VISIBLE

                    }
                    R.id.articleDetailFragment -> {

                        binding.rootToolbar.toolbarMain.visibility = View.GONE


                    }
                    R.id.notificationFragment -> {

                        binding.rootToolbar.toolbarMain.visibility = View.VISIBLE
                        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back_ios_24)


                    }

                    R.id.dashboardDetail -> {

                        binding.rootToolbar.toolbarMain.visibility = View.GONE

                        binding.bottomNavigation.visibility = View.GONE

                    }
                    R.id.personalInfoFragment -> {
                        binding.bottomNavigation.visibility = View.GONE
                    }
                    R.id.sendMessageFragment -> {
                        binding.bottomNavigation.visibility = View.GONE
                        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back_ios_24)

                    }

                    R.id.simulationFragment -> {
                        binding.rootToolbar.toolbarMain.visibility = View.VISIBLE
                        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back_ios_24)
                        binding.bottomNavigation.visibility = View.GONE


                    }


                    R.id.profileFragment -> {

                        binding.rootToolbar.toolbarMain.visibility = View.GONE
//                        val bottomSupport = bottomNavigation.menu.findItem(R.id.bottom_profile)
//                        bottomSupport.isChecked = true
                    }
                    R.id.sortFragment -> {

                        binding.rootToolbar.toolbarMain.visibility = View.VISIBLE
                        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back_ios_24)

                    }

                    else -> {

                        binding.rootToolbar.toolbarMain.visibility = View.GONE

                        binding.bottomNavigation.visibility = View.VISIBLE

                    }
                }


            }

        })

        val data = intent.getStringExtra("from")

        if (data != null) {

            navController.navigate(R.id.allCompaignsFragment)


        }


    }





    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        if (navController.navigateUp()) {
            return navController.navigateUp()
        } else return super.onSupportNavigateUp()
    }

    private fun setupFreshchat() {
        Freshchat.setImageLoader(CustomImageLoader(this))

        val config = FreshchatConfig(
            resources.getString(R.string.freshchat_app_id),
            resources.getString(R.string.freshchat_app_key)
        )
        config.setDomain(resources.getString(R.string.freshchat_domain))
        Freshchat.getInstance(this)
            .init(config)

        val freshchatUser = Freshchat.getInstance(this).user
        freshchatUser.firstName = Constant.getUserID(applicationContext).userName
        freshchatUser.lastName = ""
        freshchatUser.email = Constant.getUserID(applicationContext).address
        freshchatUser.setPhone("+92", Constant.getUserID(applicationContext).userMobile.toString())
        Freshchat.getInstance(this).user = freshchatUser

        //Set up external id
        Freshchat.getInstance(this)
            .identifyUser(Constant.getUserID(applicationContext).userID.toString() + "k234", null);

        //Set up restore id and sace
        val restoreId =
            Freshchat.getInstance(this)
                .user.restoreId
        Paper.init(this)
        Paper.book().write(Constant.FRESHCHAT_RESTORE_ID, restoreId)


        //Register broadcast receiver for generating restore id
        val broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                val restoreId =
                    Freshchat.getInstance(applicationContext)
                        .user.restoreId

                Paper.init(applicationContext)
                Paper.book().write(Constant.FRESHCHAT_RESTORE_ID, restoreId)
            }
        }


        val intentFilter = IntentFilter(Freshchat.FRESHCHAT_USER_RESTORE_ID_GENERATED)
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(broadcastReceiver, intentFilter)


    }


}






