package com.ruralculture.kayabkishan

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.ruralculture.kayabkishan.dashboard.repository.DashboardViewModel
import com.ruralculture.kayabkishan.databinding.FragmentNotificationBinding
import com.ruralculture.kayabkishan.notification.NotificationAdapter


class NotificationFragment : Fragment() {

    lateinit var binding:FragmentNotificationBinding

    private val dashboardViewModel:DashboardViewModel by viewModels()



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNotificationBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment

        dashboardViewModel.getNotification()


        lifecycleScope.launchWhenStarted {


            dashboardViewModel.dashboardEvent.observe(viewLifecycleOwner, Observer {

                when(it){

                    is DashboardViewModel.DashboardEvent.SuccessNotification -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false


                        binding.notificaitonList.apply {

                            layoutManager =  LinearLayoutManager(context)
                            adapter = NotificationAdapter(it.notification.data.toMutableList())

                        }



                    }

                    is DashboardViewModel.DashboardEvent.Loading -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true


                    }


                }






            })



        }




        return binding.root
    }

}